// Eiffel langage Lexer
// Louis M
// License: MIT
package net.libreti.intelleiffel.lexer;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import net.libreti.intelleiffel.psi.EiffelTypes;
import static com.intellij.psi.TokenType.BAD_CHARACTER;
import com.intellij.psi.TokenType;

%%

%class EiffelLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}

WHITE_SPACE=[\ \n\t\r]
END_OF_LINE_COMMENT="--"[^\r\n]*
IDENTIFIER=[a-zA-Z][a-zA-Z0-9_]*

// Constants
NUMBER_CONSTANT=([0-9]) |
                ([0-9][0-9_]*[0-9]) |
                (0[xX][0-9a-fA-F]) |
                (0[xX][0-9a-fA-F][0-9a-fA-F_]*[0-9a-fA-F]) |
                (0[cC][0-7]) |
                (0[cC][0-7][0-7_]*[0-7]) |
                (0[bB][0-1]) |
                (0[bB][0-1][0-1_]*[0-1])
CHARACTER_CONSTANT=\'([^%\n\']|(%[ABCDFHLNQRSTUV%\'\"\(\)<>])|(%\/{NUMBER_CONSTANT}\/))\'
STRING_CONSTANT=(\"([^%\n\"]|(%[ABCDFHLNQRSTUV%\'\"()<>])|(%\/{NUMBER_CONSTANT}\/)|(%\r?\n[ \t]*%))*\") |
                (\"\[([ \t]*\r?\n)~(\n[ \t]*\]\")) |
                (\"\{([ \t]*\r?\n)~(\n[ \t]*\}\"))
REAL_CONSTANT=[0-9]+\.[^.0-9]            |
              [0-9]+\.[0-9]*[eE][+-]?[0-9]+        |
              [0-9]*\.[0-9]+([eE][+-]?[0-9]+)?    |
              [0-9]{1,3}(_[0-9]{3})+\.[^.0-9]    |
              [0-9]{1,3}(_[0-9]{3})*\.([0-9]{1,3}(_[0-9]{3})*)?[eE][+-]?[0-9]{1,3}(_[0-9]{3})*    |
              ([0-9]{1,3}(_[0-9]{3})*)?\.[0-9]{1,3}(_[0-9]{3})*([eE][+-]?[0-9]{1,3}(_[0-9]{3})*)?

// Brackets
LEFT_PARENTHESE="("
RIGHT_PARENTHESE=")"
LEFT_BRACE="{"
RIGHT_BRACE="}"
LEFT_BRACKET="["
RIGHT_BRACKET="]"
EMPTY_BRACKETS="[]"

// Quote
SINGLE_QUOTE="'"
QUOTE="\""

// Operators
PLUS_OPERATOR="+"
MINUS_OPERATOR="-"
STAR_OPERATOR="*"
SLASH_OPERATOR="/"
AND_OPERATOR="&"
OR_OPERATOR="|"
HAT_OPERATOR="^"
LOWER_OPERATOR="<"
GREATER_OPERATOR=">"
LEFT_SHIFT_OPERATOR="|<<"
RIGHT_SHIFT_OPERATOR="|>>"
DIVISION_OPERATOR="//"
MODULO_OPERATOR="\\\\"
EQUAL_OPERATOR="="
NOT_EQUAL_OPERATOR="/="
GREATER_EQUAL_OPERATOR=>=|≥
LOWER_EQUAL_OPERATOR=<=|≤
EQUIVALENT_OPERATOR="~"
NOT_EQUIVALENT_OPERATOR="/~"
FREE_OPERATOR=[@#|&][^ \t\r\n]*|[∋⊆≜√π≡∨∧⋚×÷⊗⊝⦶⧀⧁⊕]+


// Separator
DOT_SEPARATOR="."
SEMI_COLON_SEPARATOR=";"
COMMA_SEPARATOR=","
COLON_SEPARATOR=":"
ARROW_SEPARATOR="->"
DOT_DOT_SEPARATOR=".."

// Other symbole
EXCLAMATION="!"
INTERROGATION="?"
DOLLAR="$"
LEFT_ARRAY="<<"
RIGHT_ARRAY=">>"
ASSIGN=":="
FOR_ALL="∀"
EXISTS="∃"
CLOCKWISE_CIRCLE="⟳"
COUNTER_CLOCKWISE_CIRCLE="⟲"
BROKEN_BAR="¦"

// Keywords
ACROSS_KEYWORD=[aA][cC][rR][oO][sS][sS]
AGENT_KEYWORD=[aA][gG][eE][nN][tT]
ALIAS_KEYWORD=[aA][lL][iI][aA][sS]
ALL_KEYWORD=[aA][lL][lL]
AND_KEYWORD=[aA][nN][dD]
AS_KEYWORD=[aA][sS]
ASSIGN_KEYWORD=[aA][sS][sS][iI][gG][nN]
ATTACHED_KEYWORD=[aA][tT][tT][aA][cC][hH][eE][dD]
ATTRIBUTE_KEYWORD=[aA][tT][tT][rR][iI][bB][uU][tT][eE]
CHECK_KEYWORD=[cC][hH][eE][cC][kK]
CLASS_KEYWORD=[cC][lL][aA][sS][sS]
CONVERT_KEYWORD=[cC][oO][nN][vV][eE][rR][tT]
CREATE_KEYWORD=[cC][rR][eE][aA][tT][eE]
CURRENT_KEYWORD=[cC][uU][rR][rR][eE][nN][tT]
DEBUG_KEYWORD=[dD][eE][bB][uU][gG]
DEFERRED_KEYWORD=[dD][eE][fF][eE][rR][rR][eE][dD]
DETACHABLE_KEYWORD=[dD][eE][tT][aA][cC][hH][aA][bB][lL][eE]
DO_KEYWORD=[dD][oO]
ELSE_KEYWORD=[eE][lL][sS][eE]
ELSEIF_KEYWORD=[eE][lL][sS][eE][iI][fF]
END_KEYWORD=[eE][nN][dD]
ENSURE_KEYWORD=[eE][nN][sS][uU][rR][eE]
EXPANDED_KEYWORD=[eE][xX][pP][aA][nN][dD][eE][dD]
EXPORT_KEYWORD=[eE][xX][pP][oO][rR][tT]
EXTERNAL_KEYWORD=[eE][xX][tT][eE][rR][nN][aA][lL]
FALSE_KEYWORD=[fF][aA][lL][sS][eE]
FEATURE_KEYWORD=[fF][eE][aA][tT][uU][rR][eE]
FROM_KEYWORD=[fF][rR][oO][mM]
FROZEN_KEYWORD=[fF][rR][oO][zZ][eE][nN]
IF_KEYWORD=[iI][fF]
IMPLIES_KEYWORD=[iI][mM][pP][lL][iI][eE][sS]
INHERIT_KEYWORD=[iI][nN][hH][eE][rR][iI][tT]
INSPECT_KEYWORD=[iI][nN][sS][pP][eE][cC][tT]
INVARIANT_KEYWORD=[iI][nN][vV][aA][rR][iI][aA][nN][tT]
LIKE_KEYWORD=[lL][iI][kK][eE]
LOCAL_KEYWORD=[lL][oO][cC][aA][lL]
LOOP_KEYWORD=[lL][oO][oO][pP]
NONE_KEYWORD=[nN][oO][nN][eE]
NOTE_KEYWORD=[nN][oO][tT][eE]
NOT_KEYWORD=[nN][oO][tT]
OBSOLETE_KEYWORD=[oO][bB][sS][oO][lL][eE][tT][eE]
OLD_KEYWORD=[oO][lL][dD]
ONCE_KEYWORD=[oO][nN][cC][eE]
ONLY_KEYWORD=[oO][nN][lL][yY]
OR_KEYWORD=[oO][rR]
PRECURSOR_KEYWORD=[pP][rR][eE][cC][uU][rR][sS][oO][rR]
REDEFINE_KEYWORD=[rR][eE][dD][eE][fF][iI][nN][eE]
RENAME_KEYWORD=[rR][eE][nN][aA][mM][eE]
REQUIRE_KEYWORD=[rR][eE][qQ][uU][iI][rR][eE]
RESCUE_KEYWORD=[rR][eE][sS][cC][uU][eE]
RESULT_KEYWORD=[rR][eE][sS][uU][lL][tT]
RETRY_KEYWORD=[rR][eE][tT][rR][yY]
SELECT_KEYWORD=[sS][eE][lL][eE][cC][tT]
SEPARATE_KEYWORD=[sS][eE][pP][aA][rR][aA][tT][eE]
SOME_KEYWORD=[sS][oO][mM][eE]
THEN_KEYWORD=[tT][hH][eE][nN]
TRUE_KEYWORD=[tT][rR][uU][eE]
TUPLE_KEYWORD=[tT][uU][pP][lL][eE]
UNDEFINE_KEYWORD=[uU][nN][dD][eE][fF][iI][nN][eE]
UNTIL_KEYWORD=[uU][nN][tT][iI][lL]
VARIANT_KEYWORD=[vV][aA][rR][iI][aA][nN][tT]
WHEN_KEYWORD=[wW][hH][eE][nN]
XOR_KEYWORD=[xX][oO][rR]



%%


    {WHITE_SPACE}                   { return TokenType.WHITE_SPACE; }
    {END_OF_LINE_COMMENT}           { return EiffelTypes.EIF_LINE_COMMENT; }

    {NUMBER_CONSTANT}               { return EiffelTypes.EIF_NUMBER; }
    {CHARACTER_CONSTANT}            { return EiffelTypes.EIF_CHARACTER; }
    {STRING_CONSTANT}               { return EiffelTypes.EIF_STRING; }
    {REAL_CONSTANT}                 { return EiffelTypes.EIF_REAL; }
    {LEFT_PARENTHESE}               { return EiffelTypes.EIF_LEFT_PARENTHESE; }
    {RIGHT_PARENTHESE}              { return EiffelTypes.EIF_RIGHT_PARENTHESE; }
    {LEFT_BRACE}                    { return EiffelTypes.EIF_LEFT_BRACE; }
    {RIGHT_BRACE}                   { return EiffelTypes.EIF_RIGHT_BRACE; }
    {LEFT_BRACKET}                  { return EiffelTypes.EIF_LEFT_BRACKET; }
    {RIGHT_BRACKET}                 { return EiffelTypes.EIF_RIGHT_BRACKET; }
    {EMPTY_BRACKETS}                { return EiffelTypes.EIF_EMPTY_BRACKETS; }
    {SINGLE_QUOTE}                  { return EiffelTypes.EIF_SINGLE_QUOTE; }
    {QUOTE}                         { return EiffelTypes.EIF_QUOTE; }
    {PLUS_OPERATOR}                 { return EiffelTypes.EIF_PLUS_OPERATOR; }
    {MINUS_OPERATOR}                { return EiffelTypes.EIF_MINUS_OPERATOR; }
    {STAR_OPERATOR}                 { return EiffelTypes.EIF_STAR_OPERATOR; }
    {SLASH_OPERATOR}                { return EiffelTypes.EIF_SLASH_OPERATOR; }
    {AND_OPERATOR}                  { return EiffelTypes.EIF_AND_OPERATOR; }
    {OR_OPERATOR}                   { return EiffelTypes.EIF_OR_OPERATOR; }
    {HAT_OPERATOR}                  { return EiffelTypes.EIF_HAT_OPERATOR; }
    {LOWER_OPERATOR}                { return EiffelTypes.EIF_LOWER_OPERATOR; }
    {GREATER_OPERATOR}              { return EiffelTypes.EIF_GREATER_OPERATOR; }
    {LEFT_SHIFT_OPERATOR}           { return EiffelTypes.EIF_LEFT_SHIFT_OPERATOR; }
    {RIGHT_SHIFT_OPERATOR}          { return EiffelTypes.EIF_RIGHT_SHIFT_OPERATOR; }
    {DIVISION_OPERATOR}             { return EiffelTypes.EIF_DIVISION_OPERATOR; }
    {MODULO_OPERATOR}               { return EiffelTypes.EIF_MODULO_OPERATOR; }
    {EQUAL_OPERATOR}                { return EiffelTypes.EIF_EQUAL_OPERATOR; }
    {NOT_EQUAL_OPERATOR}            { return EiffelTypes.EIF_NOT_EQUAL_OPERATOR; }
    {GREATER_EQUAL_OPERATOR}        { return EiffelTypes.EIF_GREATER_EQUAL_OPERATOR; }
    {LOWER_EQUAL_OPERATOR}          { return EiffelTypes.EIF_LOWER_EQUAL_OPERATOR; }
    {EQUIVALENT_OPERATOR}           { return EiffelTypes.EIF_EQUIVALENT_OPERATOR; }
    {NOT_EQUIVALENT_OPERATOR}       { return EiffelTypes.EIF_NOT_EQUIVALENT_OPERATOR; }
    {FREE_OPERATOR}                 { return EiffelTypes.EIF_FREE_OPERATOR; }
    {DOT_SEPARATOR}                 { return EiffelTypes.EIF_DOT_SEPARATOR; }
    {SEMI_COLON_SEPARATOR}          { return EiffelTypes.EIF_SEMI_COLON_SEPARATOR; }
    {COMMA_SEPARATOR}               { return EiffelTypes.EIF_COMMA_SEPARATOR; }
    {COLON_SEPARATOR}               { return EiffelTypes.EIF_COLON_SEPARATOR; }
    {ARROW_SEPARATOR}               { return EiffelTypes.EIF_ARROW_SEPARATOR; }
    {DOT_DOT_SEPARATOR}             { return EiffelTypes.EIF_DOT_DOT_SEPARATOR; }
    {EXCLAMATION}                   { return EiffelTypes.EIF_EXCLAMATION; }
    {INTERROGATION}                 { return EiffelTypes.EIF_INTERROGATION; }
    {DOLLAR}                        { return EiffelTypes.EIF_DOLLAR; }
    {LEFT_ARRAY}                    { return EiffelTypes.EIF_LEFT_ARRAY; }
    {RIGHT_ARRAY}                   { return EiffelTypes.EIF_RIGHT_ARRAY; }
    {ASSIGN}                        { return EiffelTypes.EIF_ASSIGN; }
    {FOR_ALL}                       { return EiffelTypes.EIF_FOR_ALL; }
    {EXISTS}                        { return EiffelTypes.EIF_EXISTS; }
    {CLOCKWISE_CIRCLE}              { return EiffelTypes.EIF_CLOCKWISE_CIRCLE; }
    {COUNTER_CLOCKWISE_CIRCLE}      { return EiffelTypes.EIF_COUNTER_CLOCKWISE_CIRCLE; }
    {BROKEN_BAR}                    { return EiffelTypes.EIF_BROKEN_BAR; }
    {ACROSS_KEYWORD}                { return EiffelTypes.EIF_ACROSS_KEYWORD; }
    {AGENT_KEYWORD}                 { return EiffelTypes.EIF_AGENT_KEYWORD; }
    {ALIAS_KEYWORD}                 { return EiffelTypes.EIF_ALIAS_KEYWORD; }
    {ALL_KEYWORD}                   { return EiffelTypes.EIF_ALL_KEYWORD; }
    {AND_KEYWORD}                   { return EiffelTypes.EIF_AND_KEYWORD; }
    {AS_KEYWORD}                    { return EiffelTypes.EIF_AS_KEYWORD; }
    {ASSIGN_KEYWORD}                { return EiffelTypes.EIF_ASSIGN_KEYWORD; }
    {ATTACHED_KEYWORD}              { return EiffelTypes.EIF_ATTACHED_KEYWORD; }
    {ATTRIBUTE_KEYWORD}             { return EiffelTypes.EIF_ATTRIBUTE_KEYWORD; }
    {CHECK_KEYWORD}                 { return EiffelTypes.EIF_CHECK_KEYWORD; }
    {CLASS_KEYWORD}                 { return EiffelTypes.EIF_CLASS_KEYWORD; }
    {CONVERT_KEYWORD}               { return EiffelTypes.EIF_CONVERT_KEYWORD; }
    {CREATE_KEYWORD}                { return EiffelTypes.EIF_CREATE_KEYWORD; }
    {CURRENT_KEYWORD}               { return EiffelTypes.EIF_CURRENT_KEYWORD; }
    {DEBUG_KEYWORD}                 { return EiffelTypes.EIF_DEBUG_KEYWORD; }
    {DETACHABLE_KEYWORD}            { return EiffelTypes.EIF_DETACHABLE_KEYWORD; }
    {DEFERRED_KEYWORD}              { return EiffelTypes.EIF_DEFERRED_KEYWORD; }
    {DO_KEYWORD}                    { return EiffelTypes.EIF_DO_KEYWORD; }
    {ELSE_KEYWORD}                  { return EiffelTypes.EIF_ELSE_KEYWORD; }
    {ELSEIF_KEYWORD}                { return EiffelTypes.EIF_ELSEIF_KEYWORD; }
    {END_KEYWORD}                   { return EiffelTypes.EIF_END_KEYWORD; }
    {ENSURE_KEYWORD}                { return EiffelTypes.EIF_ENSURE_KEYWORD; }
    {EXPANDED_KEYWORD}              { return EiffelTypes.EIF_EXPANDED_KEYWORD; }
    {EXPORT_KEYWORD}                { return EiffelTypes.EIF_EXPORT_KEYWORD; }
    {EXTERNAL_KEYWORD}              { return EiffelTypes.EIF_EXTERNAL_KEYWORD; }
    {FALSE_KEYWORD}                 { return EiffelTypes.EIF_FALSE_KEYWORD; }
    {FEATURE_KEYWORD}               { return EiffelTypes.EIF_FEATURE_KEYWORD; }
    {FROM_KEYWORD}                  { return EiffelTypes.EIF_FROM_KEYWORD; }
    {FROZEN_KEYWORD}                { return EiffelTypes.EIF_FROZEN_KEYWORD; }
    {IF_KEYWORD}                    { return EiffelTypes.EIF_IF_KEYWORD; }
    {IMPLIES_KEYWORD}               { return EiffelTypes.EIF_IMPLIES_KEYWORD; }
    {INHERIT_KEYWORD}               { return EiffelTypes.EIF_INHERIT_KEYWORD; }
    {INSPECT_KEYWORD}               { return EiffelTypes.EIF_INSPECT_KEYWORD; }
    {INVARIANT_KEYWORD}             { return EiffelTypes.EIF_INVARIANT_KEYWORD; }
    {LIKE_KEYWORD}                  { return EiffelTypes.EIF_LIKE_KEYWORD; }
    {LOCAL_KEYWORD}                 { return EiffelTypes.EIF_LOCAL_KEYWORD; }
    {LOOP_KEYWORD}                  { return EiffelTypes.EIF_LOOP_KEYWORD; }
    {NONE_KEYWORD}                  { return EiffelTypes.EIF_NONE_KEYWORD; }
    {NOTE_KEYWORD}                  { return EiffelTypes.EIF_NOTE_KEYWORD; }
    {NOT_KEYWORD}                   { return EiffelTypes.EIF_NOT_KEYWORD; }
    {OBSOLETE_KEYWORD}              { return EiffelTypes.EIF_OBSOLETE_KEYWORD; }
    {OLD_KEYWORD}                   { return EiffelTypes.EIF_OLD_KEYWORD; }
    {ONCE_KEYWORD}                  { return EiffelTypes.EIF_ONCE_KEYWORD; }
    {ONLY_KEYWORD}                  { return EiffelTypes.EIF_ONLY_KEYWORD; }
    {OR_KEYWORD}                    { return EiffelTypes.EIF_OR_KEYWORD; }
    {PRECURSOR_KEYWORD}             { return EiffelTypes.EIF_PRECURSOR_KEYWORD; }
    {REDEFINE_KEYWORD}              { return EiffelTypes.EIF_REDEFINE_KEYWORD; }
    {RENAME_KEYWORD}                { return EiffelTypes.EIF_RENAME_KEYWORD; }
    {REQUIRE_KEYWORD}               { return EiffelTypes.EIF_REQUIRE_KEYWORD; }
    {RESCUE_KEYWORD}                { return EiffelTypes.EIF_RESCUE_KEYWORD; }
    {RESULT_KEYWORD}                { return EiffelTypes.EIF_RESULT_KEYWORD; }
    {RETRY_KEYWORD}                 { return EiffelTypes.EIF_RETRY_KEYWORD; }
    {SELECT_KEYWORD}                { return EiffelTypes.EIF_SELECT_KEYWORD; }
    {SEPARATE_KEYWORD}              { return EiffelTypes.EIF_SEPARATE_KEYWORD; }
    {SOME_KEYWORD}                  { return EiffelTypes.EIF_SOME_KEYWORD; }
    {THEN_KEYWORD}                  { return EiffelTypes.EIF_THEN_KEYWORD; }
    {TRUE_KEYWORD}                  { return EiffelTypes.EIF_TRUE_KEYWORD; }
    {TUPLE_KEYWORD}                 { return EiffelTypes.EIF_TUPLE_KEYWORD; }
    {UNDEFINE_KEYWORD}              { return EiffelTypes.EIF_UNDEFINE_KEYWORD; }
    {UNTIL_KEYWORD}                 { return EiffelTypes.EIF_UNTIL_KEYWORD; }
    {VARIANT_KEYWORD}               { return EiffelTypes.EIF_VARIANT_KEYWORD; }
    {WHEN_KEYWORD}                  { return EiffelTypes.EIF_WHEN_KEYWORD; }
    {XOR_KEYWORD}                   { return EiffelTypes.EIF_XOR_KEYWORD; }
    {IDENTIFIER}                    { return EiffelTypes.EIF_IDENTIFIER; }


// error fallback
[^]                                  { return TokenType.BAD_CHARACTER; }





