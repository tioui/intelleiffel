package net.libreti.intelleiffel;

import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.fileTypes.SyntaxHighlighterFactory;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Factory used to generate Syntax hyghlighter.
 *
 * @author Louis M
 */
public class EiffelSyntaxHighlighterFactory extends SyntaxHighlighterFactory {

    /**
     * Generate a syntax highlighter.
     *
     * @param project     Not used (can be used to gather project elements)
     * @param virtualFile Not used (can be used to collect file specific settings)
     * @return The syntax highlighter.
     */
    @Override
    public @NotNull SyntaxHighlighter getSyntaxHighlighter(@Nullable Project project,
                                                           @Nullable VirtualFile virtualFile) {
        return new EiffelSyntaxHighlighter();
    }
}
