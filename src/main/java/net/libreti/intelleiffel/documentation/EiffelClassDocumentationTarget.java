package net.libreti.intelleiffel.documentation;

import com.intellij.lang.documentation.DocumentationMarkup;
import com.intellij.lang.documentation.QuickDocHighlightingHelper;
import com.intellij.model.Pointer;
import com.intellij.platform.backend.documentation.DocumentationResult;
import com.intellij.platform.backend.documentation.DocumentationTarget;
import com.intellij.platform.backend.presentation.TargetPresentation;
import net.libreti.intelleiffel.EiffelLanguage;
import net.libreti.intelleiffel.models.EiffelNoteDocumentation;
import net.libreti.intelleiffel.parser.Mixins.EiffelClass;
import net.libreti.intelleiffel.parser.Mixins.EiffelClassNameIdentifier;
import net.libreti.intelleiffel.parser.Mixins.EiffelNotes;
import net.libreti.intelleiffel.psi.EiffelPsiHeaderMark;
import net.libreti.intelleiffel.psi.EiffelPsiNotes;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Create documentation for Eiffel class
 *
 * @author Louis M
 */
public class EiffelClassDocumentationTarget implements DocumentationTarget {

    /**
     * The Psi element to document.
     */
    private EiffelClassNameIdentifier identifier;

    /**
     * The eiffel class that is represented by the identifier (or null if the class is not found)
     */
    private @Nullable EiffelClass eiffelClass;

    /**
     * Constructor of the object
     *
     * @param aIdentifier The Psi element to document
     */
    public EiffelClassDocumentationTarget(@NotNull EiffelClassNameIdentifier aIdentifier) {
        identifier = aIdentifier;
        eiffelClass = EiffelUtil.getInstance().findEiffelClass(identifier.getClassName(), identifier.getProject());
    }

    /**
     * Documentation of the header of the class.
     *
     * @return A string that represent the header of the class in the documentation
     */
    private String computeDocumentationHeader() {
        StringBuilder lPreLexerBuilder = new StringBuilder();
        EiffelPsiHeaderMark lMark = eiffelClass.getClassHeader().getHeaderMark();
        if (lMark != null) {
            if (lMark.getDeferredKeyword() != null) {
                lPreLexerBuilder.append(lMark.getDeferredKeyword().getText());
                lPreLexerBuilder.append(" ");
            } else {
                if (lMark.getFrozenKeyword() != null) {
                    lPreLexerBuilder.append(lMark.getFrozenKeyword().getText());
                    lPreLexerBuilder.append(" ");
                }
                if (lMark.getExpandedKeyword() != null) {
                    lPreLexerBuilder.append(lMark.getExpandedKeyword().getText());
                    lPreLexerBuilder.append(" ");
                }
            }
        }
        lPreLexerBuilder.append(eiffelClass.getClassHeader().getClassKeyword().getText());
        lPreLexerBuilder.append(" ");
        lPreLexerBuilder.append(eiffelClass.getName());
        return lPreLexerBuilder.toString();
    }

    /**
     * Create the documentation of every class notes (header and bottom of the class)
     *
     * @param aStringBuilder Where to put the documentation.
     */
    private void computeDocumentationNotes(StringBuilder aStringBuilder) {
        boolean lFirst = true;
        boolean lHasFirst = false;
        for (EiffelPsiNotes lNotes : eiffelClass.getNotesList()) {
            if (lFirst) {
                aStringBuilder.append(DocumentationMarkup.CONTENT_START);
                lHasFirst = true;
            }
            for (EiffelNoteDocumentation lNote : ((EiffelNotes) lNotes).getDocumentation()) {
                aStringBuilder.append(DocumentationMarkup.SECTIONS_START);
                aStringBuilder.append(DocumentationMarkup.SECTION_START);
                aStringBuilder.append(lNote.getId());
                aStringBuilder.append(DocumentationMarkup.SECTION_SEPARATOR);
                aStringBuilder.append(lNote.getMessage());
                aStringBuilder.append(DocumentationMarkup.SECTION_END);
                aStringBuilder.append(DocumentationMarkup.SECTIONS_END);
            }
        }
        if (lHasFirst) {
            aStringBuilder.append(DocumentationMarkup.CONTENT_END);
        }
    }

    /**
     * If the class is obsolete, show the obsolete message in the documentation
     *
     * @param aStringBuilder Where to put the documentation.
     */
    private void computeDocumentationObsolete(StringBuilder aStringBuilder) {
        if (eiffelClass.getObsolete() != null) {
            aStringBuilder.append(DocumentationMarkup.CONTENT_START);
            aStringBuilder.append(DocumentationMarkup.SECTIONS_START);
            aStringBuilder.append(DocumentationMarkup.SECTION_START);
            aStringBuilder.append(eiffelClass.getObsolete().getObsoleteKeyword().getText());
            aStringBuilder.append(DocumentationMarkup.SECTION_SEPARATOR);
            aStringBuilder.append(eiffelClass.getObsoleteMessage());
            aStringBuilder.append(DocumentationMarkup.SECTIONS_END);
            aStringBuilder.append(DocumentationMarkup.CONTENT_END);
        }
        aStringBuilder.append(DocumentationMarkup.SECTIONS_START);
    }

    /**
     * Compute the documentation of the class
     *
     * @return The documentation of the class.
     */
    @Override
    public @Nullable DocumentationResult computeDocumentation() {
        DocumentationResult lResult = null;
        if (eiffelClass != null) {
            StringBuilder lStringBuilder = new StringBuilder();
            lStringBuilder.append(DocumentationMarkup.CONTENT_START);
            lStringBuilder.append(DocumentationMarkup.DEFINITION_START);
            if (eiffelClass.isObsolete()) {
                lStringBuilder.append(DocumentationMarkup.GRAYED_START);
                lStringBuilder.append(computeDocumentationHeader());
                lStringBuilder.append(DocumentationMarkup.GRAYED_END);
            } else {
                QuickDocHighlightingHelper.appendStyledCodeFragment(lStringBuilder, eiffelClass.getProject(),
                        EiffelLanguage.getInstance(), computeDocumentationHeader());
            }
            lStringBuilder.append(DocumentationMarkup.DEFINITION_END);
            lStringBuilder.append(DocumentationMarkup.CONTENT_END);
            if (eiffelClass.isObsolete()) {
                computeDocumentationObsolete(lStringBuilder);
            }
            computeDocumentationNotes(lStringBuilder);
            lResult = DocumentationResult.documentation(lStringBuilder.toString());
        }
        return lResult;
    }

    /**
     * Create a pointer from the `identifier` that can recover an object of this class.
     *
     * @return The pointer
     */
    @Override
    public @NotNull Pointer<? extends DocumentationTarget> createPointer() {
        return new EiffelDocumentationPointer(identifier);
    }

    /**
     * Compute a presentation of the current class.
     * @return the presentation
     */
    @Override
    public @NotNull TargetPresentation computePresentation() {
        TargetPresentation lResult = null;
        if (eiffelClass != null) {
            lResult = TargetPresentation.builder(eiffelClass.getPresentableText()).icon(
                    eiffelClass.getIcon(false)).presentation();
        } else {
            lResult = TargetPresentation.builder(identifier.getClassName()).presentation();
        }
        return lResult;
    }

}
