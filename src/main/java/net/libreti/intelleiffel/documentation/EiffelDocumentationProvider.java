package net.libreti.intelleiffel.documentation;

import com.intellij.platform.backend.documentation.DocumentationTarget;
import com.intellij.platform.backend.documentation.PsiDocumentationTargetProvider;
import com.intellij.psi.PsiElement;
import net.libreti.intelleiffel.parser.Mixins.EiffelClassNameDefinition;
import net.libreti.intelleiffel.parser.Mixins.EiffelClassNameIdentifier;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Provide DocumentationTarget for Eiffel Psi element.
 *
 * @author Louis M
 */
public class EiffelDocumentationProvider implements PsiDocumentationTargetProvider {

    /**
     * Get a documentation target for a specific Eiffel Psi element.
     * @param aElement The Eiffel Psi Element that have to be documented
     * @param aOriginalElement The source of the Eiffel Psi element (not used for now)
     * @return The documentation target for the element.
     */
    @Override
    public @Nullable DocumentationTarget documentationTarget(@NotNull PsiElement aElement,
                                                             @Nullable PsiElement aOriginalElement) {
        DocumentationTarget lResult = null;
        if (aElement instanceof EiffelClassNameDefinition lDefinition) {
            lResult = new EiffelClassDocumentationTarget(
                    (EiffelClassNameIdentifier)lDefinition.getClassNameIdentifier());
        }
        return lResult;
    }
}
