package net.libreti.intelleiffel.documentation;

import com.intellij.model.Pointer;
import com.intellij.platform.backend.documentation.DocumentationTarget;
import com.intellij.psi.PsiElement;
import com.intellij.psi.SmartPointerManager;
import net.libreti.intelleiffel.parser.Mixins.EiffelClassNameIdentifier;
import org.jetbrains.annotations.Nullable;

/**
 * A Pointeur to a Documentation target.
 *
 * @author Louis M
 */
public class EiffelDocumentationPointer implements Pointer<DocumentationTarget> {

    /**
     * The pointer to the element that is documented.
     */
    private Pointer<PsiElement> elementPointer;

    /**
     * Constructor of the object
     *
     * @param aElement The lement to store in the pointer
     */
    protected EiffelDocumentationPointer(PsiElement aElement) {
        elementPointer = SmartPointerManager.createPointer(aElement);
    }

    /**
     * Get a documentation target from the element.
     *
     * @return The documentation target
     */
    @Override
    public @Nullable DocumentationTarget dereference() {
        DocumentationTarget lResult = null;
        PsiElement lElement = elementPointer.dereference();
        if (lElement != null) {
            if (lElement instanceof EiffelClassNameIdentifier lIdentifier) {
                lResult = new EiffelClassDocumentationTarget(lIdentifier);
            }
        }
        return lResult;
    }
}
