package net.libreti.intelleiffel.completion;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.util.ProcessingContext;
import net.libreti.intelleiffel.models.EiffelAddressable;
import net.libreti.intelleiffel.models.EiffelFeature;
import net.libreti.intelleiffel.psi.EiffelPsiEntityIdentifier;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;

/**
 * An Eiffel Completion Provider that provide completion for Eiffel Entity identifier
 *
 * @author Louis M
 */
public class EiffelEntityIdentifierCompletionProvider extends CompletionProvider<CompletionParameters> {


    /**
     * Complete a Psi element of type Entity_identifier
     *
     * @param aEntityIdentifier Contain the class name
     * @param aResultSet        The possible completions
     */
    private void completeClassNameIdentifier(@NotNull EiffelPsiEntityIdentifier aEntityIdentifier,
                                             @NotNull CompletionResultSet aResultSet) {

    }

    /**
     * Add a completion to the provider.
     *
     * @param aParameters Contain every pertinent parameters for the completion (PsiElement, File, etc.)
     * @param aContext    The context in wich the completion happen (not used)
     * @param aResultSet  The set in which the completions proposition will be added
     */
    public void addCompletions(@NotNull CompletionParameters aParameters,
                               @NotNull ProcessingContext aContext,
                               @NotNull CompletionResultSet aResultSet) {
        if (aParameters.getPosition().getParent() instanceof EiffelPsiEntityIdentifier lEntityIdentifier) {
            for (EiffelAddressable lAddressable :
                    EiffelUtil.getInstance().getAddressablesFromElement(lEntityIdentifier)) {
                LookupElementBuilder lBuilder = LookupElementBuilder.create(lAddressable.getName())
                        .withIcon(lAddressable.getIcon(false)).withCaseSensitivity(false);
                if (lAddressable instanceof EiffelFeature lFeature && lFeature.isObsolete()) {
                    lBuilder = lBuilder.strikeout();
                }
                aResultSet.addElement(lBuilder);
            }
        }
    }
}
