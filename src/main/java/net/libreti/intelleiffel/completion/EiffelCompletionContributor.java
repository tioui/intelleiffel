package net.libreti.intelleiffel.completion;

import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionType;
import com.intellij.patterns.PlatformPatterns;
import net.libreti.intelleiffel.psi.EiffelPsiClassNameIdentifier;
import net.libreti.intelleiffel.psi.EiffelPsiEntityIdentifier;
import net.libreti.intelleiffel.psi.EiffelTypes;

/**
 * The class that initialize every Eiffel completion provider
 *
 * @author Louis M
 */
final class EiffelCompletionContributor extends CompletionContributor {

    /**
     * Creator of the class. Initialise every completion providers
     */
    public EiffelCompletionContributor() {
        extend(CompletionType.BASIC, PlatformPatterns.psiElement(EiffelTypes.EIF_IDENTIFIER).
                        withParent(EiffelPsiClassNameIdentifier.class),
                new EiffelClassNameCompletionProvider()
            );
        extend(CompletionType.BASIC, PlatformPatterns.psiElement(EiffelTypes.EIF_IDENTIFIER).
                        withParent(EiffelPsiEntityIdentifier.class),
                new EiffelEntityIdentifierCompletionProvider()
        );
    }

}
