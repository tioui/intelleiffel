package net.libreti.intelleiffel.completion;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.util.ProcessingContext;
import net.libreti.intelleiffel.EiffelIcons;
import net.libreti.intelleiffel.models.EiffelAddressable;
import net.libreti.intelleiffel.models.EiffelFeature;
import net.libreti.intelleiffel.parser.Mixins.EiffelClass;
import net.libreti.intelleiffel.parser.Mixins.EiffelClassHeader;
import net.libreti.intelleiffel.psi.EiffelPsiClassNameIdentifier;
import net.libreti.intelleiffel.psi.EiffelPsiClassTypeName;
import net.libreti.intelleiffel.psi.EiffelPsiEntityIdentifier;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;

/**
 * An Eiffel Completion Provider that provide completion for Eiffel class name
 *
 * @author Louis M
 */
public class EiffelClassNameCompletionProvider extends CompletionProvider<CompletionParameters> {

    /**
     * Add a class name completion to the provider.
     *
     * @param aParameters Contain every pertinent parameters for the completion (PsiElement, File, etc.)
     * @param aContext    The context in wich the completion happen (not used)
     * @param aResultSet  The set in which the completions proposition will be added
     */
    public void addCompletions(@NotNull CompletionParameters aParameters,
                               @NotNull ProcessingContext aContext,
                               @NotNull CompletionResultSet aResultSet) {
        if (aParameters.getPosition().getParent() instanceof
                EiffelPsiClassNameIdentifier laClassIdentifier) {
            if (laClassIdentifier.getParent() instanceof EiffelPsiClassTypeName) {
                for (EiffelClass lClass : EiffelUtil.getInstance().getEiffelClasses(laClassIdentifier.getProject())) {
                    if (lClass != null && lClass.getName() != null) {
                        LookupElementBuilder lBuilder = LookupElementBuilder.create(lClass.getName())
                                .withCaseSensitivity(false);
                        if (((EiffelClassHeader) lClass.getClassHeader()).isDeferred()) {
                            lBuilder = lBuilder.withIcon(EiffelIcons.DEFERRED_CLASS_ICON);
                        } else {
                            lBuilder = lBuilder.withIcon(EiffelIcons.CLASS_ICON);
                        }
                        if (lClass.isObsolete()) {
                            lBuilder = lBuilder.strikeout();
                        }
                        aResultSet.addElement(lBuilder);
                    }
                }
            }
        }
    }
}
