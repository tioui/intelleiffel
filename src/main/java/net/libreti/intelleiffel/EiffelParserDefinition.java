package net.libreti.intelleiffel;

import com.intellij.lang.ParserDefinition;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.PsiFile;
import com.intellij.psi.FileViewProvider;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.openapi.project.Project;
import com.intellij.lexer.Lexer;
import net.libreti.intelleiffel.psi.EiffelFile;
import net.libreti.intelleiffel.psi.EiffelTypes;
import net.libreti.intelleiffel.psi.EiffelTokenSets;
import net.libreti.intelleiffel.parser.EiffelParser;
import net.libreti.intelleiffel.lexer.EiffelLexerAdapter;
import org.jetbrains.annotations.NotNull;

/**
 * Define the parser for the Eiffel language.
 *
 * @author Louis M
 */
public class EiffelParserDefinition implements ParserDefinition {

    /**
     * An Eiffel file parser node type.
     */
    private static final IFileElementType FILE = new IFileElementType(EiffelLanguage.getInstance());

    /**
     * Instanciate a new EiffelFile.
     *
     * @param aViewProvider virtual file.
     * @return An EiffelFile object.
     */
    @NotNull
    @Override
    public PsiFile createFile(@NotNull FileViewProvider aViewProvider) {
        return new EiffelFile(aViewProvider);
    }

    /**
     * Instanciate a new Eiffel element.
     *
     * @param aNode the node for which the PSI element should be returned.
     * @return The Eiffel element returned from aNode.
     */
    @NotNull
    @Override
    public PsiElement createElement(ASTNode aNode) {
        return EiffelTypes.Factory.createElement(aNode);
    }

    /**
     * An Eiffel token set representing String literal.
     *
     * @return The Eiffel token set that represent String literal.
     */
    @NotNull
    @Override
    public TokenSet getStringLiteralElements() {
        return EiffelTokenSets.STRING;
    }

    /**
     * An Eiffel token set representing comments.
     *
     * @return The Eiffel token set that represent comment.
     */
    @NotNull
    @Override
    public TokenSet getCommentTokens() {
        return EiffelTokenSets.LINE_COMMENT;
    }

    /**
     * The Eiffel parser node type
     *
     * @return The Eiffel parser node type.
     */
    @NotNull
    @Override
    public IFileElementType getFileNodeType() {
        return FILE;
    }

    /**
     * An Eiffel parser
     *
     * @return The Eiffel parser
     */
    @NotNull
    @Override
    public PsiParser createParser(Project project) {
        return new EiffelParser();
    }

    /**
     * Used to generate an Eiffel lexer.
     *
     * @return The Eiffel lexer adapter.
     */
    @NotNull
    @Override
    public Lexer createLexer(Project project) {
        return new EiffelLexerAdapter();
    }
}
