package net.libreti.intelleiffel;

import com.intellij.navigation.NavigationItem;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.Processor;
import com.intellij.util.indexing.FindSymbolParameters;
import com.intellij.util.indexing.IdFilter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Implementation of the Goto symbol for the Eiffel language
 *
 * @author Louis M
 */
public class EiffelSymbolContributor extends EiffelClassContributor {

    /**
     * Every names that can appear in the symbol list
     *
     * @param aProcessor Process the names
     * @param aScope The section of the editor that the search is used.
     * @param aFilter optional filter to use in an index used for searching names
     */
    @Override
    public void processNames(@NotNull Processor<? super String> aProcessor,
                             @NotNull GlobalSearchScope aScope,
                             @Nullable IdFilter aFilter) {
        super.processNames(aProcessor, aScope, aFilter);
    }

    /**
     * Get elements that match a name.
     * @param aName The name to match
     * @param aProcessor Used to process the search
     * @param aParameters Contain parameters for the symbol search
     */
    @Override
    public void processElementsWithName(@NotNull String aName,
                                        @NotNull Processor<? super NavigationItem> aProcessor,
                                        @NotNull FindSymbolParameters aParameters) {
        super.processElementsWithName(aName, aProcessor, aParameters);
    }

}
