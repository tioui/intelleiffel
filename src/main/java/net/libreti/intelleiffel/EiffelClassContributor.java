package net.libreti.intelleiffel;

import com.intellij.navigation.ChooseByNameContributorEx;
import com.intellij.navigation.NavigationItem;
import com.intellij.openapi.project.Project;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.Processor;
import com.intellij.util.containers.ContainerUtil;
import com.intellij.util.indexing.FindSymbolParameters;
import com.intellij.util.indexing.IdFilter;
import net.libreti.intelleiffel.parser.Mixins.EiffelClass;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * The contributor for go to class
 *
 * @author Louis M
 */
public class EiffelClassContributor implements ChooseByNameContributorEx {

    /**
     * Every names that can appear in the symbol list
     *
     * @param aProcessor Process the names
     * @param aScope The section of the editor that the search is used.
     * @param aFilter optional filter to use in an index used for searching names
     */
    @Override
    public void processNames(@NotNull Processor<? super String> aProcessor,
                             @NotNull GlobalSearchScope aScope,
                             @Nullable IdFilter aFilter) {
        Project project = Objects.requireNonNull(aScope.getProject());
        List<String> lClassDefinitions = new LinkedList<>();
        for (EiffelClass lClass : EiffelUtil.getInstance().getEiffelClasses(project)) {
            if (lClass != null && lClass.getName() != null) {
                lClassDefinitions.add(lClass.getName());
            }
        }
        ContainerUtil.process(lClassDefinitions, aProcessor);
    }

    /**
     * Get elements that match a name.
     * @param aName The name to match
     * @param aProcessor Used to process the search
     * @param aParameters Contain parameters for the symbol search
     */
    @Override
    public void processElementsWithName(@NotNull String aName,
                                        @NotNull Processor<? super NavigationItem> aProcessor,
                                        @NotNull FindSymbolParameters aParameters) {
        List<NavigationItem> lClassDefinition = new LinkedList<>();
        EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(aName, aParameters.getProject());
        if (lClass != null) {
            if (lClass.getClassHeader().getTupleDefinition() != null) {
                lClassDefinition.add((NavigationItem)lClass.getClassHeader().getTupleDefinition());
            } else if (lClass.getClassHeader().getClassNameDefinition() != null) {
                lClassDefinition.add((NavigationItem)lClass.getClassHeader().getClassNameDefinition());
            }
        }
        ContainerUtil.process(lClassDefinition, aProcessor);
    }

}
