package net.libreti.intelleiffel;

import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.psi.PsiElement;
import net.libreti.intelleiffel.parser.Mixins.EiffelClassTypeName;
import net.libreti.intelleiffel.psi.EiffelPsiActuals;
import net.libreti.intelleiffel.psi.EiffelPsiAttachmentMark;
import net.libreti.intelleiffel.psi.EiffelPsiExclamationMark;
import net.libreti.intelleiffel.psi.EiffelPsiFormalArguments;
import net.libreti.intelleiffel.psi.EiffelPsiInterrogationMark;
import net.libreti.intelleiffel.psi.EiffelPsiPlaceholder;
import net.libreti.intelleiffel.psi.EiffelUtil;
import net.libreti.intelleiffel.psi.EiffelPsiEiffelFile;
import net.libreti.intelleiffel.quickFix.EiffelEmptyArgumentsQuickFix;
import net.libreti.intelleiffel.quickFix.EiffelExclamationMarkQuickFix;
import net.libreti.intelleiffel.quickFix.EiffelInterrogationMarkQuickFix;
import org.jetbrains.annotations.NotNull;

/**
 * Annotate elements in the Eiffel Source code
 *
 * @author Louis M
 */
public class EiffelAnnotator implements Annotator {

    /**
     * Annotate class names when the Eiffel file containing this class in not accessible in the project.
     *
     * @param aElement The class type name to annotate
     * @param aHolder Receive every annotation for the element
     */
    private void annotateClassName(EiffelClassTypeName aElement, AnnotationHolder aHolder) {
        EiffelPsiEiffelFile aEiffelFile = EiffelUtil.getInstance().findEiffelClass(
                aElement.getClassNameIdentifier().getIdentifier().getText(), aElement.getProject());
        if (aEiffelFile == null) {
            aHolder.newAnnotation(HighlightSeverity.WARNING, "Class not found")
                    .highlightType(ProblemHighlightType.LIKE_UNKNOWN_SYMBOL).create();
        }
    }

    /**
     * Annotate an interogation mark when used as an obsolete syntax
     *
     * @param aElement The interrogation mark to annotate
     * @param aHolder Receive every annotation for the element
     */
    private void annotateInterrogationMark(EiffelPsiInterrogationMark aElement, AnnotationHolder aHolder) {
        if (aElement.getParent() instanceof EiffelPsiAttachmentMark) {
            aHolder.newAnnotation(HighlightSeverity.WARNING,
                    "Obsolete syntax. Use 'detachable' instead of '?'")
                    .highlightType(ProblemHighlightType.LIKE_DEPRECATED)
                    .withFix(new EiffelInterrogationMarkQuickFix(aElement)).create();
        }
    }

    /**
     * Annotate an empty formal argument
     *
     * @param aElement The formal to annotate
     * @param aHolder Receive every annotation for the element
     */
    private void annotateFormalArgument(EiffelPsiFormalArguments aElement, AnnotationHolder aHolder) {
        if (aElement.getEntityDeclarationList() == null) {
            aHolder.newAnnotation(HighlightSeverity.WARNING,
                    "Obsolete syntax. Remove the () for empty formal argument list")
                    .highlightType(ProblemHighlightType.LIKE_DEPRECATED)
                    .withFix(new EiffelEmptyArgumentsQuickFix(aElement)).create();
        }
    }

    /**
     * Annotate an empty actual parameter
     *
     * @param aElement The actual parameter to annotate
     * @param aHolder Receive every annotation for the element
     */
    private void annotateActuals(EiffelPsiActuals aElement, AnnotationHolder aHolder) {
        if (aElement.getActualList() == null) {
            aHolder.newAnnotation(HighlightSeverity.WARNING,
                    "Obsolete syntax. Remove the () for empty actual parameter list")
                    .highlightType(ProblemHighlightType.LIKE_DEPRECATED)
                    .withFix(new EiffelEmptyArgumentsQuickFix(aElement)).create();
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param aElement The Eiffel Psi element to annotate.
     * @param aHolder  Receive every annotation for the element
     */
    @Override
    public void annotate(@NotNull PsiElement aElement, @NotNull AnnotationHolder aHolder) {
        if (aElement instanceof EiffelClassTypeName lEiffelPsiClassName) {
            annotateClassName(lEiffelPsiClassName, aHolder);
        } else if (aElement instanceof EiffelPsiExclamationMark lExclamationMark) {
            aHolder.newAnnotation(HighlightSeverity.WARNING, "Obsolete syntax. Use 'attached' instead of '!'")
                    .highlightType(ProblemHighlightType.LIKE_DEPRECATED)
                    .withFix(new EiffelExclamationMarkQuickFix(lExclamationMark)).create();
        } else if (aElement instanceof EiffelPsiInterrogationMark lMark) {
            annotateInterrogationMark(lMark, aHolder);
        } else if (aElement instanceof EiffelPsiFormalArguments lArgument) {
            annotateFormalArgument(lArgument, aHolder);
        } else if (aElement instanceof EiffelPsiActuals lActuals) {
            annotateActuals(lActuals, aHolder);
        }
    }

}
