package net.libreti.intelleiffel;

import com.intellij.lang.Commenter;
import org.jetbrains.annotations.Nullable;

/**
 * Indicate to the IntelliJ editor how to comment line in Eiffel
 */
public class EiffelCommenter implements Commenter {

    /**
     * Return the characters used to put a line in comment
     *
     * @return The line comment characters ("--")
     */
    @Override
    public String getLineCommentPrefix() {
        return "--";
    }

    /**
     * Return the characters used to create a block comment (there is no block comment in Eiffel)
     *
     * @return The character to used in block comment (here, empty String since there is no block comment in Eiffel)
     */
    @Override
    public String getBlockCommentPrefix() {
        return "";
    }

    /**
     * Return the characters used to end a block comment (there is no block comment in Eiffel)
     *
     * @return The character to used in block comment (here, null since there is no block comment in Eiffel)
     */
    @Nullable
    @Override
    public String getBlockCommentSuffix() {
        return null;
    }

    /**
     * Return the characters used to start a block comment (there is no block comment in Eiffel)
     *
     * @return The character to used in block comment (here, null since there is no block comment in Eiffel)
     */
    @Nullable
    @Override
    public String getCommentedBlockCommentPrefix() {
        return null;
    }

    /**
     * Return the characters used to end a block comment (there is no block comment in Eiffel)
     *
     * @return The character to used in block comment (here, null since there is no block comment in Eiffel)
     */
    @Nullable
    @Override
    public String getCommentedBlockCommentSuffix() {
        return null;
    }

}
