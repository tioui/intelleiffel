package net.libreti.intelleiffel;

import com.intellij.openapi.fileTypes.LanguageFileType;

import javax.swing.Icon;

/**
 * The type of every Eiffel file.
 *
 * @author Louis M
 */
public class EiffelFileType extends LanguageFileType {

    /**
     * Singleton instance of EiffelFileType.
     */
    private static final EiffelFileType INSTANCE = new EiffelFileType();

    /**
     * Retreive the singleton instance of EiffelFileType.
     *
     * @return The EiffelFileType instance
     */
    public static EiffelFileType getInstance() {
        return INSTANCE;
    }

    /**
     * Creator of the class
     */
    private EiffelFileType() {
        super(EiffelLanguage.getInstance());
    }

    /**
     * The name of an Eiffel file.
     *
     * @return A text that represent the name of an Eiffel file.
     */
    @Override
    public String getName() {
        return "Eiffel file";
    }

    /**
     * The description of an Eiffel file.
     *
     * @return A text that represent the description of an Eiffel file.
     */
    @Override
    public String getDescription() {
        return "Eiffel language file";
    }

    /**
     * The extension (without the dot ".") of an Eiffel file.
     *
     * @return The extension of an Eiffel file.
     */
    @Override
    public String getDefaultExtension() {
        return "e";
    }

    /**
     * The icon to show in the project browser for an Eiffel source file
     *
     * @return The icon of an Eiffel source file.
     */
    @Override
    public Icon getIcon() {
        return EiffelIcons.E_FILE_ICON;
    }


}
