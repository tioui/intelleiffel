package net.libreti.intelleiffel.formatter;

import com.intellij.application.options.CodeStyle;
import com.intellij.formatting.Alignment;
import com.intellij.formatting.Block;
import com.intellij.formatting.ChildAttributes;
import com.intellij.formatting.Indent;
import com.intellij.formatting.Spacing;
import com.intellij.formatting.SpacingBuilder;
import com.intellij.formatting.Wrap;
import com.intellij.formatting.WrapType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.TokenType;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.formatter.common.AbstractBlock;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.tree.TokenSet;
import net.libreti.intelleiffel.EiffelFileType;
import net.libreti.intelleiffel.psi.EiffelTokenSets;
import net.libreti.intelleiffel.psi.EiffelTypes;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A bock used to create code formation in an eiffel code.
 *
 * @author Louis M
 */
public class EiffelBlock extends AbstractBlock {

    /**
     * Used to do the spacing in the object
     */
    private final SpacingBuilder spacingBuilder;

    /**
     * The indent to used in this block
     */
    private Indent indent;

    /**
     * Creator of the object.
     *
     * @param aNode           The node to alling
     * @param aWrap           How to wrap the node
     * @param aAlignment      The alignment type
     * @param aSpacingBuilder The spacing builder used to set the spacing of the node.
     */
    public EiffelBlock(@NotNull ASTNode aNode, @Nullable Wrap aWrap, @Nullable Alignment aAlignment,
                       SpacingBuilder aSpacingBuilder) {
        super(aNode, aWrap, aAlignment);
        spacingBuilder = aSpacingBuilder;
        indent = Indent.getNoneIndent();
    }

    /**
     * Creator of the object.
     *
     * @param aNode           The node to alling
     * @param aWrap           How to wrap the node
     * @param aAlignment      The alignment type
     * @param aSpacingBuilder The spacing builder used to set the spacing of the node.
     * @param aIndent         The indentation to use
     */
    public EiffelBlock(@NotNull ASTNode aNode, @Nullable Wrap aWrap, @Nullable Alignment aAlignment,
                       SpacingBuilder aSpacingBuilder, Indent aIndent) {
        super(aNode, aWrap, aAlignment);
        spacingBuilder = aSpacingBuilder;
        indent = aIndent;
    }

    /**
     * Return the block children of a node
     *
     * @param aElements Every elements to indent
     * @return The block children of the node
     */
    protected List<Block> buildChildrenIndentedFor(IElementType... aElements) {
        List<Block> lResult = new ArrayList<>();
        ASTNode lChild = myNode.getFirstChildNode();
        List<IElementType> lElements = Arrays.asList(aElements);
        while (lChild != null) {
            if (lElements.contains(lChild.getElementType())) {
                lResult.add(new EiffelBlock(lChild, Wrap.createWrap(WrapType.NONE, false), null,
                        spacingBuilder, Indent.getNormalIndent()));
            } else {
                buildChildrenDefaultIndent(lResult, lChild);
            }
            lChild = lChild.getTreeNext();
        }
        return lResult;
    }

    /**
     * Remove depilcation code from buildChildren* code
     *
     * @param aResult The children of this object, accumulated since now
     * @param aChild  The child that is manage in this iteration.
     */
    private void buildChildrenDefaultIndent(List<Block> aResult, ASTNode aChild) {
        if (aChild.getElementType() != TokenType.WHITE_SPACE) {
            aResult.add(new EiffelBlock(aChild, Wrap.createWrap(WrapType.NONE, false), null,
                    spacingBuilder));
        }
    }

    /**
     * Return the block children of a node with indented element with a number of space indentation
     *
     * @param aSpaces   The number of space to indent
     * @param aElements Every elements to indent
     * @return The block children of the node
     */
    protected List<Block> buildChildrenIndentedSpaces(int aSpaces, IElementType... aElements) {
        List<Block> lResult = new ArrayList<>();
        ASTNode lChild = myNode.getFirstChildNode();
        List<IElementType> lElements = Arrays.asList(aElements);
        while (lChild != null) {
            if (lElements.contains(lChild.getElementType())) {
                lResult.add(new EiffelBlock(lChild, Wrap.createWrap(WrapType.NONE, false), null,
                        spacingBuilder, Indent.getSpaceIndent(aSpaces)));
            } else buildChildrenDefaultIndent(lResult, lChild);
            lChild = lChild.getTreeNext();
        }
        return lResult;
    }

    /**
     * Return the block children of a node with indented element with a doublen indentation
     *
     * @param aElements Every elements to indent
     * @return The block children of the node
     */
    protected List<Block> buildChildrenDoubleIndentFor(IElementType... aElements) {
        CodeStyleSettings settings = CodeStyle.getSettings(myNode.getPsi().getProject());
        int indentSize = settings.getIndentSize(EiffelFileType.getInstance());
        return buildChildrenIndentedSpaces(indentSize * 2, aElements);
    }

    /**
     * Return the block children of an EiffelClass node
     *
     * @return The block children of the node
     */
    protected List<Block> buildChildrenEiffelEiffelFile() {
        List<Block> lResult = new ArrayList<>();
        CodeStyleSettings settings = CodeStyle.getSettings(myNode.getPsi().getProject());
        int indentSize = settings.getIndentSize(EiffelFileType.getInstance());
        ASTNode lChild = myNode.getFirstChildNode();
        boolean lLastAttributeDetected = false;
        while (lChild != null) {
            if (lLastAttributeDetected && lChild.getElementType() == EiffelTypes.EIF_LINE_COMMENT) {
                lResult.add(new EiffelBlock(lChild, Wrap.createWrap(WrapType.NONE, false), null,
                        spacingBuilder, Indent.getSpaceIndent(indentSize * 3)));
            } else if (lChild.getElementType() == EiffelTypes.EIF_FEATURES) {
                lResult.add(new EiffelBlock(lChild, Wrap.createWrap(WrapType.NONE, false), null,
                        spacingBuilder));
                // I know this is ugly, but I did not find any better way to manage last attribute documentation
                lLastAttributeDetected = false;
                ASTNode[] lFeatureClause = lChild.getChildren(EiffelTokenSets.FEATURE_CLAUSE);
                if (lFeatureClause.length > 0) {
                    ASTNode[] lDeclarationList = lFeatureClause[lFeatureClause.length - 1].getChildren(
                            EiffelTokenSets.FEATURE_DECLARATION_LIST);
                    if (lDeclarationList.length > 0) {
                        ASTNode[] lDeclarations = lDeclarationList[lDeclarationList.length - 1].getChildren(
                                EiffelTokenSets.FEATURE_DECLARATION);
                        if (lDeclarations.length > 0) {
                            ASTNode[] lDeclarationBodies = lDeclarations[lDeclarations.length - 1].getChildren(
                                    EiffelTokenSets.DECLARATION_BODY);
                            if (lDeclarationBodies.length > 0) {
                                ASTNode[] lQueryMark = lDeclarationBodies[lDeclarationBodies.length - 1].getChildren(
                                        EiffelTokenSets.QUERY_MARK);
                                lLastAttributeDetected = lQueryMark.length > 0;
                            }
                        }
                    }
                }
            } else {
                buildChildrenDefaultIndent(lResult, lChild);
            }
            lChild = lChild.getTreeNext();
        }
        return lResult;
    }

    /**
     * Return the block children of a node
     *
     * @return The block children of the node
     */
    @Override
    protected List<Block> buildChildren() {
        List<Block> lResult;
        if (myNode.getElementType() == EiffelTypes.EIF_FEATURE_CLAUSE) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_FEATURE_DECLARATION_LIST);
        } else if (myNode.getElementType() == EiffelTypes.EIF_FEATURE_DECLARATION) {
            lResult = buildChildrenDoubleIndentFor(EiffelTypes.EIF_LINE_COMMENT);
        } else if (myNode.getElementType() == EiffelTypes.EIF_EIFFEL_FILE) {
            lResult = buildChildrenEiffelEiffelFile();
        } else if (myNode.getElementType() == EiffelTypes.EIF_NOTES) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_NOTE_LIST);
        } else if (myNode.getElementType() == EiffelTypes.EIF_CLASS_HEADER) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_CLASS_NAME_DEFINITION);
        } else if (myNode.getElementType() == EiffelTypes.EIF_OBSOLETE) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_MESSAGE);
        } else if (myNode.getElementType() == EiffelTypes.EIF_INHERIT_CLAUSE) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_PARENT_LIST);
        } else if (myNode.getElementType() == EiffelTypes.EIF_PARENT) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_FEATURE_ADAPTATION);
        } else if (myNode.getElementType() == EiffelTypes.EIF_UNDEFINE ||
                myNode.getElementType() == EiffelTypes.EIF_REDEFINE ||
                myNode.getElementType() == EiffelTypes.EIF_SELECT ||
                myNode.getElementType() == EiffelTypes.EIF_NEW_EXPORT_ITEM ||
                myNode.getElementType() == EiffelTypes.EIF_ONLY ||
                myNode.getElementType() == EiffelTypes.EIF_CREATION_CLAUSE ||
                myNode.getElementType() == EiffelTypes.EIF_CONSTRAINT_CREATORS) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_FEATURE_LIST);
        } else if (myNode.getElementType() == EiffelTypes.EIF_RENAME) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_RENAME_LIST);
        } else if (myNode.getElementType() == EiffelTypes.EIF_NEW_EXPORT_LIST) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_NEW_EXPORT_ITEM);
        } else if (myNode.getElementType() == EiffelTypes.EIF_CONVERTER) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_CONVERTER_LIST);
        } else if (myNode.getElementType() == EiffelTypes.EIF_INVARIANT ||
                myNode.getElementType() == EiffelTypes.EIF_PRECONDITION ||
                myNode.getElementType() == EiffelTypes.EIF_POSTCONDITION ||
                myNode.getElementType() == EiffelTypes.EIF_CHECK) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_ASSERTION);
        } else if (myNode.getElementType() == EiffelTypes.EIF_FEATURE_VALUE) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_LINE_COMMENT, EiffelTypes.EIF_NOTES, EiffelTypes.EIF_OBSOLETE,
                    EiffelTypes.EIF_ATTRIBUTE_OR_ROUTINE);
        } else if (myNode.getElementType() == EiffelTypes.EIF_LOCAL_DECLARATIONS) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_ENTITY_DECLARATION_LIST);
        } else if (myNode.getElementType() == EiffelTypes.EIF_DECLARATION_BODY_VALUE) {
            lResult = buildChildrenDoubleIndentFor(EiffelTypes.EIF_LINE_COMMENT);
        } else if (myNode.getElementType() == EiffelTypes.EIF_RESCUE ||
                myNode.getElementType() == EiffelTypes.EIF_ATTRIBUTE ||
                myNode.getElementType() == EiffelTypes.EIF_INTERNAL ||
                myNode.getElementType() == EiffelTypes.EIF_ELSE_PART ||
                myNode.getElementType() == EiffelTypes.EIF_INITIALIZATION ||
                myNode.getElementType() == EiffelTypes.EIF_LOOP_BODY ||
                myNode.getElementType() == EiffelTypes.EIF_LOOP_SYMBOLIC ||
                myNode.getElementType() == EiffelTypes.EIF_DEBUG) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_COMPOUND);
        } else if (myNode.getElementType() == EiffelTypes.EIF_EXTERNAL_ROUTINE) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_EXTERNAL_LANGUAGE);
        } else if (myNode.getElementType() == EiffelTypes.EIF_EXTERNAL_NAME) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_STRING);
        } else if (myNode.getElementType() == EiffelTypes.EIF_WHEN_PART) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_CHOICES, EiffelTypes.EIF_COMPOUND);
        } else if (myNode.getElementType() == EiffelTypes.EIF_THEN_PART) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_BOOLEAN_EXPRESSION, EiffelTypes.EIF_COMPOUND);
        } else if (myNode.getElementType() == EiffelTypes.EIF_THEN_PART_EXPRESSION) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_BOOLEAN_EXPRESSION, EiffelTypes.EIF_EXPRESSION);
        } else if (myNode.getElementType() == EiffelTypes.EIF_CONDITIONAL_EXPRESSION) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_EXPRESSION);
        } else if (myNode.getElementType() == EiffelTypes.EIF_LOOP_SYMBOLIC_EXPRESSION ||
                myNode.getElementType() == EiffelTypes.EIF_EXIT_CONDITION ||
                myNode.getElementType() == EiffelTypes.EIF_LOOP_BODY_EXPRESSION) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_BOOLEAN_EXPRESSION);
        } else if (myNode.getElementType() == EiffelTypes.EIF_MULTI_BRANCH ||
                myNode.getElementType() == EiffelTypes.EIF_ACROSS_KEYWORD ||
                myNode.getElementType() == EiffelTypes.EIF_VARIANT) {
            lResult = buildChildrenIndentedFor(EiffelTypes.EIF_EXPRESSION);
        } else {
            lResult = buildChildrenIndentedFor();
        }
        return lResult;
    }

    /**
     * The default child indent.
     *
     * @return The indent
     */
    @Override
    protected @Nullable Indent getChildIndent() {
        return Indent.getNormalIndent();
    }

    /**
     * Create a child attributes from an indentation and an alignment for a certain child of this
     *
     * @param lNewChildIndex the index where a new child is inserted.
     * @return The new child attributs
     */
    @Override
    public @NotNull ChildAttributes getChildAttributes(int lNewChildIndex) {
        ChildAttributes lResult;
        if (myNode.getElementType() == EiffelTypes.EIF_EIFFEL_FILE &&
                ((EiffelBlock)getSubBlocks().get(lNewChildIndex - 1)).getNode().getElementType() ==
                        EiffelTypes.EIF_CLASS_HEADER) {
            lResult = new ChildAttributes(Indent.getNoneIndent(), null);

        } else if (myNode.getElementType() == EiffelTypes.EIF_COMPOUND) {
            lResult = new ChildAttributes(Indent.getNoneIndent(), null);
        } else {
            lResult = super.getChildAttributes(lNewChildIndex);
        }
        return lResult;
    }

    /**
     * Retreive the indentation to used in the object (always equal to `indent`)
     *
     * @return The value of `indent`
     */
    @Override
    public @Nullable Indent getIndent() {
        return indent;
    }

    /**
     * Get the spacing to use between two blocks
     *
     * @param child1 the first child for which spacing is requested;
     *               {@code null} if given {@code 'child2'} block is the first document block
     * @param child2 the second child for which spacing is requested.
     * @return The spacing to used
     */
    @Nullable
    @Override
    public Spacing getSpacing(@Nullable Block child1, @NotNull Block child2) {
        return spacingBuilder.getSpacing(this, child1, child2);
    }

    /**
     * Is the block a leaf in the block tree
     *
     * @return True if the block is a leaf. False if not.
     */
    @Override
    public boolean isLeaf() {
        return myNode.getFirstChildNode() == null;
    }

}
