package net.libreti.intelleiffel.formatter;

import com.intellij.formatting.FormattingContext;
import com.intellij.formatting.FormattingModel;
import com.intellij.formatting.FormattingModelBuilder;
import com.intellij.formatting.FormattingModelProvider;
import com.intellij.formatting.SpacingBuilder;
import com.intellij.formatting.Wrap;
import com.intellij.formatting.WrapType;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import net.libreti.intelleiffel.EiffelLanguage;
import net.libreti.intelleiffel.psi.EiffelTypes;
import org.jetbrains.annotations.NotNull;

/**
 * Use to format Eiffel file
 *
 * @author Louis M
 */
public class EiffelFormattingModelBuilder implements FormattingModelBuilder {

    /**
     * Create the Spacing builder to manage spacing between elements
     *
     * @param aSettings The code style settings of the builder
     * @return A new spacing builder.
     */
    private static SpacingBuilder createSpaceBuilder(CodeStyleSettings aSettings) {
        return new SpacingBuilder(aSettings, EiffelLanguage.getInstance())
                .around(EiffelTypes.EIF_ASSIGN)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_ASSIGNMENT_OPERATORS)
                .around(EiffelTypes.EIF_AND_KEYWORD)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_LOGICAL_OPERATORS)
                .around(EiffelTypes.EIF_OR_KEYWORD)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_LOGICAL_OPERATORS)
                .around(EiffelTypes.EIF_NOT_KEYWORD)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_LOGICAL_OPERATORS)
                .around(EiffelTypes.EIF_BINARY_AND_THEN)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_LOGICAL_OPERATORS)
                .around(EiffelTypes.EIF_BINARY_OR_ELSE)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_LOGICAL_OPERATORS)
                .around(EiffelTypes.EIF_IMPLIES_KEYWORD)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_LOGICAL_OPERATORS)
                .around(EiffelTypes.EIF_LOWER_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_RELATIONAL_OPERATORS)
                .around(EiffelTypes.EIF_LOWER_EQUAL_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_RELATIONAL_OPERATORS)
                .around(EiffelTypes.EIF_GREATER_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_RELATIONAL_OPERATORS)
                .around(EiffelTypes.EIF_GREATER_EQUAL_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_RELATIONAL_OPERATORS)
                .around(EiffelTypes.EIF_EQUAL_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_EQUALITY_OPERATORS)
                .around(EiffelTypes.EIF_NOT_EQUAL_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_EQUALITY_OPERATORS)
                .around(EiffelTypes.EIF_EQUIVALENT_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_EQUALITY_OPERATORS)
                .around(EiffelTypes.EIF_NOT_EQUIVALENT_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_EQUALITY_OPERATORS)
                .around(EiffelTypes.EIF_AND_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_BITWISE_OPERATORS)
                .around(EiffelTypes.EIF_OR_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_BITWISE_OPERATORS)
                .around(EiffelTypes.EIF_PLUS_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_ADDITIVE_OPERATORS)
                .around(EiffelTypes.EIF_MINUS_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_ADDITIVE_OPERATORS)
                .around(EiffelTypes.EIF_STAR_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_MULTIPLICATIVE_OPERATORS)
                .around(EiffelTypes.EIF_DIVISION_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_MULTIPLICATIVE_OPERATORS)
                .around(EiffelTypes.EIF_SLASH_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_MULTIPLICATIVE_OPERATORS)
                .around(EiffelTypes.EIF_MODULO_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_MULTIPLICATIVE_OPERATORS)
                .around(EiffelTypes.EIF_LEFT_SHIFT_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_SHIFT_OPERATORS)
                .around(EiffelTypes.EIF_RIGHT_SHIFT_OPERATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AROUND_SHIFT_OPERATORS)
                .after(EiffelTypes.EIF_COMMA_SEPARATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AFTER_COMMA)
                .before(EiffelTypes.EIF_COMMA_SEPARATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_BEFORE_COMMA)
                .after(EiffelTypes.EIF_SEMI_COLON_SEPARATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_AFTER_SEMICOLON)
                .before(EiffelTypes.EIF_SEMI_COLON_SEPARATOR)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_BEFORE_SEMICOLON)
                .after(EiffelTypes.EIF_LEFT_PARENTHESE)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_WITHIN_PARENTHESES)
                .before(EiffelTypes.EIF_RIGHT_PARENTHESE)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_WITHIN_PARENTHESES)
                .after(EiffelTypes.EIF_LEFT_BRACE)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_WITHIN_BRACES)
                .before(EiffelTypes.EIF_RIGHT_BRACE)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_WITHIN_BRACES)
                .after(EiffelTypes.EIF_LEFT_BRACKET)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_WITHIN_BRACKETS)
                .before(EiffelTypes.EIF_RIGHT_BRACKET)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_WITHIN_BRACKETS)
                .before(EiffelTypes.EIF_ACTUALS)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_BEFORE_METHOD_CALL_PARENTHESES)
                .before(EiffelTypes.EIF_FORMAL_ARGUMENTS)
                .spaceIf(aSettings.getCommonSettings(
                        EiffelLanguage.getInstance().getID()).SPACE_BEFORE_METHOD_PARENTHESES);
    }

    /**
     * Create the formatting model for Eiffel files
     *
     * @param aFormattingContext The context of the formating
     *
     * @return A new formating model
     */
    @Override
    public @NotNull FormattingModel createModel(@NotNull FormattingContext aFormattingContext) {
        CodeStyleSettings codeStyleSettings = aFormattingContext.getCodeStyleSettings();
        return FormattingModelProvider
                .createFormattingModelForPsiFile(aFormattingContext.getContainingFile(),
                        new EiffelBlock(aFormattingContext.getNode(),
                                Wrap.createWrap(WrapType.NONE, false),
                                null,
                                createSpaceBuilder(codeStyleSettings)),
                        codeStyleSettings);
    }

}
