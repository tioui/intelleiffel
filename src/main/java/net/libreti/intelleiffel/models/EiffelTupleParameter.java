package net.libreti.intelleiffel.models;

import net.libreti.intelleiffel.EiffelIcons;
import net.libreti.intelleiffel.parser.Mixins.EiffelType;
import net.libreti.intelleiffel.psi.EiffelPsiTupleIdentifier;
import org.jetbrains.annotations.NotNull;

import javax.swing.Icon;

/**
 * Represent a single parameter of a tuple
 *
 * @author Louis M
 */
public class EiffelTupleParameter extends EiffelAddressable {
    /**
     * The tuple parameter identifier
     */
    EiffelPsiTupleIdentifier identifier;

    /**
     * The Eiffel type of the parameter
     */
    EiffelType type;

    /**
     * Constructor of the object.
     * @param aIdentifier The parameter identifier of the parameter
     * @param aType The Eiffel type of the parameter
     */
    public EiffelTupleParameter(EiffelPsiTupleIdentifier aIdentifier, EiffelType aType) {
        identifier = aIdentifier;
        type = aType;
    }

    /**
     * The return type of the parameter
     *
     * @return The return type of the parameter
     */
    @Override
    public @NotNull EiffelType getReturnType() {
        return type;
    }

    /**
     * The icon of the current parameter.
     *
     * @param unused Used to mean if open/close icons for tree renderer. No longer in use.
     * @return The icon of the current object.
     */
    @Override
    public @NotNull Icon getIcon(boolean unused) {
        return EiffelIcons.LOCAL_ENTITY_ICON;
    }

    /**
     * The name of the parameter
     *
     * @return the name of the parameter
     */
    @Override
    public @NotNull String getName() {
        return identifier.getIdentifier().getText();
    }
}
