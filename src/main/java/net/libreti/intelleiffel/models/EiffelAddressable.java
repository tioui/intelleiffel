package net.libreti.intelleiffel.models;

import com.intellij.navigation.ItemPresentation;
import net.libreti.intelleiffel.parser.Mixins.EiffelType;
import org.jetbrains.annotations.Nullable;

public abstract  class EiffelAddressable implements ItemPresentation, EiffelNamable {

    /**
     * A text the represent the current object.
     *
     * @return A text the represent the current object.
     */
    @Override
    public @Nullable String getPresentableText() {
        return getName();
    }

    /**
     * If there is a return type in the feature, return the type.
     *
     * @return The return type of the feature if any. Null if no return type.
     */
    public abstract @Nullable EiffelType getReturnType();

}
