package net.libreti.intelleiffel.models;

import com.intellij.psi.PsiComment;
import com.intellij.psi.PsiElement;
import net.libreti.intelleiffel.EiffelIcons;
import net.libreti.intelleiffel.parser.Mixins.EiffelAlias;
import net.libreti.intelleiffel.parser.Mixins.EiffelClass;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureClause;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureList;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureName;
import net.libreti.intelleiffel.parser.Mixins.EiffelType;
import net.libreti.intelleiffel.psi.EiffelFile;
import net.libreti.intelleiffel.psi.EiffelPsiAlias;
import net.libreti.intelleiffel.psi.EiffelPsiAssertionClause;
import net.libreti.intelleiffel.psi.EiffelPsiDeclarationBody;
import net.libreti.intelleiffel.psi.EiffelPsiDeclarationBodyValue;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureBody;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureDeclaration;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureDeclarationList;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureValue;
import net.libreti.intelleiffel.psi.EiffelPsiFormalArguments;
import net.libreti.intelleiffel.psi.EiffelPsiInternal;
import net.libreti.intelleiffel.psi.EiffelPsiPostcondition;
import net.libreti.intelleiffel.psi.EiffelPsiQueryMark;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A feature with a single name
 *
 * @author Louis M
 */
public class EiffelFeature extends EiffelAddressable {

    /**
     * Retreive the feature name Psi element.
     *
     * @return the element
     */
    public @NotNull EiffelFeatureName getFeatureName() {
        return featureName;
    }

    /**
     * Retreive the declaration body Psi element.
     *
     * @return the element
     */
    public @NotNull EiffelPsiDeclarationBody getDeclarationBody() {
        return declarationBody;
    }

    /**
     * The name of the feature
     */
    @NotNull EiffelFeatureName featureName;

    /**
     * The body of the feature declaration
     */
    @NotNull EiffelPsiDeclarationBody declarationBody;

    /**
     * Constructor of the object
     *
     * @param aName The Feature name of the feature
     * @param aBody The declaration body of the feature
     */
    public EiffelFeature(@NotNull EiffelFeatureName aName, @NotNull EiffelPsiDeclarationBody aBody) {
        featureName = aName;
        declarationBody = aBody;
        aliasesOverride = null;
        rename = null;
    }

    /**
     * If there is a rename (in inheritance) attached to the feature.
     */
    private @Nullable String rename;

    /**
     * Return a rename (in inheritance) attached to the feature, if any.
     * @return Return the rename string, or null if there is no rename.
     */
    public String getRename() {
        return rename;
    }

    /**
     * Assing a rename (in inheritance) to the feature.
     *
     * @param aRename A rename (or null)
     */
    public void setRename(@Nullable String aRename) {
        rename = aRename;
    }

    /**
     * Replace the aliases when the feature is renamed (on inheritance, mainly)
     */
    private @Nullable List<EiffelAlias> aliasesOverride;

    /**
     * Retreive the aliases replacement.
     *
     * @return the aliases
     */
    public @Nullable List<EiffelAlias> getAliasesOverride() {
        return aliasesOverride != null ? new ArrayList<>(aliasesOverride) : null;
    }

    /**
     * Override the original aliases with new aliases
     * @param aAliasesOverride the new aliases
     */
    public void setAliasesOverride(@Nullable List<EiffelAlias> aAliasesOverride) {
        aliasesOverride = aAliasesOverride;
    }

    /**
     * The aliased of the feature, as stated in the original source code.
     *
     * @return the aliases.
     */
    public List<EiffelAlias> getOriginalAliases() {
        List<EiffelAlias> lResult = new LinkedList<>();
        for (EiffelPsiAlias lAlias : featureName.getExtendedFeatureName().getAliasList()) {
            lResult.add((EiffelAlias) lAlias);
        }
        return lResult;
    }

    /**
     * The name of the original feature (if inherited)
     *
     * @return the original name of the feature
     */
    public String getOriginalName() {
        return featureName.getExtendedFeatureName().getIdentifier().getText();
    }

    /**
     * The name of the feature
     *
     * @return the name of the feature
     */
    @Override
    public String getName() {
        String lResult;
        if (rename != null) {
            lResult = rename;
        } else {
            lResult = getOriginalName();
        }
        return lResult;
    }

    /**
     * The feature is declared as frozen
     *
     * @return True if the feature is frozen. False if not.
     */
    public boolean isFrozen() {
        return featureName.getFrozenKeyword() != null;
    }

    /**
     * The feature is a routine
     *
     * @return True if the feature is a routine. False if not.
     */
    public boolean isRoutine() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                lResult = lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody().getEffectiveRoutine() != null;
            }
        }
        return lResult;
    }

    /**
     * The feature is a function
     *
     * @return True if the feature is a function. False if not.
     */
    public boolean isFunction() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                lResult =
                        lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody().getEffectiveRoutine() != null
                                && lValue.getQueryMark() != null;
            }
        }
        return lResult;
    }

    /**
     * The feature is a prodecude
     *
     * @return True if the feature is a procedure. False if not.
     */
    public boolean isProcedure() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                lResult =
                        lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody().getEffectiveRoutine() != null
                                && lValue.getQueryMark() == null;
            }
        }
        return lResult;
    }

    /**
     * The feature is a routine
     *
     * @return True if the feature is a routine. False if not.
     */
    public boolean hasAttributeRoutine() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                lResult = lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody().getAttribute() != null;
            }
        }
        return lResult;
    }

    /**
     * The feature is an external routine
     *
     * @return True if the feature is an external routine. False if not.
     */
    public boolean isExternal() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                EiffelPsiFeatureBody lBody = lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody();
                if (lBody.getEffectiveRoutine() != null) {
                    lResult = lBody.getEffectiveRoutine().getExternalRoutine() != null;
                }
            }
        }
        return lResult;
    }

    /**
     * The feature is a once feature
     *
     * @return True if the feature is a once feature. False if not.
     */
    public boolean isOnce() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                EiffelPsiFeatureBody lBody = lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody();
                if (lBody.getEffectiveRoutine() != null) {
                    EiffelPsiInternal lInternal = lBody.getEffectiveRoutine().getInternal();
                    if (lInternal != null) {
                        lResult = lInternal.getRoutineMark().getOnce() != null;
                    }
                }

            }
        }
        return lResult;
    }

    /**
     * Is the feature obsolete
     *
     * @return True if the feature is obsolete. False if not.
     */
    public boolean isObsolete() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
        if (lValue != null) {
            lResult = lValue.getFeatureValue().getObsolete() != null;
        }
        return lResult;
    }

    /**
     * Is the feature deferred.
     *
     * @return True if the feature is deferred. False if not.
     */
    public boolean isDeferred() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                lResult =
                        lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody().getDeferredKeyword() != null;
            }
        }
        return lResult;
    }

    /**
     * Is the feature an instance free routine.
     *
     * @return True if the feature is an instance free routine. False if not.
     */
    public boolean isInstanceFreeRoutine() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                EiffelPsiPostcondition lPostcondition =
                        lValue.getFeatureValue().getAttributeOrRoutine().getPostcondition();
                if (lPostcondition != null && lPostcondition.getAssertion() != null) {
                    for (EiffelPsiAssertionClause lClause : lPostcondition.getAssertion().getAssertionClauseList()) {
                        if (lClause.getUnlabeledAssertionClause() != null) {
                            lResult = lClause.getUnlabeledAssertionClause().getClassKeyword() != null;
                        }
                    }
                }
                lResult =
                        lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody().getDeferredKeyword() != null;
            }
        }
        return lResult;
    }

    /**
     * Is the feature deferred.
     *
     * @return True if the feature is deferred. False if not.
     */
    public boolean isConstant() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
        if (lValue != null) {
            lResult = lValue.getFeatureValue().getExplicitValue() != null;
        }
        return lResult;
    }

    /**
     * Is the feature deferred.
     *
     * @return True if the feature is deferred. False if not.
     */
    public boolean isAttribute() {
        return declarationBody.getQueryMark() != null || hasAttributeRoutine();
    }

    /**
     * If there is an assigner in the feature, return the name.
     *
     * @return The name of the assigner of the feature if any. Null if no assigner.
     */
    public @Nullable String getAssignerName() {
        String lResult = null;
        EiffelPsiQueryMark lMark = declarationBody.getQueryMark();
        if (lMark == null) {
            EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
            if (lValue != null) {
                lMark = lValue.getQueryMark();
            }
        }
        if (lMark != null) {
            if (lMark.getAssignerMark() != null) {
                lResult = lMark.getAssignerMark().getFeatureName().getIdentifier().getText();
            }
        }
        return lResult;
    }

    /**
     * Get the name of the class that contain this feature.
     *
     * @return the name of the class
     */
    public @Nullable String getContainingClassName() {
        String lResult = null;
        if (featureName.getContainingFile() instanceof EiffelFile lEiffelFile) {
            EiffelClass lClass = EiffelUtil.getInstance().findClassInFile(lEiffelFile);
            if (lClass != null) {
                lResult = lClass.getName();
            }
        }
        return lResult;
    }

    /**
     * If there is a return type in the feature, return the type.
     *
     * @return The return type of the feature if any. Null if no return type.
     */
    public @Nullable EiffelType getReturnType() {
        EiffelType lResult = null;
        EiffelPsiQueryMark lMark = declarationBody.getQueryMark();
        if (lMark == null) {
            EiffelPsiDeclarationBodyValue lValue = declarationBody.getDeclarationBodyValue();
            if (lValue != null) {
                lMark = lValue.getQueryMark();
            }
        }
        if (lMark != null) {
            lResult = ((EiffelType)lMark.getTypeMark().getType());
        }
        return lResult;
    }

    /**
     * The documentation comment (without the comment tag) of the feature attribute
     *
     * @return the documentation
     */
    private @NotNull String getAttributeDocumentation() {
        StringBuilder lResult = new StringBuilder();
        if (declarationBody.getParent() instanceof EiffelPsiFeatureDeclaration lDeclaration) {
            if (lDeclaration.getParent() instanceof EiffelPsiFeatureDeclarationList lDeclarationList) {
                PsiElement[] lElements = lDeclarationList.getChildren();
                int lIndex = -1;
                for (int i = 0; i < lElements.length && lIndex < 0; i++) {
                    if (lElements[i] == lDeclaration) {
                        lIndex = i;
                    }
                }
                boolean lNextDeclarationFound = false;
                if (lIndex >= 0) {
                    for (int i = lIndex + 1; i < lElements.length && !lNextDeclarationFound; i++) {
                        if (lElements[i] instanceof PsiComment lComment) {
                            lResult.append(lComment.getText().replaceAll("\\\\ ", " ").
                                    substring(2).trim());
                        } else {
                            lNextDeclarationFound = true;
                        }
                    }
                }
                if (lDeclarationList.getFeatureDeclarationList().
                        get(lDeclarationList.getFeatureDeclarationList().size() - 1) == lDeclaration) {
                    if (lDeclarationList.getParent() instanceof EiffelFeatureClause lFeatureClause) {
                        if (lFeatureClause.getParent() instanceof EiffelFeatureList lFeatureList) {
                            if (lFeatureList.getFeatureClauseList().
                                    get(lFeatureList.getFeatureClauseList().size() - 1) == lFeatureClause) {
                                if (lFeatureList.getParent() instanceof EiffelClass lClass) {
                                    PsiElement[] lFileChildren = lClass.getChildren();
                                    int i = 0;
                                    while (i<lFileChildren.length && lFileChildren[i] != lFeatureList) {
                                        i = i + 1;
                                    }
                                    while (i<lFileChildren.length && lFileChildren[i] instanceof PsiComment lComment) {
                                        lResult.append(lComment.getText().replaceAll("\\\\ ", " ").
                                                substring(2).trim());
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
        return lResult.toString();
    }

    /**
     * The documentation comment (without the comment tag) of the feature
     *
     * @return the documentation
     */
    public @NotNull String getDocumentation() {
        String lResult;
        if (declarationBody.getQueryMark() != null) {
            lResult = getAttributeDocumentation();
        } else if (declarationBody.getDeclarationBodyValue() != null) {
            StringBuilder lBuilder = new StringBuilder();
            int lCount = 0;
            PsiElement[] lElements = declarationBody.getDeclarationBodyValue().getChildren();
            if (lElements[lCount] instanceof EiffelPsiFormalArguments) {
                lCount = lCount + 1;
            }
            if (lElements[lCount] instanceof EiffelPsiQueryMark) {
                lCount = lCount + 1;
            }
            boolean lNextDeclarationFound = false;
            for (int i = lCount; i < lElements.length && !lNextDeclarationFound; i++) {
                if (lElements[i] instanceof PsiComment lComment) {
                    lBuilder.append(lComment.getText().replaceAll("\\\\ ", " ").
                            substring(2).trim());
                } else if (lElements[i] instanceof EiffelPsiFeatureValue) {
                    lNextDeclarationFound = true;
                }
            }
            lResult = lBuilder.toString();
        } else {
            lResult = "";
        }
        return lResult;
    }

    /**
     * The icon of the current class.
     *
     * @param unused Used to mean if open/close icons for tree renderer. No longer in use.
     * @return The icon of the current object.
     */
    @Override
    public @NotNull Icon getIcon(boolean unused) {
        Icon lResult = EiffelIcons.FEATURE_METHOD_ICON;
        if (isObsolete()) {
            lResult = EiffelIcons.FEATURE_OBSOLETE_ICON;
        } else if (isDeferred()) {
            if (isInstanceFreeRoutine()) {
                lResult = EiffelIcons.FEATURE_DEFERRED_INSTANCE_FREE_ICON;
            } else {
                lResult = EiffelIcons.FEATURE_DEFERRED_ICON;
            }
        } else if (isConstant()) {
            lResult = EiffelIcons.FEATURE_CONSTANT_ICON;
        } else if (isExternal()) {
            lResult = EiffelIcons.FEATURE_EXTERNAL_ICON;
        } else if (isFrozen()) {
            if (isOnce()) {
                lResult = EiffelIcons.FEATURE_FROZEN_ONCE_ICON;
            } else {
                lResult = EiffelIcons.FEATURE_FROZEN_ICON;
            }
        } else if (isOnce()) {
            lResult = EiffelIcons.FEATURE_ONCE_ICON;
        } else if (isInstanceFreeRoutine()) {
            lResult = EiffelIcons.FEATURE_INSTANCE_FREE_ICON;
        } else if (isAttribute()) {
            lResult = EiffelIcons.FEATURE_ATTRIBUTE_ICON;
        }
        return lResult;
    }
}
