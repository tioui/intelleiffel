package net.libreti.intelleiffel.models;

import org.jetbrains.annotations.NotNull;

/**
 * Element that have a name.
 *
 * @author Louis M
 */
public interface EiffelNamable {

    /**
     * The name of the element
     *
     * @return the name text
     */
    @NotNull String getName();
}
