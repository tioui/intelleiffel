package net.libreti.intelleiffel.models;

/**
 * A note documentation in an Eiffel class file
 *
 * @author Louis M
 */
public class EiffelNoteDocumentation {


    /**
     * The identifier of the note documentation
     */
    private String id;

    /**
     * The documentation message
     */
    private String message;

    /**
     * Retrieve the identifier of the documentation note
     *
     * @return the identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Retrieve the message of the documentation note
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Creator of the object
     *
     * @param aId the identifier of the note
     * @param aMessage the documentation message of the note
     */
    public EiffelNoteDocumentation(String aId, String aMessage) {
        id = aId;
        message = aMessage;
    }
}
