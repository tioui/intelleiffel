package net.libreti.intelleiffel.models;

import net.libreti.intelleiffel.EiffelIcons;
import net.libreti.intelleiffel.parser.Mixins.EiffelDeclarationIdentifier;
import net.libreti.intelleiffel.parser.Mixins.EiffelType;
import org.jetbrains.annotations.NotNull;

import javax.swing.Icon;

public class EiffelEntityDeclaration extends EiffelAddressable {

    /**
     * The entity identifier
     */
    EiffelDeclarationIdentifier identifier;

    /**
     * The Eiffel type of the entity
     */
    EiffelType type;

    /**
     * Constructor of the object.
     * @param aIdentifier The parameter identifier of the entity
     * @param aType The Eiffel type of the entity
     */
    public EiffelEntityDeclaration(EiffelDeclarationIdentifier aIdentifier, EiffelType aType) {
        identifier = aIdentifier;
        type = aType;
    }

    /**
     * The eiffel Type of the entity
     *
     * @return The return type of the entity
     */
    @Override
    public @NotNull EiffelType getReturnType() {
        return type;
    }

    /**
     * The icon of the current entity.
     *
     * @param unused Used to mean if open/close icons for tree renderer. No longer in use.
     * @return The icon of the current object.
     */
    @Override
    public @NotNull Icon getIcon(boolean unused) {
        return EiffelIcons.LOCAL_ENTITY_ICON;
    }

    /**
     * The name of the entity
     *
     * @return the name of the entity
     */
    @Override
    public @NotNull String getName() {
        return identifier.getIdentifier().getText();
    }

}
