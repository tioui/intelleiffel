package net.libreti.intelleiffel.usage;

import com.intellij.lang.cacheBuilder.DefaultWordsScanner;
import com.intellij.lang.cacheBuilder.WordsScanner;
import com.intellij.lang.findUsages.FindUsagesProvider;
import com.intellij.psi.PsiElement;
import net.libreti.intelleiffel.lexer.EiffelLexerAdapter;
import net.libreti.intelleiffel.parser.Mixins.EiffelClassNameRenamable;
import net.libreti.intelleiffel.psi.EiffelTokenSets;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Provide Eiffel elements usage listing.
 *
 * @author Louis M
 */
final public class EiffelUsageProvider implements FindUsagesProvider {

    /**
     * The scanner used to find words in the Eiffel code.
     *
     * @return The scanner.
     */
    @Override
    public WordsScanner getWordsScanner() {
        return new DefaultWordsScanner(new EiffelLexerAdapter(),
                EiffelTokenSets.IDENTIFIER,
                EiffelTokenSets.LINE_COMMENT,
                EiffelTokenSets.STRING);
    }

    /**
     * True if the element can manage usage listing. False if not
     * @param aPsiElement the element for which usages are searched.
     * @return True if the element can manage usage listing. False if not
     */
    @Override
    public boolean canFindUsagesFor(@NotNull PsiElement aPsiElement) {
        return aPsiElement instanceof EiffelClassNameRenamable;
    }

    /**
     * The help topic to show for this usage.
     * @param aPsiElement the element for which the help topic is requested.
     * @return The help topic (none for now)
     */
    @Nullable
    @Override
    public String getHelpId(@NotNull PsiElement aPsiElement) {
        return null;
    }

    /**
     * The type of the usage element
     *
     * @param aElement the element for which the type is requested.
     * @return The type name if the element has a type. Empty string if not.
     */
    @NotNull
    @Override
    public String getType(@NotNull PsiElement aElement) {
        String lResult = "";
        if (aElement instanceof EiffelClassNameRenamable) {
            lResult = "Eiffel class";
        }
        return lResult;
    }

    /**
     * A description of an element.
     * @param aElement the element for which the name is requested.
     * @return The name of the element.
     */
    @NotNull
    @Override
    public String getDescriptiveName(@NotNull PsiElement aElement) {
        String lResult = "";
        if (aElement instanceof EiffelClassNameRenamable lFile && lFile.getName() != null) {
            lResult = lFile.getName();
        }
        return lResult;
    }

    /**
     * The full text of the node of the element.
     * @param aElement     the element for which the node text is requested.
     * @param aUseFullName if true, the returned text should use fully qualified names
     * @return the text of the node.
     */
    @NotNull
    @Override
    public String getNodeText(@NotNull PsiElement aElement, boolean aUseFullName) {
        String lResult = "";
        if (aElement instanceof EiffelClassNameRenamable lFile && lFile.getName() != null) {
            lResult = lFile.getName();
        }
        return lResult;
    }

}
