package net.libreti.intelleiffel;

import com.intellij.openapi.util.IconLoader;

import javax.swing.Icon;

/**
 * The icons of every Eiffel file type.
 *
 * @author Louis M
 */
public class EiffelIcons {

        /**
         * Icon of an Eiffel source code file.
         */
        public static final Icon E_FILE_ICON =
                IconLoader.getIcon("/images/e-file-icon.png", EiffelIcons.class);

        /**
         * Icon of an Eiffel project file.
         */
        public static final Icon ECF_FILE_ICON =
                IconLoader.getIcon("/images/ecf-file-icon.png", EiffelIcons.class);

        /**
         * Icon of an Eiffel class
         */
        public static final Icon CLASS_ICON =
                IconLoader.getIcon("/images/class-icon.png", EiffelIcons.class);

        /**
         * Icon of an Eiffel deferred class.
         */
        public static final Icon DEFERRED_CLASS_ICON =
                IconLoader.getIcon("/images/deferred-class-icon.png", EiffelIcons.class);

        /**
         * Icon of a public feature clause.
         */
        public static final Icon FEATURE_PUBLIC_ICON =
                IconLoader.getIcon("/images/feature-public-icon.png", EiffelIcons.class);

        /**
         * Icon of a private feature clause.
         */
        public static final Icon FEATURE_PRIVATE_ICON =
                IconLoader.getIcon("/images/feature-private-icon.png", EiffelIcons.class);

        /**
         * Icon of a friend feature clause.
         */
        public static final Icon FEATURE_FRIENDS_ICON =
                IconLoader.getIcon("/images/feature-friends-icon.png", EiffelIcons.class);

        /**
         * Icon of an attribute feature.
         */
        public static final Icon FEATURE_ATTRIBUTE_ICON =
                IconLoader.getIcon("/images/feature-attribute-icon.png", EiffelIcons.class);

        /**
         * Icon of a method feature.
         */
        public static final Icon FEATURE_METHOD_ICON =
                IconLoader.getIcon("/images/feature-method-icon.png", EiffelIcons.class);

        /**
         * Icon of a deferred feature.
         */
        public static final Icon FEATURE_DEFERRED_ICON =
                IconLoader.getIcon("/images/feature-method-deferred-icon.png", EiffelIcons.class);

        /**
         * Icon of a frozen feature.
         */
        public static final Icon FEATURE_FROZEN_ICON =
                IconLoader.getIcon("/images/feature-method-frozen-icon.png", EiffelIcons.class);

        /**
         * Icon of a once feature.
         */
        public static final Icon FEATURE_ONCE_ICON =
                IconLoader.getIcon("/images/feature-methode-once-icon.png", EiffelIcons.class);

        /**
         * Icon of an instance free feature.
         */
        public static final Icon FEATURE_INSTANCE_FREE_ICON =
                IconLoader.getIcon("/images/feature-instance-free-icon.png", EiffelIcons.class);

        /**
         * Icon of a constant feature.
         */
        public static final Icon FEATURE_CONSTANT_ICON =
                IconLoader.getIcon("/images/feature-constant-icon.png", EiffelIcons.class);

        /**
         * Icon of a frozen feature that is also a once feature.
         */
        public static final Icon FEATURE_FROZEN_ONCE_ICON =
                IconLoader.getIcon("/images/feature-frozen-once-icon.png", EiffelIcons.class);

        /**
         * Icon of a deferred feature that is also instance free.
         */
        public static final Icon FEATURE_DEFERRED_INSTANCE_FREE_ICON =
                IconLoader.getIcon("/images/feature-deferred-instance-free-icon.png", EiffelIcons.class);

        /**
         * Icon of an external feature.
         */
        public static final Icon FEATURE_EXTERNAL_ICON =
                IconLoader.getIcon("/images/feature-external-icon.png", EiffelIcons.class);

        /**
         * Icon of an obsolete feature.
         */
        public static final Icon FEATURE_OBSOLETE_ICON =
                IconLoader.getIcon("/images/feature-obsolete-icon.png", EiffelIcons.class);

        /**
         * Icon of an entity
         */
        public static final Icon LOCAL_ENTITY_ICON =
                IconLoader.getIcon("/images/local-entity-icon.png", EiffelIcons.class);

}
