package net.libreti.intelleiffel.quickFix;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.codeInspection.util.IntentionFamilyName;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.util.IncorrectOperationException;
import net.libreti.intelleiffel.parser.EiffelElementFactory;
import net.libreti.intelleiffel.parser.Mixins.EiffelType;
import net.libreti.intelleiffel.psi.EiffelPsiAttachmentMark;
import net.libreti.intelleiffel.psi.EiffelPsiExclamationMark;
import org.jetbrains.annotations.NotNull;

public class EiffelExclamationMarkQuickFix extends BaseIntentionAction {

    /**
     * The '!' mark to replace
     */
    @NotNull EiffelPsiExclamationMark element;

    /**
     * Constructor of the object
     *
     * @param aElement the exclamation mark to replace
     */
    public EiffelExclamationMarkQuickFix(@NotNull EiffelPsiExclamationMark aElement) {
        element = aElement;
    }

    /**
     * The quick fix can be applied.
     *
     * @param aProject the project in which the availability is checked.
     * @param aEditor  the editor in which the intention will be invoked.
     * @param aFile    the file open in the editor.
     *
     * @return True if the fix can be used; false if not.
     */
    @Override
    public boolean isAvailable(@NotNull Project aProject, Editor aEditor, PsiFile aFile) {
        return aFile.isWritable();
    }

    /**
     * Apply the quick fix
     * @param aProject the project in which the intention is invoked.
     * @param aEditor  the editor in which the intention is invoked.
     * @param aFile    the file open in the editor.
     * @throws IncorrectOperationException When an invalid operation as been used
     */
    @Override
    public void invoke(@NotNull Project aProject, Editor aEditor, PsiFile aFile) throws IncorrectOperationException {
        ApplicationManager.getApplication().invokeLater(() ->
                WriteCommandAction.writeCommandAction(aProject).run(() -> {
                    if (element.getParent() instanceof EiffelPsiAttachmentMark lAttachmentMark &&
                            lAttachmentMark.getParent() instanceof EiffelType lType) {
                        PsiElement lNewAttachment = EiffelElementFactory.getInstance().
                                createAttachedAttachmentKeyword(aProject);
                        if (lNewAttachment != null) {
                            lType.getNode().replaceChild(lAttachmentMark.getNode(), lNewAttachment.getNode());
                        }
                    }
                })
        );
    }

    /**
     * The text to show in the quick fix window.
     *
     * @return the text to show
     */
    @NotNull
    @Override
    public String getText() {
        return "Replace '!' by 'attached'";
    }
    /**
     * The name if the fix.
     * @return The name if the fix.
     */
    @Override
    public @NotNull @IntentionFamilyName String getFamilyName() {
        return "Replace '!' by 'attached'";
    }
}