package net.libreti.intelleiffel.structure;

import com.intellij.ide.structureView.StructureViewModel;
import com.intellij.ide.structureView.StructureViewModelBase;
import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.Sorter;
import com.intellij.openapi.editor.Editor;
import net.libreti.intelleiffel.parser.Mixins.EiffelClass;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureName;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureClause;
import net.libreti.intelleiffel.psi.EiffelFile;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureDeclaration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A View model for the Eiffel structure
 *
 * @author Louis M
 */
public class EiffelStructureViewModel  extends StructureViewModelBase implements
        StructureViewModel.ElementInfoProvider {

    /**
     * Creator of the structure view model.
     *
     * @param aEditor The editor that will contain the structure view
     * @param aPsiFile The Eiffel file that will be structured
     * @param aClass The first node of the Eiffel file
     */
    public EiffelStructureViewModel(@Nullable Editor aEditor, EiffelFile aPsiFile, EiffelClass aClass) {
        super(aPsiFile, aEditor, new EiffelFileStructureView(aClass));
    }

    /**
     * How the sorting will be done.
     *
     * @return a sorter
     */
    @NotNull
    public Sorter @NotNull [] getSorters() {
        return new Sorter[]{Sorter.ALPHA_SORTER};
    }

    /**
     * ??? I don't know what that do... Sorry.
     *
     * @param element The Structure tree
     * @return True if ... I don't know if what.
     */
    @Override
    public boolean isAlwaysShowsPlus(StructureViewTreeElement element) {
        return false;
    }

    /**
     * The element that is always a leaf in the structure tree.
     *
     * @param element The structure tree
     * @return True if the element must be a leaf in the tree.
     */
    @Override
    public boolean isAlwaysLeaf(StructureViewTreeElement element) {
        return element.getValue() instanceof EiffelFeatureName;
    }

    /**
     * The types that can be used in the Structure aware Navigation bar.
     *
     * @return The array of types
     * @see EiffelStructureAwareNavbar
     */
    @Override
    protected Class<?> @NotNull [] getSuitableClasses() {
        return new Class[]{
                EiffelClass.class,
                EiffelFeatureClause.class,
                EiffelPsiFeatureDeclaration.class
        };
    }

}
