package net.libreti.intelleiffel.structure;

import com.intellij.ide.navigationToolbar.StructureAwareNavBarModelExtension;
import com.intellij.lang.Language;
import net.libreti.intelleiffel.EiffelLanguage;
import net.libreti.intelleiffel.parser.Mixins.EiffelClass;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureName;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureClause;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureDeclaration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;

/**
 * Used to manage navigation bar.
 */
public class EiffelStructureAwareNavbar extends StructureAwareNavBarModelExtension {

    /**
     * The language tha the current object manage.
     *
     * @return The language
     */
    @NotNull
    @Override
    protected Language getLanguage() {
        return EiffelLanguage.getInstance();
    }

    /**
     * The text to show for a specific object.
     *
     * @param aObject The object to get the text for
     * @return the text.
     */
    @Override
    public @Nullable String getPresentableText(Object aObject) {
        String lResult = null;
        if (aObject instanceof EiffelClass lClass) {
            lResult = lClass.getName();
        }
        if (aObject instanceof EiffelFeatureDeclaration lDeclaration) {
            lResult = lDeclaration.getPresentableText();
        }
        if (aObject instanceof EiffelFeatureClause lFeatureClause) {
            lResult = lFeatureClause.getPresentableText();
        }
        if (aObject instanceof EiffelFeatureName lFeature) {
            if (lFeature.getPresentation() != null)  {
                lResult = lFeature.getPresentation().getPresentableText();
            }
        }

        return lResult;
    }

    /**
     * The icon to show for a specific object.
     *
     * @param aObject The object to get the icon for
     * @return the icon.
     */
    @Override
    @Nullable
    public Icon getIcon(Object aObject) {
        Icon lResult = null;
        if (aObject instanceof EiffelClass lClass) {
            lClass.getIcon(false);
        }
        if (aObject instanceof EiffelFeatureClause lFeatureClause) {
            lResult = lFeatureClause.getIcon(false);
        }
        if (aObject instanceof EiffelFeatureDeclaration lDeclaration) {
            if (lDeclaration.getPresentation() != null)  {
                lResult = lDeclaration.getPresentation().getIcon(false);
            }
        }
        if (aObject instanceof EiffelFeatureName lFeature) {
            if (lFeature.getPresentation() != null)  {
                lResult = lFeature.getPresentation().getIcon(false);
            }

        }

        return lResult;
    }

}
