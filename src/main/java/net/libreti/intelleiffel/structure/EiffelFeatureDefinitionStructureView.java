package net.libreti.intelleiffel.structure;

import com.intellij.ide.util.treeView.smartTree.TreeElement;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureDeclaration;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureName;
import net.libreti.intelleiffel.psi.EiffelPsiNewFeature;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

/**
 * Used to generate Structure View
 *
 * @author Louis M
 */
public class EiffelFeatureDefinitionStructureView extends EiffelStructureViewElement  {

    /**
     * The feature to show in the structure view.
     */
    private EiffelFeatureDeclaration eiffelFeatureDeclaration;

    /**
     * Constructor of the Structure view
     *
     * @param aFeature The feature declaration to show in the structure view
     */
    public EiffelFeatureDefinitionStructureView(EiffelFeatureDeclaration aFeature) {
        super(aFeature);
        eiffelFeatureDeclaration = aFeature;
    }

    /**
     * Every child of the eiffel feature in the structure view.
     *
     * @return the children of the eiffel feature in the structure view.
     */
    @Override
    public TreeElement @NotNull [] getChildren() {
        List<TreeElement> treeElements = new LinkedList<>();
        if (eiffelFeatureDeclaration.getNewFeatureList().getNewFeatureList().size() > 1) {
            for (EiffelPsiNewFeature lFeatureName : eiffelFeatureDeclaration.getNewFeatureList().getNewFeatureList()) {
                if (lFeatureName instanceof EiffelFeatureName lName) {
                    treeElements.add(new EiffelFeatureNameStructureView(lName));
                }
            }
        }
        return treeElements.toArray(new TreeElement[0]);
    }
}
