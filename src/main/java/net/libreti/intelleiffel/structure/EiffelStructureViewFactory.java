package net.libreti.intelleiffel.structure;

import com.intellij.ide.structureView.StructureViewBuilder;
import com.intellij.ide.structureView.StructureViewModel;
import com.intellij.ide.structureView.TreeBasedStructureViewBuilder;
import com.intellij.lang.PsiStructureViewFactory;
import com.intellij.openapi.editor.Editor;
import com.intellij.psi.PsiFile;
import net.libreti.intelleiffel.parser.Mixins.EiffelClass;
import net.libreti.intelleiffel.psi.EiffelFile;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Factory used to generate Eiffel structure
 */
final public class EiffelStructureViewFactory implements PsiStructureViewFactory {

    /**
     * Retreive a structure builder for a file
     *
     * @param aPsiFile The file to generate the structure builder
     * @return The builder to generate
     */
    @Override
    public @Nullable StructureViewBuilder getStructureViewBuilder(@NotNull final PsiFile aPsiFile) {
        StructureViewBuilder lResult = null;
        if (aPsiFile instanceof EiffelFile lFile) {
            EiffelClass lClass = EiffelUtil.getInstance().findClassInFile(lFile);
            if (lClass != null) {
                lResult =  new TreeBasedStructureViewBuilder() {
                    /**
                     * Create a view model for the structure builder.
                     *
                     * @param aEditor The editor that the structure will be shown into
                     * @return the structure view model
                     */
                    @NotNull
                    @Override
                    public StructureViewModel createStructureViewModel(@Nullable Editor aEditor) {
                        return new EiffelStructureViewModel(aEditor, lFile, lClass);
                    }
                };

            }
        }
        return lResult;
    }

}
