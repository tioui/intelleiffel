package net.libreti.intelleiffel.structure;

import com.intellij.ide.projectView.PresentationData;
import com.intellij.ide.util.treeView.smartTree.TreeElement;
import com.intellij.navigation.ItemPresentation;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureName;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

/**
 * Used to show in an Eiffel feature in a structure view
 *
 * @author Louis M
 */
public class EiffelFeatureNameStructureView extends EiffelStructureViewElement {

    /**
     * The feature to show in the structure view.
     */
    private EiffelFeatureName eiffelFeatureName;

    /**
     * Constructor of the Structure view
     *
     * @param aFeature The feature name to show in the structure view
     */
    public EiffelFeatureNameStructureView(EiffelFeatureName aFeature) {
        super(aFeature);
        eiffelFeatureName = aFeature;
    }


    /**
     * The Presentation of the Eiffel feature name in the structure view.
     *
     * @return the presentation of the Eiffel feature name
     */
    @NotNull
    @Override
    public ItemPresentation getPresentation() {
        ItemPresentation presentation = eiffelFeatureName.getPresentation();
        return presentation != null ? presentation : new PresentationData();
    }

    /**
     * Every child of the eiffel feature in the structure view.
     *
     * @return the children of the eiffel feature in the structure view.
     */
    @Override
    public TreeElement @NotNull [] getChildren() {
        List<TreeElement> treeElements = new LinkedList<>();
        return treeElements.toArray(new TreeElement[0]);
    }
}
