package net.libreti.intelleiffel.structure;

import com.intellij.ide.projectView.PresentationData;
import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.SortableTreeElement;
import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.NavigatablePsiElement;
import org.jetbrains.annotations.NotNull;

/**
 * Used to show in an Eiffel element in a structure view
 *
 * @author Louis M
 */
public abstract class EiffelStructureViewElement implements StructureViewTreeElement, SortableTreeElement {

    /**
     * The eiffel element to show in the structure view.
     */
    private final NavigatablePsiElement eiffelElement;

    /**
     * Constructor of the Structure view
     *
     * @param aElement The Eiffel element to show in the structure view
     */
    public EiffelStructureViewElement(NavigatablePsiElement aElement) {
        this.eiffelElement = aElement;
    }

    /**
     * The element value.
     *
     * @return the element value
     */
    @Override
    public Object getValue() {
        return eiffelElement;
    }

    /**
     * Used to navigate to the Eiffel element.
     *
     * @param requestFocus {@code true} if focus requesting is necessary
     */
    @Override
    public void navigate(boolean requestFocus) {
        eiffelElement.navigate(requestFocus);
    }

    /**
     * Indicate if the element can be navigated to.
     *
     * @return True if the element can be navigated to. False if not.
     */
    @Override
    public boolean canNavigate() {
        return eiffelElement.canNavigate();
    }

    /**
     * Indicate if there is a source that the element can be navigated to.
     *
     * @return True if the element can be navigated to a source code. False if not.
     */
    @Override
    public boolean canNavigateToSource() {
        return eiffelElement.canNavigateToSource();
    }

    /**
     * Used to sort the elements in the view.
     *
     * @return The mane of the element since we want to sort alphabetically
     */
    @NotNull
    @Override
    public String getAlphaSortKey() {
        String name = eiffelElement.getName();
        return name != null ? name : "";
    }

    /**
     * The Presentation of the Eiffel element in the structure view.
     *
     * @return the presentation of the Eiffel element
     */
    @NotNull
    @Override
    public ItemPresentation getPresentation() {
        ItemPresentation presentation = null;
        if (eiffelElement instanceof ItemPresentation lPresentation) {
            presentation = lPresentation;
        } else {
            presentation = eiffelElement.getPresentation();
        }
        return presentation != null ? presentation : new PresentationData();
    }

}
