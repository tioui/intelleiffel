package net.libreti.intelleiffel.structure;

import com.intellij.ide.util.treeView.smartTree.TreeElement;
import net.libreti.intelleiffel.parser.Mixins.EiffelClass;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureClause;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureClause;
import net.libreti.intelleiffel.psi.EiffelPsiFeatures;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

/**
 * Used to show in an Eiffel File in a structure view
 *
 * @author Louis M
 */
public class EiffelFileStructureView extends EiffelStructureViewElement{

    /**
     * The EiffelFile to show in the structure view.
     */
    private EiffelClass eiffelClass;

    /**
     * Constructor of the Structure view
     *
     * @param aEiffelClass The file to show in the structure view
     */
    public EiffelFileStructureView(EiffelClass aEiffelClass) {
        super(aEiffelClass);
        eiffelClass = aEiffelClass;
    }

    /**
     * Every child of the EiffelFile in the structure view.
     *
     * @return the children of the EiffelFile in the structure view.
     */
    @Override
    public TreeElement @NotNull [] getChildren() {
        List<TreeElement> treeElements = new LinkedList<>();
        EiffelPsiFeatures lFeatures = eiffelClass.getFeatures();
        if (lFeatures != null) {
            for (EiffelPsiFeatureClause lPsiClause : lFeatures.getFeatureClauseList()) {
                if (lPsiClause instanceof EiffelFeatureClause lFeatureClause) {
                    treeElements.add(new EiffelFeatureClauseStructureView(lFeatureClause));
                }
            }
        }
        return treeElements.toArray(new TreeElement[0]);
    }

}
