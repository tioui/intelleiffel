package net.libreti.intelleiffel.structure;

import com.intellij.ide.util.treeView.smartTree.TreeElement;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureDeclaration;
import net.libreti.intelleiffel.parser.Mixins.EiffelFeatureClause;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureDeclaration;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureDeclarationList;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

/**
 * Used to show in an Eiffel feature clause in a structure view
 *
 * @author Louis M
 */
public class EiffelFeatureClauseStructureView extends EiffelStructureViewElement {

    /**
     * The feature clause to show in the structure view.
     */
    private EiffelFeatureClause eiffelFeature;

    /**
     * Constructor of the Structure view
     *
     * @param aFeature The feature clause to show in the structure view
     */
    public EiffelFeatureClauseStructureView(EiffelFeatureClause aFeature) {
        super(aFeature);
        eiffelFeature = aFeature;
    }

    /**
     * Every child of the eiffel feature in the structure view.
     *
     * @return the children of the eiffel feature in the structure view.
     */
    @Override
    public TreeElement @NotNull [] getChildren() {
        List<TreeElement> treeElements = new LinkedList<>();
        EiffelPsiFeatureDeclarationList lFeatureList = eiffelFeature.getFeatureDeclarationList();
        if (lFeatureList != null) {
            for (EiffelPsiFeatureDeclaration lDeclaration : lFeatureList.getFeatureDeclarationList()) {
                treeElements.add(new EiffelFeatureDefinitionStructureView((EiffelFeatureDeclaration) lDeclaration));
            }
        }
        return treeElements.toArray(new TreeElement[0]);
    }

}
