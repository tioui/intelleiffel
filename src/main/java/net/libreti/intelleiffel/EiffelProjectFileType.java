package net.libreti.intelleiffel;

import com.intellij.lang.xml.XMLLanguage;
import com.intellij.openapi.fileTypes.LanguageFileType;

import javax.swing.Icon;

/**
 * Define an Eiffel configuration file
 *
 * @author Louis M
 */
public class EiffelProjectFileType extends LanguageFileType {

    /**
     * A singleton instance of EiffelProjectFileType.
     */
    private static final EiffelProjectFileType INSTANCE = new EiffelProjectFileType();

    /**
     * Retreive the singleton instance of EiffelProjectFileType.
     *
     * @return The EiffelProjectFileType instance
     */
    public static EiffelProjectFileType getInstance() {
        return INSTANCE;
    }

    /**
     * Creator of the class.
     */
    private EiffelProjectFileType() {
        super(XMLLanguage.INSTANCE);
    }

    /**
     * The name of the Eiffel configuration file type.
     *
     * @return the name of the Eiffel configuration file type.
     */
    @Override
    public String getName() {
        return "Eiffel project file";
    }

    /**
     * The description of the Eiffel configuration file type.
     *
     * @return the description of the Eiffel configuration file type.
     */
    @Override
    public String getDescription() {
        return "Eiffel project file";
    }

    /**
     * The extension (without the dot ".") of an Eiffel configuration file.
     *
     * @return the extension (always ecf).
     */
    @Override
    public String getDefaultExtension() {
        return "ecf";
    }

    /**
     * The icon to show in the project browser for an Eiffel configuration file
     *
     * @return The icon of an Eiffel configuration file.
     */
    @Override
    public Icon getIcon() {
        return EiffelIcons.ECF_FILE_ICON;
    }


}
