package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.models.EiffelAddressable;
import net.libreti.intelleiffel.psi.EiffelFile;
import net.libreti.intelleiffel.psi.EiffelPsiClassType;
import net.libreti.intelleiffel.psi.EiffelUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * A class Eiffel type
 *
 * @author Louis M
 */
public abstract class EiffelClassType extends ASTWrapperPsiElement
        implements EiffelPsiClassType {

    /**
     * Constructor of the object
     * @param aNode The element Psi node.
     */
    public EiffelClassType(ASTNode aNode) {
        super(aNode);
    }

    /**
     * Get every feature accessible via this type (not just the exported one)
     *
     * @return every feature in the class of this type
     */
    public List<EiffelAddressable> getFeatures() {
        List<EiffelAddressable> lResult;
        EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(
                getClassTypeName().getClassNameIdentifier().getIdentifier().getText(), getProject());
        if (lClass != null) {
            lResult = new ArrayList<>(lClass.getAllFeatures());
        } else {
            lResult = new ArrayList<>();
        }
        return lResult;
    }

    /**
     * Retrieve every accessible feature in the type class from the current class
     *
     * @return every accessibles features
     */
    public List<EiffelAddressable> getAccessibleFeatures() {
        List<EiffelAddressable> lResult;
        EiffelClass lTargetClass = EiffelUtil.getInstance().findEiffelClass(
                getClassTypeName().getClassNameIdentifier().getIdentifier().getText(), getProject());
        if (lTargetClass != null) {
            EiffelClass lCurrentClass = EiffelUtil.getInstance().findClassInFile((EiffelFile) getContainingFile());
            if (lCurrentClass != null) {
                lResult = new ArrayList<>(lTargetClass.getExportedFeatures(lCurrentClass.getName()));
            } else {
                lResult = new ArrayList<>(lTargetClass.getExportedFeatures());
            }
        } else {
            lResult = new ArrayList<>();
        }
        return lResult;
    }
}
