package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.psi.EiffelPsiInheritClause;
import net.libreti.intelleiffel.psi.EiffelPsiInheritance;
import net.libreti.intelleiffel.psi.EiffelPsiParent;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Contain every inheritance clause in the class file.
 *
 * @author Louis M
 */
public abstract class EiffelInheritance extends EiffelFeaturable implements EiffelPsiInheritance {


    /**
     * The constructor of the object
     *
     * @param aNode The node of the element
     */
    public EiffelInheritance(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * Return every parent that have conformance
     *
     * @return the parents that have conformance
     */
    public List<EiffelParent> getConformanceParents() {
        List<EiffelParent> lResult = new LinkedList<>();
        for (EiffelPsiInheritClause lClause : getInheritClauseList()) {
            if (((EiffelInheritClause)lClause).has_conformance()) {
                for (EiffelPsiParent lParent : lClause.getParentList().getParentList()) {
                    lResult.add((EiffelParent)lParent);
                }
            }
        }
        return lResult;
    }

    /**
     * Return every parent
     *
     * @return the parents
     */
    public List<EiffelParent> getParents() {
        List<EiffelParent> lResult = new LinkedList<>();
        for (EiffelPsiInheritClause lClause : getInheritClauseList()) {
            for (EiffelPsiParent lParent : lClause.getParentList().getParentList()) {
                lResult.add((EiffelParent)lParent);
            }
        }
        return lResult;
    }

    /**
     * The list of featurable to used in this object
     *
     * @return the list of featurables
     */
    @Override
    protected List<EiffelFeaturable> getFeaturableList() {
        List<EiffelFeaturable> lResult = new ArrayList<>(getInheritClauseList().size());
        for (EiffelPsiInheritClause lClause: getInheritClauseList()) {
            lResult.add((EiffelFeaturable) lClause);
        }
        return lResult;
    }

}
