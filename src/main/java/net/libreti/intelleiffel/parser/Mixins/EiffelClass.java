package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.VfsUtilCore;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import net.libreti.intelleiffel.models.EiffelFeature;
import net.libreti.intelleiffel.models.EiffelNoteDocumentation;
import net.libreti.intelleiffel.psi.EiffelPsiEiffelFile;
import net.libreti.intelleiffel.psi.EiffelPsiNotes;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Mixin for the Grammar rules Eiffel_File
 *
 * @author Louis M
 */
public abstract class EiffelClass extends ASTWrapperPsiElement implements EiffelPsiEiffelFile, ItemPresentation {

    /**
     * Initialize the mixin
     * @param aNode The syntaxic node of the Eiffel File
     */
    public EiffelClass(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * Return the class name of the current Eiffel file
     * @return The class name
     */
    @Override
    public @Nullable String getName() {
        return ((EiffelClassHeader)getClassHeader()).getClassName();
    }

    /**
     * Return the documentation of the current Eiffel File
     * @return The documentation
     */
    public @NotNull List<EiffelNoteDocumentation> getDocumentation() {
        List<EiffelNoteDocumentation> lResult = new LinkedList<>();
        for (EiffelPsiNotes lNotes : getNotesList()) {
            lResult.addAll(((EiffelNotes)lNotes).getDocumentation());
        }
        return lResult;
    }

    /**
     * Check if the class is deferred.
     *
     * @return True if the class is deferred. False if not.
     */
    public boolean is_deferred() {
        EiffelClassHeader lHeader = (EiffelClassHeader)getClassHeader();
        return lHeader.isDeferred();
    }

    /**
     * Check if the class is frozen.
     *
     * @return True if the class is frozen. False if not.
     */
    public boolean is_frozen() {
        EiffelClassHeader lHeader = (EiffelClassHeader)getClassHeader();
        return lHeader.isFrozen();
    }

    /**
     * Check if the class is expanded.
     *
     * @return True if the class is expanded. False if not.
     */
    public boolean is_expanded() {
        EiffelClassHeader lHeader = (EiffelClassHeader)getClassHeader();
        return lHeader.isExpanded();
    }

    /**
     * Is the current file obsolete and should not be used
     * @return True if the file is obsolete. False if not.
     */
    public boolean isObsolete() {
        return getObsolete() != null;
    }

    /**
     * The obsolete message if `isObsolete` is true.
     * @return The message or empty string if `isObsolete` is false
     */
    public @NotNull String getObsoleteMessage() {
        String lMessage = "";
        if (getObsolete() != null) {
            lMessage = EiffelUtil.getInstance().getStringValue(
                    getObsolete().getMessage().getString().getText().replaceAll("\\\\ ", " ")); ;
        }
        return lMessage;
    }

    /**
     * Add the class name `aName` in a list (if it is not in it already) and add every parent name of the class with
     * that name to the list also
     * @param aList The list of ancestors
     * @param aName The class name to manage
     */
    private void getAncestorsOf(List<String> aList, String aName) {
        boolean lFound = false;
        for (Iterator<String> lIterator = aList.iterator(); lIterator.hasNext() && !lFound; lIterator.next() ) {
            if (aName.equals(lIterator.next())) {
                lFound = true;
            }
        }
        if (lFound) {
            aList.add(aName);
            EiffelClass lEiffelClass = EiffelUtil.getInstance().findEiffelClass(aName, getProject());
            if (lEiffelClass != null) {
                for (EiffelParent lParent : lEiffelClass.getConformanceParents()) {
                    getAncestorsOf(aList, lParent.getName());
                }
            }
        }
    }

    /**
     * Retreive all conform ancestors of this class.
     *
     * @return the ancestors
     */
    public List<String> getAncestorNames() {
        List<String> lResult = new LinkedList<>();
        for (EiffelParent lParent : getConformanceParents()) {
            getAncestorsOf(lResult, lParent.getName());
        }
        return lResult;
    }

    /**
     * A text the represent the current object.
     *
     * @return A text the represent the current object.
     */
    @Override
    public @Nullable String getPresentableText() {
        String lResult = null;
        if (getClassHeader().getTupleDefinition() != null) {
            lResult = ((EiffelTupleDefinition)getClassHeader().getTupleDefinition()).getPresentableText();
        } else if (getClassHeader().getClassNameDefinition() != null) {
            lResult = ((EiffelClassNameDefinition)getClassHeader().getClassNameDefinition()).getPresentableText();
        }
        return lResult;
    }

    /**
     * A text the represent the current object's location.
     *
     * @return the location.
     */
    @Nullable
    @Override
    public String getLocationString() {
        String lResult = null;
        PsiFile lContainingFile = getContainingFile();
        if (lContainingFile != null) {
            VirtualFile lpath = ProjectRootManager.getInstance(getProject()).getFileIndex().getContentRootForFile(lContainingFile.getVirtualFile());
            if (lpath != null) {
                lResult = VfsUtilCore.getRelativeLocation(lContainingFile.getVirtualFile(), lpath);
            }
            if (lResult == null) {
                lResult = lContainingFile.getName();
            }
        }
        return lResult;
    }

    /**
     * Every feature in the feature list
     *
     * @return every feature in this.
     */
    public @NotNull List<EiffelFeature> getAllFeatures() {
        List<EiffelFeature> lResult = new LinkedList<>();
        if (getFeatures() != null) {
            lResult.addAll(((EiffelFeatureList)getFeatures()).getFeatures());
        }
        if (getInheritance() != null) {
            lResult.addAll(((EiffelInheritance)getInheritance()).getFeatures());
        }
        EiffelUtil.getInstance().removeDuplicate(lResult);
        return lResult;
    }

    /**
     * The features in the feature list that are public
     *
     * @return the public features.
     */
    public @NotNull List<EiffelFeature> getExportedFeatures() {
        List<EiffelFeature> lResult = new LinkedList<>();
        if (getFeatures() != null) {
            lResult.addAll(((EiffelFeatureList)getFeatures()).getExportedFeatures());
        }
        if (getInheritance() != null) {
            lResult.addAll(((EiffelInheritance)getInheritance()).getExportedFeatures());
        }
        EiffelUtil.getInstance().removeDuplicate(lResult);
        return lResult;
    }

    /**
     * The features in the feature list that are exported to a type
     *
     * @param aTypeName The type to check if the feature clause are exported to
     * @return the exported features.
     */
    public @NotNull List<EiffelFeature> getExportedFeatures(String aTypeName) {
        List<EiffelFeature> lResult = new LinkedList<>();
        if (getFeatures() != null) {
            lResult.addAll(((EiffelFeatureList)getFeatures()).getExportedFeatures(aTypeName));
        }
        if (getInheritance() != null) {
            lResult.addAll(((EiffelInheritance)getInheritance()).getExportedFeatures(aTypeName));
        }
        EiffelUtil.getInstance().removeDuplicate(lResult);
        return lResult;
    }

    /**
     * Retreive all attributes from a list of features.
     *
     * @param aFeatures the list of feature
     * @return the attributes
     */
    private List<EiffelFeature> getAttributesFrom(List<EiffelFeature> aFeatures) {
        List<EiffelFeature> lResult = new LinkedList<>();
        for (EiffelFeature lFeature : aFeatures) {
            if (lFeature.isAttribute()) {
                lResult.add(lFeature);
            }
        }
        return lResult;
    }

    /**
     * Retrieve all attributes of the class
     *
     * @return every attributes
     */
    public List<EiffelFeature> getAllAttributes() {
        return getAttributesFrom(getAllFeatures());
    }

    /**
     * Retrieve exported attributes of the class
     *
     * @return exported attributes
     */
    public List<EiffelFeature> getExportedAttributes() {
        return getAttributesFrom(getExportedFeatures());
    }

    /**
     * Retrieve attributes of the class that are exported to a type
     *
     * @param aTypeName The type to check if the feature clause are exported to
     * @return exported attributes
     */
    public List<EiffelFeature> getExportedAttributes(String aTypeName) {
        return getAttributesFrom(getExportedFeatures(aTypeName));
    }

    /**
     * Retreive all functions from a list of features.
     *
     * @param aFeatures the list of feature
     * @return the functions
     */
    private List<EiffelFeature> getFunctionsFrom(List<EiffelFeature> aFeatures) {
        List<EiffelFeature> lResult = new LinkedList<>();
        for (EiffelFeature lFeature : aFeatures) {
            if (lFeature.isFunction()) {
                lResult.add(lFeature);
            }
        }
        return lResult;
    }

    /**
     * Retrieve all function of the class
     *
     * @return every function
     */
    public List<EiffelFeature> getAllFunctions() {
        return getFunctionsFrom(getAllFeatures());
    }

    /**
     * Retrieve exported function of the class
     *
     * @return exported function
     */
    public List<EiffelFeature> getExportedFunctions() {
        return getFunctionsFrom(getExportedFeatures());
    }

    /**
     * Retrieve functions of the class that are exported to a type
     *
     * @param aTypeName The type to check if the feature clause are exported to
     * @return exported function
     */
    public List<EiffelFeature> getExportedFunctions(String aTypeName) {
        return getFunctionsFrom(getExportedFeatures(aTypeName));
    }

    /**
     * Retreive all procedures from a list of features.
     *
     * @param aFeatures the list of feature
     * @return the procedures
     */
    private List<EiffelFeature> getProceduresFrom(List<EiffelFeature> aFeatures) {
        List<EiffelFeature> lResult = new LinkedList<>();
        for (EiffelFeature lFeature : aFeatures) {
            if (lFeature.isProcedure()) {
                lResult.add(lFeature);
            }
        }
        return lResult;
    }

    /**
     * Retrieve all procedures of the class
     *
     * @return every procedure
     */
    public List<EiffelFeature> getAllProcedures() {
        return getProceduresFrom(getAllFeatures());
    }

    /**
     * Retrieve exported procedures of the class
     *
     * @return exported procedures
     */
    public List<EiffelFeature> getExportedProcedures() {
        return getProceduresFrom(getExportedFeatures());
    }

    /**
     * Retrieve procedures of the class that are exported to a type
     *
     * @param aTypeName The type to check if the feature clause are exported to
     * @return exported procedures
     */
    public List<EiffelFeature> getExportedProcedures(String aTypeName) {
        return getProceduresFrom(getExportedFeatures(aTypeName));
    }

    /**
     * Retrieve all instance free routines from a list of features.
     *
     * @param aFeatures the list of feature
     * @return the instance free routines
     */
    public List<EiffelFeature> getInstanceFreeRoutinesFrom(List<EiffelFeature> aFeatures) {
        List<EiffelFeature> lResult = new LinkedList<>();
        for (EiffelFeature lFeature : aFeatures) {
            if (lFeature.isInstanceFreeRoutine()) {
                lResult.add(lFeature);
            }
        }
        return lResult;
    }

    /**
     * Retrieve all instance free routines of the class
     *
     * @return every instance free routines
     */
    public List<EiffelFeature> getAllInstanceFreeRoutines() {
        return getInstanceFreeRoutinesFrom(getAllFeatures());
    }

    /**
     * Retrieve exported instance free routines of the class
     *
     * @return exported instance free routines
     */
    public List<EiffelFeature> getExportedInstanceFreeRoutines() {
        return getInstanceFreeRoutinesFrom(getExportedFeatures());
    }

    /**
     * Retrieve instance free routines of the class that are exported to a type
     *
     * @param aTypeName The type to check if the feature clause are exported to
     * @return exported instance free routines
     */
    public List<EiffelFeature> getExportedInstanceFreeRoutines(String aTypeName) {
        return getInstanceFreeRoutinesFrom(getExportedFeatures(aTypeName));
    }

    /**
     * Retrieve all instance free routines from a list of features.
     *
     * @param aFeatures the list of feature
     * @return the instance free routines
     */
    public List<EiffelFeature> getConstantsFrom(List<EiffelFeature> aFeatures) {
        List<EiffelFeature> lResult = new LinkedList<>();
        for (EiffelFeature lFeature : aFeatures) {
            if (lFeature.isConstant()) {
                lResult.add(lFeature);
            }
        }
        return lResult;
    }

    /**
     * Retrieve all instance free routines of the class
     *
     * @return every instance free routines
     */
    public List<EiffelFeature> getAllConstants() {
        return getConstantsFrom(getAllFeatures());
    }

    /**
     * Retrieve exported instance free routines of the class
     *
     * @return exported instance free routines
     */
    public List<EiffelFeature> getExportedConstants() {
        return getConstantsFrom(getExportedFeatures());
    }

    /**
     * Retrieve instance free routines of the class that are exported to a type
     *
     * @param aTypeName The type to check if the feature clause are exported to
     * @return exported instance free routines
     */
    public List<EiffelFeature> getExportedConstants(String aTypeName) {
        return getConstantsFrom(getExportedFeatures(aTypeName));
    }

    /**
     * Return every parent that have conformance
     *
     * @return the parents that have conformance
     */
    public List<EiffelParent> getConformanceParents() {
        List<EiffelParent> lResult;
        if (getInheritance() != null) {
            lResult = ((EiffelInheritance)getInheritance()).getConformanceParents();
        } else {
            lResult = new LinkedList<>();
        }
        EiffelUtil.getInstance().removeDuplicate(lResult);
        return lResult;
    }

    /**
     * Return every parent
     *
     * @return the parents
     */
    public List<EiffelParent> getParents() {
        List<EiffelParent> lResult;
        if (getInheritance() != null) {
            lResult = ((EiffelInheritance)getInheritance()).getParents();
        } else {
            lResult = new LinkedList<>();
        }
        EiffelUtil.getInstance().removeDuplicate(lResult);
        return lResult;
    }


    /**
     * The icon of the current class.
     *
     * @param unused Used to mean if open/close icons for tree renderer. No longer in use.
     * @return The icon of the current object.
     */
    @Override
    public @Nullable Icon getIcon(boolean unused) {
        Icon lResult = null;
        if (getClassHeader().getTupleDefinition() != null) {
            lResult = ((EiffelTupleDefinition)getClassHeader().getTupleDefinition()).getIcon(unused);
        } else if (getClassHeader().getClassNameDefinition() != null) {
            lResult = ((EiffelClassNameDefinition)getClassHeader().getClassNameDefinition()).getIcon(unused);
        }
        return lResult;
    }

    /**
     * Text representation of the current object.
     *
     * @return Text representation of the current object.
     */
    @Override
    public String toString() {
        return "Eiffel File";
    }

}


