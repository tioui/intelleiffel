package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiNameIdentifierOwner;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.IncorrectOperationException;
import net.libreti.intelleiffel.EiffelFileType;
import net.libreti.intelleiffel.parser.EiffelElementFactory;
import net.libreti.intelleiffel.psi.EiffelFile;
import net.libreti.intelleiffel.psi.EiffelPsiClassNameIdentifier;
import net.libreti.intelleiffel.psi.EiffelTypes;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Collection;

/**
 * A renamable class identifier.
 *
 * @author Louis M
 */
public abstract class EiffelClassNameRenamable extends ASTWrapperPsiElement implements PsiNameIdentifierOwner {

    /**
     * Used to show logging message
     */
    private static final Logger LOGGER = Logger.getInstance(EiffelClassNameRenamable.class);

    /**
     * Class creator
     *
     * @param aNode The AST node of the EiffelClassNameDefinition
     */
    public EiffelClassNameRenamable(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * The identifier to get class name from.
     *
     * @return the identifier.
     */
    @NotNull
    public abstract EiffelPsiClassNameIdentifier getClassNameIdentifier();

    /**
     * Get the name of the Class name definition
     *
     * @return The name of the class
     */
    @Nullable
    @NonNls
    @Override
    public String getName() {
        return getClassNameIdentifier().getIdentifier().getText();
    }

    /**
     * Get the name identifier of the Class name definition
     *
     * @return The name identifier
     */
    @Nullable
    @Override
    public PsiElement getNameIdentifier() {
        return getClassNameIdentifier().getIdentifier();
    }


    /**
     * Replace all EiffelClassName with the new name doing a deep search.
     *
     * @param aElement The element to manage (element and children)
     * @param aOldName The old name of the class
     * @param aNewName The new name of the class
     * @throws IncorrectOperationException When the new name is not valid
     */
    private void deepReplaceClassIdentifier(PsiElement aElement, String aOldName, String aNewName)
            throws IncorrectOperationException {
        if (aElement instanceof EiffelPsiClassNameIdentifier lClassName) {
            if (aOldName.equals(lClassName.getIdentifier().getText().toUpperCase())) {
                PsiElement lNewIdentifier = EiffelElementFactory.getInstance().createEiffelElement(getProject(),
                        aNewName.toUpperCase(), EiffelTypes.EIF_IDENTIFIER);
                if (lNewIdentifier != null) {
                    lClassName.getNode().replaceChild(lClassName.getIdentifier().getNode(), lNewIdentifier.getNode());
                } else {
                    throw new IncorrectOperationException(aNewName + " is not a valid class identifier.");
                }
            }
        } else {
            PsiElement[] lChildren = aElement.getChildren();
            for (PsiElement lChild : lChildren) {
                deepReplaceClassIdentifier(lChild, aOldName, aNewName);
            }
        }
    }

    /**
     * Rename (refactor) the name of the class.
     *
     * @param aNewName the new name of the class
     * @return Always return the current object (this)
     * @throws IncorrectOperationException When the class definition cannot be change or the new name is not valid
     */
    @Override
    public PsiElement setName(@NonNls @NotNull String aNewName) throws IncorrectOperationException {
        String lOriginalName = getName().toUpperCase();
        EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(lOriginalName, getProject());
        if (lClass == null || !lClass.isWritable()) {
            throw new IncorrectOperationException("Cannot rename since the class definition cannot be modified.");
        } else {
            Collection<VirtualFile> lVirtualFiles =
                    FileTypeIndex.getFiles(EiffelFileType.getInstance(), GlobalSearchScope.allScope(getProject()));
            for (VirtualFile lVirtualFile : lVirtualFiles) {
                EiffelFile lEiffelFile = (EiffelFile) PsiManager.getInstance(getProject()).findFile(lVirtualFile);

                if (lEiffelFile != null) {
                    PsiFile[] lRoots = lEiffelFile.getPsiRoots();
                    for (PsiFile lRoot : lRoots) {
                        deepReplaceClassIdentifier(lRoot, lOriginalName, aNewName);
                    }
                }
            }
            try {
                lClass.getContainingFile().getVirtualFile().rename(this, lOriginalName.toLowerCase() + ".e");
            } catch (IOException lException) {
                LOGGER.error(lException);
            }

        }
        return this;
    }

    /**
     * Text representation of the current object.
     *
     * @return Text representation of the current object.
     */
    @Override
    public String toString() {
        return getName();
    }

}
