package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import net.libreti.intelleiffel.models.EiffelFeature;
import net.libreti.intelleiffel.psi.EiffelPsiNewFeature;
import net.libreti.intelleiffel.psi.EiffelPsiNewFeatureList;
import org.jetbrains.annotations.Nullable;

/**
 * A feature definition
 *
 * @author Louis M
 */
public abstract class EiffelFeatureName extends ASTWrapperPsiElement
        implements EiffelPsiNewFeature {

    /**
     * Constructor of the object
     * @param aNode The element Psi node.
     */
    public EiffelFeatureName(ASTNode aNode) {
        super(aNode);
    }

    /**
     * The feature is declared as frozen
     *
     * @return True if the feature is frozen. False if not.
     */
    public boolean is_frozen() {
        return getFrozenKeyword() != null;
    }

    /**
     * Return the presentation of the current object
     *
     * @return the presentation
     */
    public @Nullable ItemPresentation getPresentation() {
        ItemPresentation lResult = null;
        if (getParent() instanceof EiffelPsiNewFeatureList lFeatureList) {
            if (lFeatureList.getParent() instanceof EiffelFeatureDeclaration lDeclaration) {
                EiffelFeature lFeature = lDeclaration.getFeatureFromName(this);
                if (lFeature != null)  {
                    lResult = lFeature;
                }
            }
        }
        return lResult;
    }



}
