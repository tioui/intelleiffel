package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import net.libreti.intelleiffel.models.EiffelAddressable;
import net.libreti.intelleiffel.models.EiffelFeature;
import net.libreti.intelleiffel.parser.EiffelElementFactory;
import net.libreti.intelleiffel.psi.EiffelFile;
import net.libreti.intelleiffel.psi.EiffelPsiAnchored;
import net.libreti.intelleiffel.psi.EiffelPsiTupleType;
import net.libreti.intelleiffel.psi.EiffelPsiType;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * An Eiffel type
 *
 * @author Louis M
 */
public abstract class EiffelType extends ASTWrapperPsiElement
        implements EiffelPsiType {

    /**
     * Constructor of the object
     * @param aNode The element Psi node.
     */
    public EiffelType(ASTNode aNode) {
        super(aNode);
    }

    /**
     * Retrieve if the type is attached
     *
     * @return True if the type is attached; False if not.
     */
    public boolean isAttached() {
        return
                (getAttachmentMark() != null && getAttachmentMark().getExclamationMark() != null) ||
                (getAttachmentKeyword() != null && getAttachmentKeyword().getAttachedKeyword() != null);
    }

    /**
     * Retrieve if the type is detachable
     *
     * @return True if the type is detachable; False if not.
     */
    public boolean isDetachable() {
        return
                (getAttachmentMark() != null && getAttachmentMark().getInterrogationMark() != null) ||
                        (getAttachmentKeyword() != null && getAttachmentKeyword().getDetachableKeyword() != null);
    }

    /**
     * Retrieve if the type is separate
     *
     * @return True if the type is separate; False if not.
     */
    public boolean isSeparate() {
        return getSeparateKeyword() != null;
    }

    /**
     * Get every feature from the class "aClassName"
     *
     * @param aClassName The name of the class to get the features
     * @return The features of the class
     */
    private @NotNull List<EiffelFeature> getFeaturesFromClassName(String aClassName) {
        List<EiffelFeature> lResult;
        EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(aClassName, getProject());
        if (lClass != null) {
            lResult = lClass.getAllFeatures();
        } else {
            lResult = new ArrayList<>();
        }
        return lResult;
    }

    /**
     * Get the real type of the anchored type with a `Current` anchor
     * @return The type of `Current'
     */
    private @Nullable EiffelType getAnchoredTypeCurrent() {
        EiffelType lResult = null;
        if (getContainingFile() instanceof EiffelFile lFile) {
            EiffelClass lClass = EiffelUtil.getInstance().findClassInFile(lFile);
            if (lClass != null) {
                PsiElement lType = EiffelElementFactory.getInstance().createType(getProject(),
                        lClass.getName());
                if (lType instanceof EiffelType lEiffelType) {
                    lResult = lEiffelType;
                }
            }
        }
        return lResult;
    }

//    /**
//     * Get the type of the variable named `aIdentifier` in `aDeclaration`
//     *
//     * @param aDeclaration The list of declaration to found the variable
//     * @param aIdentifier The name of the variable
//     * @return The Type of the variable. Null if not found.
//     */
//    private @Nullable EiffelType getAnchoredTypeIdentifierDeclaration(
//            EiffelPsiEntityDeclarationList aDeclaration, String aIdentifier) {
//        EiffelType lResult = null;
//        Iterator<EiffelPsiEntityDeclarationGroup> lGroupIterator =
//                aDeclaration.getEntityDeclarationGroupList().iterator();
//        boolean lFoundSelf = false;
//        while (!lFoundSelf && lResult == null && !lGroupIterator.hasNext()) {
//            EiffelPsiEntityDeclarationGroup lGroup = lGroupIterator.next();
//            if (lGroup.getTypeMark().getType() == this) {
//                lFoundSelf = true;
//            } else {
//                Iterator<EiffelPsiDeclarationIdentifier> lIdentifierIterator =
//                        lGroup.getIdentifierList().getDeclarationIdentifierList().iterator();
//                while (lResult == null && !lIdentifierIterator.hasNext()) {
//                    String lIdentifier = lIdentifierIterator.next().getIdentifier().getText();
//                    if (lIdentifier.equalsIgnoreCase(aIdentifier)) {
//                        lResult = (EiffelType) lGroup.getTypeMark().getType();
//                    }
//                }
//            }
//        }
//        return lResult;
//    }

//    /**
//     * Find an expression with a certain name in the current Psi tree.
//     *
//     * @param aIdentifier The name of the variable to found
//     * @return
//     */
//    private @Nullable EiffelType getAnchoredTypeIdentifier(String aIdentifier) {
//        EiffelType lResult = null;
//        PsiElement lCurrentElement = getParent();
//        while (lResult == null && lCurrentElement != null && !(lCurrentElement instanceof EiffelFile)) {
//            if (lCurrentElement instanceof EiffelPsiAttributeOrRoutine lAttributeRoutine &&
//                    lAttributeRoutine.getLocalDeclarations() != null &&
//                    lAttributeRoutine.getLocalDeclarations().getEntityDeclarationList() != null) {
//                lResult = getAnchoredTypeIdentifierDeclaration(
//                        lAttributeRoutine.getLocalDeclarations().getEntityDeclarationList(), aIdentifier);
//            } else if (lCurrentElement instanceof EiffelPsiDeclarationBodyValue lBodyValue &&
//                    lBodyValue.getFormalArguments() != null &&
//                    lBodyValue.getFormalArguments().getEntityDeclarationList() != null) {
//                lResult = getAnchoredTypeIdentifierDeclaration(
//                        lBodyValue.getFormalArguments().getEntityDeclarationList(), aIdentifier);
//            } else if (lCurrentElement instanceof EiffelClass lClass) {
//                Iterator<EiffelFeature> lFeatureIterator = lClass.getAllFeatures().iterator();
//                while (lResult == null && lFeatureIterator.hasNext()) {
//                    EiffelFeature lFeature = lFeatureIterator.next();
//                    if (lFeature.getName().equalsIgnoreCase(aIdentifier)) {
//                        lResult = lFeature.getReturnType();
//                    }
//                }
//            }
//            lCurrentElement = lCurrentElement.getParent();
//        }
//        return lResult;
//    }

    /**
     * Get the Tuple type from this type.
     *
     * @return The Tuple type if this represents a tuple. Null if not.
     */
    public @Nullable EiffelTupleType getTupleType() {
        EiffelTupleType lResult = null;
        if (getClassOrTupleType() != null && getClassOrTupleType().getTupleType() instanceof EiffelTupleType lType) {
            lResult = lType;
        }
        return lResult;
    }

    /**
     * Get the Class type from this type.
     *
     * @return The Class type if this represents a Class. Null if not.
     */
    public @Nullable EiffelClassType getClassType() {
        EiffelClassType lResult = null;
        if (getClassOrTupleType() != null && getClassOrTupleType().getClassType() instanceof EiffelClassType lType) {
            lResult = lType;
        }
        return lResult;
    }

    /**
     * Getting the type of Anchor. Might be another Anchored type.
     *
     * @return The anchor type
     */
    public @Nullable EiffelType getAnchoredType() {
        EiffelType lResult = null;
        EiffelPsiAnchored lAnchored = getAnchored();
        if (lAnchored != null) {
            if (lAnchored.getAnchor().getCurrentKeyword() != null) {
                lResult = getAnchoredTypeCurrent();
            } else if (lAnchored.getAnchor().getExpression() != null) {
                // Todo
            }
        }
        return lResult;
    }

    /**
     * Retrieve every feature of the type (even those not exported).
     *
     * @return every features
     */
    public @NotNull List<EiffelAddressable> getFeatures() {
        List<EiffelAddressable> lResult;
        if (getClassType() != null) {
            lResult = getClassType().getFeatures();
        } else if (getAnchored() != null) {
            EiffelType lAnchoredType = getAnchoredType();
            if (lAnchoredType != null) {
                lResult = lAnchoredType.getFeatures();
            } else {
                lResult = new ArrayList<>();
            }
        } else if (getTupleType() != null) {
            lResult = getTupleType().getAddressable();
        } else {
            lResult = new ArrayList<>();
        }
        return lResult;
    }

    /**
     * Retrieve every accessible feature of the type.
     *
     * @return every accessible features
     */
    public @NotNull List<EiffelAddressable> getAccessibleFeatures() {
        List<EiffelAddressable> lResult;
        if (getClassType() != null) {
            lResult = getClassType().getAccessibleFeatures();
        } else if (getAnchored() != null) {
            // Todo: This does not work. Will have to be fixed when the expression type resolution will be done.
            EiffelType lAnchoredType = getAnchoredType();
            if (lAnchoredType != null) {
                lResult = lAnchoredType.getAccessibleFeatures();
            } else {
                lResult = new ArrayList<>();
            }
        } else if (getTupleType() != null) {
            lResult = getTupleType().getAccessibleFeatures();
        } else {
            lResult = new ArrayList<>();
        }
        return lResult;
    }

}
