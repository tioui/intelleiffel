package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.PsiComment;
import net.libreti.intelleiffel.EiffelIcons;
import net.libreti.intelleiffel.models.EiffelFeature;
import net.libreti.intelleiffel.psi.EiffelPsiClassTypeName;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureClause;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureDeclaration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;

import java.util.ArrayList;
import java.util.List;

/**
 * An Eiffel feature clause.
 *
 * @author Louis M
 */
public abstract class EiffelFeatureClause extends ASTWrapperPsiElement
        implements EiffelPsiFeatureClause, ItemPresentation {

    /**
     * Constructor of the object
     *
     * @param aNode The node of the current element.
     */
    public EiffelFeatureClause(ASTNode aNode) {
        super(aNode);
    }

    /**
     * Every feature in this feature clause
     *
     * @return the features
     */
    public @NotNull List<EiffelFeature> getFeatures() {
        List<EiffelFeature> lResult;
        if (getFeatureDeclarationList() != null) {
            lResult = new ArrayList<>(getFeatureDeclarationList().getFeatureDeclarationList().size());
            for (EiffelPsiFeatureDeclaration lFeatureDeclaration : getFeatureDeclarationList().getFeatureDeclarationList()) {
                if (lFeatureDeclaration instanceof EiffelFeatureDeclaration lDeclaration) {
                    lResult.addAll(lDeclaration.getFeatures());
                }
            }
        } else {
            lResult = new ArrayList<>();
        }
        return lResult;
    }

    /**
     * A text the represent the current object.
     *
     * @return A text the represent the current object.
     */
    @Override
    public @Nullable String getPresentableText() {
        String lResult;
        PsiComment lComment = findChildByClass(PsiComment.class);
        if (lComment != null) {
            lResult = lComment.getText().substring(2).trim();
        } else {
            lResult = getFeatureKeyword().getText();
        }
        return lResult;
    }

    /**
     * The icon of the current class.
     *
     * @param unused Used to mean if open/close icons for tree renderer. No longer in use.
     * @return The icon of the current object.
     */
    @Override
    public @Nullable Icon getIcon(boolean unused) {
        Icon lResult;
        if (getClients() != null) {
            if (getClients().getClassList() != null) {
                if (getClients().getClassList().getNoneKeyword() != null) {
                    lResult = EiffelIcons.FEATURE_PRIVATE_ICON;
                } else {
                    lResult = EiffelIcons.FEATURE_FRIENDS_ICON;
                    for (EiffelPsiClassTypeName lClassType : getClients().getClassList().getClassTypeNameList()) {
                        if (lClassType instanceof EiffelClassTypeName lTypeName && lTypeName.isAny()) {
                            lResult = EiffelIcons.FEATURE_PUBLIC_ICON;
                        }
                    }
                }
            } else {
                lResult = EiffelIcons.FEATURE_PRIVATE_ICON;
            }
        } else {
            lResult = EiffelIcons.FEATURE_PUBLIC_ICON;
        }
        return lResult;
    }

}
