package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.models.EiffelNoteDocumentation;
import net.libreti.intelleiffel.psi.EiffelPsiNoteEntry;
import net.libreti.intelleiffel.psi.EiffelPsiNotes;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

/**
 * Mixin for the Grammar rules Notes
 *
 * @author Louis M
 */
public abstract class EiffelNotes extends ASTWrapperPsiElement implements EiffelPsiNotes {


    /**
     * Initialize the mixin
     * @param aNode The syntaxic node
     */
    public EiffelNotes(@NotNull ASTNode aNode) {
        super(aNode);
    }


    /**
     * Return the documentation of the current notes element
     * @return The documentation
     */
    public @NotNull List<EiffelNoteDocumentation> getDocumentation() {
        List<EiffelNoteDocumentation> lResult = new LinkedList<>();
        if (getNoteList() != null) {
            for (EiffelPsiNoteEntry lEntry : getNoteList().getNoteEntryList()) {
                lResult.add(new EiffelNoteDocumentation(lEntry.getIdentifier().getText(),
                        lEntry.getNoteValues().getText().replaceAll("\\\\ ", " ")));
            }
        }
        return lResult;
    }

    /**
     * Text representation of the current object.
     *
     * @return Text representation of the current object.
     */
    @Override
    public String toString() {
        return "Eiffel Notes";
    }
}
