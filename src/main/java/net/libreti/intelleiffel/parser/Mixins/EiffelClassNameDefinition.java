package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import net.libreti.intelleiffel.EiffelIcons;
import net.libreti.intelleiffel.psi.EiffelPsiClassNameDefinition;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;

/**
 * Mixin for the Grammar rules Eiffel_class_name_definition
 *
 * @author Louis M
 */
public abstract class EiffelClassNameDefinition
        extends EiffelClassNameRenamable
        implements EiffelPsiClassNameDefinition, ItemPresentation{

    /**
     * Class creator
     *
     * @param aNode The AST node of the EiffelClassNameDefinition
     */
    public EiffelClassNameDefinition(@NotNull ASTNode aNode) {
        super(aNode);
    }

    @Nullable
    @Override
    public String getPresentableText() {
        return getName();
    }

    /**
     * The icon of the current class.
     *
     * @param unused Used to mean if open/close icons for tree renderer. No longer in use.
     * @return The icon of the current object.
     */
    @Override
    public Icon getIcon(boolean unused) {
        Icon lResult;
        EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(getName(), getProject());
        if (lClass != null && ((EiffelClassHeader) lClass.getClassHeader()).isDeferred()) {
            lResult = EiffelIcons.DEFERRED_CLASS_ICON;
        } else {
            lResult = EiffelIcons.CLASS_ICON;
        }
        return lResult;
    }

    /**
     * The element that present this object.
     *
     * @return the element that represent this object (it is themselves)
     */
    public ItemPresentation getPresentation() {
        return this;
    }
}
