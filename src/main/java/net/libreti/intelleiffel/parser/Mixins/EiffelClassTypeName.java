package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiReference;
import com.intellij.psi.impl.source.resolve.reference.ReferenceProvidersRegistry;
import net.libreti.intelleiffel.psi.EiffelPsiClassTypeName;
import org.jetbrains.annotations.NotNull;

/**
 * Mixin for the Grammar rules Eiffel_class_name_definition
 *
 * @author Louis M
 */
public abstract class EiffelClassTypeName extends EiffelClassNameRenamable implements EiffelPsiClassTypeName {

    /**
     * The ANY (root) class name in Eiffel.
     */
    public final static String ANY_TEXT = "ANY";

    /**
     * Class creator
     *
     * @param aNode The AST node of the EiffelClassNameDefinition
     */
    public EiffelClassTypeName(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * The name of the type
     *
     * @return the type name
     */
    @Override
    public @NotNull String getName() {
        return getClassNameIdentifier().getText();
    }

    /**
     * Return the references to this
     *
     * @return the references
     */
    @NotNull
    @Override
    public PsiReference[] getReferences() {
        return ReferenceProvidersRegistry.getReferencesFromProviders(this);
    }

    /**
     * This is an ANY type (the root class)
     *
     * @return True if the type is an any type
     */
    public boolean isAny() {
        return ANY_TEXT.equals(getClassNameIdentifier().getIdentifier().getText());
    }

}
