package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.psi.EiffelPsiAlias;
import org.jetbrains.annotations.NotNull;

/**
 * A feature alias.
 *
 * @author Louis M
 */
public abstract class EiffelAlias extends ASTWrapperPsiElement implements EiffelPsiAlias {

    /**
     * Initialize the mixin
     * @param aNode The syntaxic node
     */
    public EiffelAlias(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * The name (text) of the alias
     *
     * @return the alias text
     */
    @Override
    public String getName() {
        return getString().getText();
    }

    /**
     * This alias can be used to convert types
     *
     * @return True if this alias can be used to convert
     */
    public boolean isConvert() {
        return getConvertKeyword() != null;
    }

}
