package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import net.libreti.intelleiffel.EiffelIcons;
import net.libreti.intelleiffel.models.EiffelFeature;
import net.libreti.intelleiffel.psi.EiffelPsiAssertionClause;
import net.libreti.intelleiffel.psi.EiffelPsiDeclarationBody;
import net.libreti.intelleiffel.psi.EiffelPsiDeclarationBodyValue;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureBody;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureDeclaration;
import net.libreti.intelleiffel.psi.EiffelPsiInternal;
import net.libreti.intelleiffel.psi.EiffelPsiNewFeature;
import net.libreti.intelleiffel.psi.EiffelPsiPostcondition;
import net.libreti.intelleiffel.psi.EiffelPsiQueryMark;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A declaration of an Eiffel feature
 *
 * @author Louis M
 */
public abstract class EiffelFeatureDeclaration extends ASTWrapperPsiElement
        implements EiffelPsiFeatureDeclaration, ItemPresentation {


    /**
     * Constructor of the object
     * @param aNode The element Psi node.
     */
    public EiffelFeatureDeclaration(ASTNode aNode) {
        super(aNode);
    }

    /**
     * The Feature names Psi elements
     *
     * @return the elements
     */
    public List<EiffelFeatureName> getFeatureNames() {
        List<EiffelFeatureName> lResult = new LinkedList<>();
        for (EiffelPsiNewFeature lFeatureName: getNewFeatureList().getNewFeatureList()) {
            lResult.add((EiffelFeatureName) lFeatureName);
        }
        return lResult;
    }

    /**
     * Every eiffel features in this feature definition (a feature definition can have multiple names)
     * @return the features in this declaration
     */
    public List<EiffelFeature> getFeatures() {
        List<EiffelFeatureName> lNames = getFeatureNames();
        List<EiffelFeature> lResult = new ArrayList<>(lNames.size());
        EiffelPsiDeclarationBody lBody = getDeclarationBody();
        for (EiffelFeatureName lFeatureName: lNames) {
            lResult.add(new EiffelFeature(lFeatureName, lBody));
        }
        return lResult;
    }

    /**
     * Retreive a feature from a feature name.
     *
     * @param aFeatureNane The feature name
     * @return The feature
     */
    public @Nullable EiffelFeature getFeatureFromName(EiffelFeatureName aFeatureNane) {
        EiffelFeature lResult = null;
        for (EiffelPsiNewFeature lFeatureName : getNewFeatureList().getNewFeatureList()) {
            if (lFeatureName == aFeatureNane) {
                lResult = new EiffelFeature(aFeatureNane, getDeclarationBody());
            }
        }
        return lResult;
    }

    /**
     * A text the represent the current object.
     *
     * @return A text the represent the current object.
     */
    @Override
    public @Nullable String getPresentableText() {
        StringBuilder lResult = new StringBuilder();
        List<EiffelFeatureName> lNames = getFeatureNames();
        boolean lFirstDone = false;
        for (EiffelFeatureName lFeatureName: lNames) {
            if (lFeatureName.getPresentation() != null) {
                if (lFirstDone) {
                    lResult.append(", ");
                } else {
                    lFirstDone = true;
                }
                lResult.append(lFeatureName.getPresentation().getPresentableText());
            }
        }
        return !lResult.isEmpty() ? lResult.toString() : null;
    }



    /**
     * The feature is a routine
     *
     * @return True if the feature is a routine. False if not.
     */
    public boolean is_routine() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = getDeclarationBody().getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                lResult = lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody().getEffectiveRoutine() != null;
            }
        }
        return lResult;
    }

    /**
     * The feature is a routine
     *
     * @return True if the feature is a routine. False if not.
     */
    public boolean has_attribute_routine() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = getDeclarationBody().getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                lResult = lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody().getAttribute() != null;
            }
        }
        return lResult;
    }

    /**
     * The feature is an external routine
     *
     * @return True if the feature is an external routine. False if not.
     */
    public boolean is_external() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = getDeclarationBody().getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                EiffelPsiFeatureBody lBody = lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody();
                if (lBody.getEffectiveRoutine() != null) {
                    lResult = lBody.getEffectiveRoutine().getExternalRoutine() != null;
                }

            }
        }
        return lResult;
    }

    /**
     * The feature is a once feature
     *
     * @return True if the feature is a once feature. False if not.
     */
    public boolean is_once() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = getDeclarationBody().getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                EiffelPsiFeatureBody lBody = lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody();
                if (lBody.getEffectiveRoutine() != null) {
                    EiffelPsiInternal lInternal = lBody.getEffectiveRoutine().getInternal();
                    if (lInternal != null) {
                        lResult = lInternal.getRoutineMark().getOnce() != null;
                    }
                }

            }
        }
        return lResult;
    }

    /**
     * Is the feature obsolete
     *
     * @return True if the feature is obsolete. False if not.
     */
    public boolean is_obsolete() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = getDeclarationBody().getDeclarationBodyValue();
        if (lValue != null) {
            lResult = lValue.getFeatureValue().getObsolete() != null;
        }
        return lResult;
    }

    /**
     * Is the feature deferred.
     *
     * @return True if the feature is deferred. False if not.
     */
    public boolean is_deferred() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = getDeclarationBody().getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                lResult =
                        lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody().getDeferredKeyword() != null;
            }
        }
        return lResult;
    }

    /**
     * Is the feature an instance free routine.
     *
     * @return True if the feature is an instance free routine. False if not.
     */
    public boolean is_instance_free_routine() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = getDeclarationBody().getDeclarationBodyValue();
        if (lValue != null) {
            if (lValue.getFeatureValue().getAttributeOrRoutine() != null) {
                EiffelPsiPostcondition lPostcondition =
                        lValue.getFeatureValue().getAttributeOrRoutine().getPostcondition();
                if (lPostcondition != null && lPostcondition.getAssertion() != null) {
                    for (EiffelPsiAssertionClause lClause : lPostcondition.getAssertion().getAssertionClauseList()) {
                        if (lClause.getUnlabeledAssertionClause() != null) {
                            lResult = lClause.getUnlabeledAssertionClause().getClassKeyword() != null;
                        }
                    }
                }
                lResult =
                        lValue.getFeatureValue().getAttributeOrRoutine().getFeatureBody().getDeferredKeyword() != null;
            }
        }
        return lResult;
    }

    /**
     * Is the feature deferred.
     *
     * @return True if the feature is deferred. False if not.
     */
    public boolean is_constant() {
        boolean lResult = false;
        EiffelPsiDeclarationBodyValue lValue = getDeclarationBody().getDeclarationBodyValue();
        if (lValue != null) {
            lResult = lValue.getFeatureValue().getExplicitValue() != null;
        }
        return lResult;
    }

    /**
     * Is the feature deferred.
     *
     * @return True if the feature is deferred. False if not.
     */
    public boolean is_attribute() {
        return getDeclarationBody().getQueryMark() != null || has_attribute_routine();
    }

    /**
     * If there is an assigner in the feature, return the name.
     *
     * @return The name of the assigner of the feature if any. Null if no assigner.
     */
    public @Nullable String getAssignerName() {
        String lResult = null;
        EiffelPsiQueryMark lMark = getDeclarationBody().getQueryMark();
        if (lMark == null) {
            EiffelPsiDeclarationBodyValue lValue = getDeclarationBody().getDeclarationBodyValue();
            if (lValue != null) {
                lMark = lValue.getQueryMark();
            }
        }
        if (lMark != null) {
            if (lMark.getAssignerMark() != null) {
                lResult = lMark.getAssignerMark().getFeatureName().getIdentifier().getText();
            }
        }
        return lResult;
    }

    /**
     * If there is a return type in the feature, return the type.
     *
     * @return The return type of the feature if any. Null if no return type.
     */
    public @Nullable EiffelType getReturnType() {
        EiffelType lResult = null;
        EiffelPsiQueryMark lMark = getDeclarationBody().getQueryMark();
        if (lMark == null) {
            EiffelPsiDeclarationBodyValue lValue = getDeclarationBody().getDeclarationBodyValue();
            if (lValue != null) {
                lMark = lValue.getQueryMark();
            }
        }
        if (lMark != null) {
            lResult = ((EiffelType)lMark.getTypeMark().getType());
        }
        return lResult;
    }

    /**
     * The icon of the current class.
     *
     * @param unused Used to mean if open/close icons for tree renderer. No longer in use.
     * @return The icon of the current object.
     */
    @Override
    public @Nullable Icon getIcon(boolean unused) {
        Icon lResult;
        List<EiffelFeature> lFeatures = getFeatures();
        if (lFeatures.size() != 1) {
            lResult = EiffelIcons.FEATURE_METHOD_ICON;
            if (is_obsolete()) {
                lResult = EiffelIcons.FEATURE_OBSOLETE_ICON;
            } else if (is_deferred()) {
                if (is_instance_free_routine()) {
                    lResult = EiffelIcons.FEATURE_DEFERRED_INSTANCE_FREE_ICON;
                } else {
                    lResult = EiffelIcons.FEATURE_DEFERRED_ICON;
                }
            } else if (is_constant()) {
                lResult = EiffelIcons.FEATURE_CONSTANT_ICON;
            } else if (is_external()) {
                lResult = EiffelIcons.FEATURE_EXTERNAL_ICON;
            } else if (is_once()) {
                lResult = EiffelIcons.FEATURE_ONCE_ICON;
            } else if (is_instance_free_routine()) {
                lResult = EiffelIcons.FEATURE_INSTANCE_FREE_ICON;
            } else if (is_attribute()) {
                lResult = EiffelIcons.FEATURE_ATTRIBUTE_ICON;
            }
        } else {
            lResult = lFeatures.get(0).getIcon(unused);
        }
        return lResult;
    }
}
