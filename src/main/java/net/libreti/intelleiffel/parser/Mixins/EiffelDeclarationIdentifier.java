package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.NlsSafe;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiNamedElement;
import com.intellij.util.IncorrectOperationException;
import net.libreti.intelleiffel.parser.EiffelElementFactory;
import net.libreti.intelleiffel.psi.EiffelPsiAttachedTest;
import net.libreti.intelleiffel.psi.EiffelPsiAttributeOrRoutine;
import net.libreti.intelleiffel.psi.EiffelPsiBinaryExpression;
import net.libreti.intelleiffel.psi.EiffelPsiBooleanExpression;
import net.libreti.intelleiffel.psi.EiffelPsiConditional;
import net.libreti.intelleiffel.psi.EiffelPsiDeclarationBodyValue;
import net.libreti.intelleiffel.psi.EiffelPsiDeclarationIdentifier;
import net.libreti.intelleiffel.psi.EiffelPsiEntityIdentifier;
import net.libreti.intelleiffel.psi.EiffelPsiExitCondition;
import net.libreti.intelleiffel.psi.EiffelPsiExpression;
import net.libreti.intelleiffel.psi.EiffelPsiExpressionNoBinary;
import net.libreti.intelleiffel.psi.EiffelPsiFormalArguments;
import net.libreti.intelleiffel.psi.EiffelPsiIteration;
import net.libreti.intelleiffel.psi.EiffelPsiLocalDeclarations;
import net.libreti.intelleiffel.psi.EiffelPsiLoop;
import net.libreti.intelleiffel.psi.EiffelPsiLoopSymbolic;
import net.libreti.intelleiffel.psi.EiffelPsiLoopSymbolicDeclaration;
import net.libreti.intelleiffel.psi.EiffelPsiLoopSymbolicExpression;
import net.libreti.intelleiffel.psi.EiffelPsiOperatorExpression;
import net.libreti.intelleiffel.psi.EiffelPsiThenPart;
import net.libreti.intelleiffel.psi.EiffelPsiThenPartExpression;
import net.libreti.intelleiffel.psi.EiffelPsiThenPartExpressionList;
import net.libreti.intelleiffel.psi.EiffelPsiThenPartList;
import net.libreti.intelleiffel.psi.EiffelTypes;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


/**
 * Mixin for the Grammar rules Eiffel_declaration_identifier
 *
 * Todo:
 *      - When the method to get classes feature will be created, validate that renamed identifier are not in
 *        the classes features.
 *      - The local assignment are not final. It does not manage unary and binary boolean expression right.
 *        Example1:
 * 			            from
 * 		            	until
 * 		            		attached l_test1 as la_test
 * 		            	loop
 * 		            		if attached l_test2 as la_test then
 * 		            			print(la_test)
 * 		            		end
 * 		            	end
 * 		      In this example, since the attached of the "until" section is not negate, the "la_test" of
 * 		      the until section is not accessible in the loop section. This make it possible for the
 * 		      "la_test" in the "until" section and the "la_test" of the "if" to coexist. The instrction
 * 		      "print(la_test)" will use the "la_test" of the "if".
 * 		      So, when I rename the "la_test" of the "until" section, it should not change the "la_test"
 * 		      of the "if" section. Currently, it does.
 * 		  Example2:
 * 			            if is_valid and attached l_test1 as la_test then
 *                          print(la_test)
 *     			        else
 *     		        		if attached l_test2 as la_test then
 *     		        			print(la_test)
 *     			        	end
 *     			        end
 *            In this example, if I rename "la_test" of the first "if", only the first "print(la_test)" should
 *            be renamed. But currently, the second "print(la_Test)" is also renamed.
 *
 * @author Louis M
 */
public abstract class EiffelDeclarationIdentifier extends ASTWrapperPsiElement implements EiffelPsiDeclarationIdentifier, PsiNamedElement {

    /**
     * Class creator
     *
     * @param aNode The AST node of the EiffelDeclarationIdentifier
     */
    public EiffelDeclarationIdentifier(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /*
     * Replace `aChild` by a new Node containing `aName` in `aParent`
     *
     * @param aParent The parent that will contain the new Node
     * @param aChild  The child to add the new node
     * @param aName   The content of the new node
     * @throws IncorrectOperationException When `aName` is not valid
     */
    private void replaceElement(PsiElement aParent, PsiElement aChild, String aName)
            throws IncorrectOperationException {
        PsiElement lNewElement = EiffelElementFactory.getInstance().createEiffelElement(getProject(), aName,
                EiffelTypes.EIF_IDENTIFIER);
        if (lNewElement != null) {
            aParent.getNode().replaceChild(aChild.getNode(), lNewElement.getNode());
        } else {
            throw new IncorrectOperationException(aName + " is not a valid identifier");
        }
    }

    /**
     * Validate that `aName` is not an already used identifier in the tree rooted at `aElement`.
     * <p>
     * Does nothing if `aName` is not found in the tree. Launch an exception if it is found.
     *
     * @param aElement The root element of the tree to validate
     * @param aName    The name that should not pe found in the tree
     * @throws IncorrectOperationException If `aName` is found in the tree
     */
    private void validateIdentifierNotFound(PsiElement aElement, String aName) throws IncorrectOperationException {
        if (aElement instanceof EiffelPsiDeclarationIdentifier &&
                ((EiffelPsiDeclarationIdentifier) aElement).getIdentifier().getText().toUpperCase().equals(
                        aName.toUpperCase())) {
            throw new IncorrectOperationException("The identifier " + aName +
                    " is already used in the renaming space.");
        } else if (aElement instanceof EiffelPsiEntityIdentifier &&
                ((EiffelPsiEntityIdentifier) aElement).getIdentifier().getText().toUpperCase().equals(
                        aName.toUpperCase())) {
            throw new IncorrectOperationException("The identifier " + aName +
                    " is already used in the renaming space.");
        }
    }

    /**
     * Iteration part of executeRename.
     *
     * @param aElement The element to search for Entities
     * @param aOldName The name to search in `aElement`.
     * @param aNewName The new name to put in Entity_identifier
     * @throws IncorrectOperationException When `aNewName` is not valid
     * @see EiffelDeclarationIdentifier#executeRename
     */
    private void executeRenameIteration(PsiElement aElement, String aOldName, String aNewName)
            throws IncorrectOperationException {
        if (aElement instanceof EiffelPsiEntityIdentifier) {
            EiffelPsiEntityIdentifier lEntity = (EiffelPsiEntityIdentifier) aElement;
            if (lEntity.getIdentifier().getText().toUpperCase().equals(aOldName.toUpperCase())) {
                replaceElement(lEntity, lEntity.getIdentifier(), aNewName);
            }
        } else {
            for (PsiElement lElement : aElement.getChildren()) {
                executeRenameIteration(lElement, aOldName, aNewName);
            }
        }
    }


    /**
     * Rename every Entity_identifier in `aElement` from `aOldName` to `aNewName`
     *
     * @param aElement The element to search for Entities
     * @param aOldName The name to search in `aElement`.
     * @param aNewName The new name to put in Entity_identifier
     * @throws IncorrectOperationException When `aNewName` is not valid
     */
    private void executeRename(PsiElement aElement, String aOldName, String aNewName)
            throws IncorrectOperationException {
        validateIdentifierNotFound(aElement, aNewName);
        executeRenameIteration(aElement, aOldName, aNewName);
    }

    /**
     * Rename the Entity_identifier in `aLocalDeclaration` from `aOldName` to `aNewName`
     *
     * @param aLocalDeclaration The EiffelLocalDeclarations node
     * @param aOldName          The name to search in `aLocalDeclaration`.
     * @param aNewName          The new name to put in `aLocalDeclaration`
     * @throws IncorrectOperationException When `aNewName` is not valid
     */
    private void executeRenameLocal(EiffelPsiLocalDeclarations aLocalDeclaration, String aOldName, String aNewName)
            throws IncorrectOperationException {
        PsiElement lParent = aLocalDeclaration.getParent();
        if (lParent instanceof EiffelPsiAttributeOrRoutine) {
            EiffelPsiAttributeOrRoutine lAttributeOrRoutine = (EiffelPsiAttributeOrRoutine) lParent;
            executeRename(lAttributeOrRoutine.getFeatureBody(), aOldName, aNewName);
            if (lAttributeOrRoutine.getRescue() != null) {
                executeRename(lAttributeOrRoutine.getRescue(), aOldName, aNewName);
            }
        } else {
            throw new IncorrectOperationException("Cannot rename this identifier.");
        }
    }

    /**
     * Rename the Entity_identifier in `aFormalArguments` from `aOldName` to `aNewName`
     *
     * @param aFormalArguments The EiffelFormalArguments node
     * @param aOldName         The name to search in `aFormalArguments`.
     * @param aNewName         The new name to put in `aFormalArguments`
     * @throws IncorrectOperationException When `aNewName` is not valid
     */
    private void executeRenameArgument(EiffelPsiFormalArguments aFormalArguments, String aOldName, String aNewName)
            throws IncorrectOperationException {
        PsiElement lParent = aFormalArguments.getParent();
        if (lParent instanceof EiffelPsiDeclarationBodyValue) {
            EiffelPsiDeclarationBodyValue lDeclarationBody = (EiffelPsiDeclarationBodyValue) lParent;
            executeRename(lDeclarationBody, aOldName, aNewName);
        } else {
            throw new IncorrectOperationException("Cannot rename this identifier.");
        }
    }

    /**
     * Rename every Entity_identifier in `aIteration` from `aOldName` to `aNewName`
     *
     * @param aIteration The EiffelIteration node
     * @param aOldName   The name to search in `aIteration`.
     * @param aNewName   The new name to put in `aIteration`
     * @throws IncorrectOperationException When `aNewName` is not valid
     */
    private void executeRenameIteration(EiffelPsiIteration aIteration, String aOldName, String aNewName)
            throws IncorrectOperationException {
        executeRename(aIteration.getExpression(), aOldName, aNewName);
    }

    /**
     * Then_part part of executeRenameAttachedTestExpression
     *
     * @param aThenPart The EiffelThenPart node
     * @param aOldName  The name to search in `aExpression`.
     * @param aNewName  The new name to put in `aExpression`
     * @throws IncorrectOperationException When `aNewName` is not valid
     */
    private void executeRenameAttachedTestExpressionThenPart(EiffelPsiThenPart aThenPart, String aOldName, String aNewName)
            throws IncorrectOperationException {
        if (aThenPart.getCompound() != null) {
            executeRename(aThenPart.getCompound(), aOldName, aNewName);
        }
        if (aThenPart.getParent() instanceof EiffelPsiThenPartList) {
            EiffelPsiThenPartList lThenPartList = (EiffelPsiThenPartList) (aThenPart.getParent());
            boolean lFound = false;
            for (EiffelPsiThenPart lThenPart : lThenPartList.getThenPartList()) {
                if (lFound) {
                    executeRename(lThenPart, aOldName, aNewName);
                } else {
                    if (lThenPart == aThenPart) {
                        lFound = true;
                    }
                }
            }
            PsiElement lParent = aThenPart.getParent().getParent();
            if (lParent instanceof EiffelPsiConditional && ((EiffelPsiConditional) lParent).getElsePart() != null) {
                executeRename(((EiffelPsiConditional) lParent).getElsePart(), aOldName, aNewName);
            }
        }
    }

    /**
     * Then_part_expression part of executeRenameAttachedTestExpression
     *
     * @param aThenPart The EiffelThenPartExpression node
     * @param aOldName  The name to search in `aExpression`.
     * @param aNewName  The new name to put in `aExpression`
     * @throws IncorrectOperationException When `aNewName` is not valid
     */
    private void executeRenameAttachedTestExpressionThenPartExpression(EiffelPsiThenPartExpression aThenPart,
                                                                       String aOldName, String aNewName)
            throws IncorrectOperationException {
        executeRename(aThenPart.getExpression(), aOldName, aNewName);
        if (aThenPart.getParent() instanceof EiffelPsiThenPartList) {
            EiffelPsiThenPartExpressionList lThenPartList = (EiffelPsiThenPartExpressionList) (aThenPart.getParent());
            boolean lFound = false;
            for (EiffelPsiThenPartExpression lThenPart : lThenPartList.getThenPartExpressionList()) {
                if (lFound) {
                    executeRename(lThenPart, aOldName, aNewName);
                } else {
                    if (lThenPart == aThenPart) {
                        lFound = true;
                    }
                }
            }
            PsiElement lParent = aThenPart.getParent().getParent();
            if (lParent instanceof EiffelPsiConditional && ((EiffelPsiConditional) lParent).getElsePart() != null) {
                executeRename(((EiffelPsiConditional) lParent).getElsePart(), aOldName, aNewName);
            }
        }
    }

    /**
     * Expression part of executeRenameAttachedTest
     *
     * @param aExpression The EiffelExpression node
     * @param aOldName    The name to search in `aExpression`.
     * @param aNewName    The new name to put in `aExpression`
     * @throws IncorrectOperationException When `aNewName` is not valid
     */
    private void executeRenameAttachedTestExpression(EiffelPsiExpression aExpression, String aOldName, String aNewName)
            throws IncorrectOperationException {
        PsiElement lParent = aExpression.getParent();
        if (lParent instanceof EiffelPsiBinaryExpression) {
            lParent = lParent.getParent();
            if (lParent instanceof EiffelPsiExpression) {
                executeRenameAttachedTestExpression((EiffelPsiExpression) lParent, aOldName, aNewName);
            } else if (lParent instanceof EiffelPsiOperatorExpression) {
                lParent = lParent.getParent().getParent().getParent(); // Boolean_expression's parent
                if (lParent instanceof EiffelPsiThenPart) {
                    executeRenameAttachedTestExpressionThenPart((EiffelPsiThenPart) lParent, aOldName, aNewName);
                } else if (lParent instanceof EiffelPsiThenPartExpression) {
                    executeRenameAttachedTestExpressionThenPartExpression((EiffelPsiThenPartExpression) lParent, aOldName,
                            aNewName);
                } else if (lParent instanceof EiffelPsiExitCondition) {
                    PsiElement lLoop = ((EiffelPsiExitCondition) lParent).getParent();
                    if (lLoop instanceof EiffelPsiLoop) {
                        executeRename(((EiffelPsiLoop) lLoop).getLoopBody(), aOldName, aNewName);
                    }
                }
            }
        }

    }

    /**
     * Rename every Entity_identifier in `aAttachedTest` from `aOldName` to `aNewName`
     *
     * @param aAttachedTest The EiffelAttachedTest node
     * @param aOldName      The name to search in `aAttachedTest`.
     * @param aNewName      The new name to put in `aAttachedTest`
     * @throws IncorrectOperationException When `aNewName` is not valid
     */
    private void executeRenameAttachedTest(EiffelPsiAttachedTest aAttachedTest, String aOldName, String aNewName)
            throws IncorrectOperationException {
        PsiElement lParent = aAttachedTest.getParent();
        if (lParent instanceof EiffelPsiBooleanExpression) {
            lParent = lParent.getParent();
            if (lParent instanceof EiffelPsiThenPartExpression) {
                executeRename(((EiffelPsiThenPartExpression) lParent).getExpression(),
                        aOldName, aNewName);
            } else if (lParent instanceof EiffelPsiThenPart && ((EiffelPsiThenPart) lParent).getCompound() != null) {
                executeRename(((EiffelPsiThenPart) lParent).getCompound(), aOldName, aNewName);
            }
        } else if (lParent instanceof EiffelPsiExpressionNoBinary) {
            lParent = lParent.getParent();
            if (lParent instanceof EiffelPsiBinaryExpression) {
                executeRename(((EiffelPsiBinaryExpression) lParent).getExpression(), aOldName, aNewName);
            } else if (lParent instanceof EiffelPsiExpression) {
                executeRenameAttachedTestExpression((EiffelPsiExpression) lParent, aOldName, aNewName);
            }
        }
    }

    /**
     * Rename every Entity_identifier in `aLoop` from `aOldName` to `aNewName`
     *
     * @param aLoop The EiffelPsiLoopSymbolic node
     * @param aOldName      The name to search in `aAttachedTest`.
     * @param aNewName      The new name to put in `aAttachedTest`
     * @throws IncorrectOperationException When `aNewName` is not valid
     */
    private void executeRenameLoopSymbolic(EiffelPsiLoopSymbolic aLoop, String aOldName, String aNewName)
            throws IncorrectOperationException {
        PsiElement lCompound = aLoop.getCompound();
        if (lCompound != null) {
            executeRename(lCompound, aOldName, aNewName);
        }
    }

    /**
     * Rename every Entity_identifier in `aLoop` from `aOldName` to `aNewName`
     *
     * @param aLoop The EiffelPsiLoopSymbolic node
     * @param aOldName      The name to search in `aAttachedTest`.
     * @param aNewName      The new name to put in `aAttachedTest`
     * @throws IncorrectOperationException When `aNewName` is not valid
     */
    private void executeRenameLoopSymbolicExpression(EiffelPsiLoopSymbolicExpression aLoop, String aOldName,
                                                     String aNewName) throws IncorrectOperationException {
        PsiElement lExpression = aLoop.getBooleanExpression();
        executeRename(lExpression, aOldName, aNewName);
    }

    /**
     * Rename (refactor) the name of the declaration identifier.
     *
     * @param aNewName the new name of the identifier
     * @return Always return the current object (this)
     * @throws IncorrectOperationException When the new name is not valid
     */
    @Override
    public PsiElement setName(@NlsSafe @NotNull String aNewName) throws IncorrectOperationException {
        try {
            PsiElement lParentElement = getParent();
            if (lParentElement instanceof EiffelPsiIteration) {
                executeRenameIteration((EiffelPsiIteration)lParentElement, getName(), aNewName);
            } else if (lParentElement instanceof EiffelPsiAttachedTest) {
                executeRenameAttachedTest((EiffelPsiAttachedTest) lParentElement, getName(), aNewName);
            } else if (lParentElement instanceof EiffelPsiLoopSymbolicDeclaration) {
                lParentElement = lParentElement.getParent();
                if (lParentElement instanceof EiffelPsiLoopSymbolic) {
                    executeRenameLoopSymbolic((EiffelPsiLoopSymbolic)lParentElement, getName(), aNewName);
                } else if (lParentElement instanceof EiffelPsiLoopSymbolicExpression) {
                    executeRenameLoopSymbolicExpression((EiffelPsiLoopSymbolicExpression)lParentElement, getName(),
                            aNewName);
                } else {
                    throw new IncorrectOperationException("Cannot rename this identifier.");
                }
            } else {
                lParentElement = lParentElement.getParent().getParent().getParent();
                if (lParentElement instanceof EiffelPsiLocalDeclarations) {
                    executeRenameLocal((EiffelPsiLocalDeclarations)lParentElement, getName(), aNewName);
                } else if (lParentElement instanceof EiffelPsiFormalArguments) {
                    executeRenameArgument((EiffelPsiFormalArguments)lParentElement, getName(), aNewName);
                } else {
                    throw new IncorrectOperationException("Cannot rename this identifier.");
                }
            }

        } catch (NullPointerException laException) {
            throw new IncorrectOperationException("Cannot rename this identifier.");
        }
        replaceElement(this, getIdentifier(), aNewName);
        return this;
    }

    /**
     * Get the name of the definition identifier.
     *
     * @return The name of the class
     */
    @Nullable
    @NonNls
    @Override
    public String getName() {
        return getIdentifier().getText();
    }

    /**
     * Text representation of the current object.
     *
     * @return Text representation of the current object.
     */
    @Override
    public String toString() {
        return "Identifier of declaration";
    }


}
