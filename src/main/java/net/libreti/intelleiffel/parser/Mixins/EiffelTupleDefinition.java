package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import net.libreti.intelleiffel.EiffelIcons;
import net.libreti.intelleiffel.psi.EiffelPsiTupleDefinition;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;

/**
 * Represente the TUPLE class definition
 *
 * @author Louis M
 */
public abstract class EiffelTupleDefinition extends ASTWrapperPsiElement implements EiffelPsiTupleDefinition, ItemPresentation {

    /**
     * Creator of the element
     * @param aNode the Psi element node.
     */
    public EiffelTupleDefinition(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * A text the represent the current object.
     *
     * @return A text the represent the current object.
     */
    @Override
    public @Nullable String getPresentableText() {
        return getTupleKeyword().getText();
    }

    /**
     * The icon of the current class.
     *
     * @param unused Used to mean if open/close icons for tree renderer. No longer in use.
     * @return The icon of the current object.
     */
    @Override
    public @Nullable Icon getIcon(boolean unused) {
        return EiffelIcons.CLASS_ICON;
    }
}
