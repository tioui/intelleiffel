package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.models.EiffelAddressable;
import net.libreti.intelleiffel.models.EiffelTupleParameter;
import net.libreti.intelleiffel.psi.EiffelFile;
import net.libreti.intelleiffel.psi.EiffelPsiTupleDeclarationGroup;
import net.libreti.intelleiffel.psi.EiffelPsiTupleIdentifier;
import net.libreti.intelleiffel.psi.EiffelPsiTupleType;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A tuple Eiffel type
 *
 * @author Louis M
 */
public abstract class EiffelTupleType extends ASTWrapperPsiElement
        implements EiffelPsiTupleType {

    /**
     * Constructor of the object
     * @param aNode The element Psi node.
     */
    public EiffelTupleType(ASTNode aNode) {
        super(aNode);
    }

    /**
     * Fill a list of Eiffel addressable with the tuple parameters
     *
     * @param aAddressables The list of Eiffel addressable
     */
    private void fillParameters(List<EiffelAddressable> aAddressables) {
        if (getTupleParameterList() != null &&
                getTupleParameterList().getTupleParameters() != null &&
                getTupleParameterList().getTupleParameters().getTupleDeclarationList() != null) {
            for (EiffelPsiTupleDeclarationGroup lGroup : getTupleParameterList().getTupleParameters().
                    getTupleDeclarationList().getTupleDeclarationGroupList()) {
                EiffelType lType = (EiffelType)lGroup.getTypeMark().getType();
                for (EiffelPsiTupleIdentifier lIdentifier : lGroup.getTupleIdentifierList().getTupleIdentifierList()) {
                    aAddressables.add(new EiffelTupleParameter(lIdentifier, lType));
                }
            }
        }
    }

    /**
     * Get every feature from a tuple object (including generics)
     *
     * @return The features of the tuple
     */
    public @NotNull List<EiffelAddressable> getAddressable() {
        List<EiffelAddressable> lResult;
        EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(getTupleKeyword().getText(), getProject());
        if (lClass != null) {
            lResult = new LinkedList<>(lClass.getAllFeatures());
        } else {
            lResult = new ArrayList<>();
        }
        fillParameters(lResult);
        return lResult;
    }

    /**
     * Retrieve every accessible feature in the type class from the current class
     *
     * @return every accessibles features
     */
    public List<EiffelAddressable> getAccessibleFeatures() {
        List<EiffelAddressable> lResult;
        EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(getTupleKeyword().getText(), getProject());
        if (lClass != null) {
            lResult = new LinkedList<>(lClass.getExportedFeatures());
        } else {
            lResult = new ArrayList<>();
        }
        fillParameters(lResult);
        return lResult;
    }

}
