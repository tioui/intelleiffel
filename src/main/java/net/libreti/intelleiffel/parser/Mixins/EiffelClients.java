package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.psi.EiffelPsiClassTypeName;
import net.libreti.intelleiffel.psi.EiffelPsiClients;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * An Eiffel client for information hiding
 *
 * @author Louis M
 */
public abstract class EiffelClients extends ASTWrapperPsiElement implements EiffelPsiClients {

    /**
     * Creator of the object
     *
     * @param aNode The AST node of the Client element
     */
    public EiffelClients(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * Validate that a type is conformed (can use) to the client list
     *
     * @param aTypeName The type name to validate the conformity
     * @return True if the type is conformed to this. False if not.
     */
    public boolean isConform(String aTypeName) {
        boolean lResult = false;
        if (getClassList() != null) {
            if (getClassList().getNoneKeyword() == null) {
                for (EiffelPsiClassTypeName lName : getClassList().getClassTypeNameList()) {
                    EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(aTypeName, getProject());
                    if (lClass != null) {
                        List<String> lParent = lClass.getAncestorNames();
                        for (String lParentName : lParent) {
                            if (((EiffelClassTypeName)lName).getName().equalsIgnoreCase(lParentName)) {
                                lResult = true;
                            }
                        }
                    } else {
                        lResult = ((EiffelClassTypeName)lName).getName().equalsIgnoreCase(aTypeName);
                    }
                }
            }
        }
        return lResult;
    }

}
