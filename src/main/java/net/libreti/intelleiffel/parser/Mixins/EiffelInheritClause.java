package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.psi.EiffelPsiInheritClause;
import net.libreti.intelleiffel.psi.EiffelPsiParent;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * An inherit clause (containing parent and adaptations
 */
public abstract class EiffelInheritClause extends EiffelFeaturable implements EiffelPsiInheritClause {

    /**
     * The constructor of the object
     *
     * @param aNode The node of the element
     */
    public EiffelInheritClause(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * The inherit clause is not Non-Conformance
     *
     * @return True if the inherit clause is not Non-Conformance
     */
    public boolean has_conformance() {
        return getNonConformance() != null;
    }

    /**
     * The list of featurable to used in this object
     *
     * @return the list of featurables
     */
    @Override
    protected List<EiffelFeaturable> getFeaturableList() {
        List<EiffelFeaturable> lResult = new ArrayList<>(getParentList().getParentList().size());
        for (EiffelPsiParent lParent: getParentList().getParentList()) {
            lResult.add((EiffelFeaturable) lParent);
        }
        return lResult;
    }

}
