package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.models.EiffelFeature;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureClause;
import net.libreti.intelleiffel.psi.EiffelPsiFeatures;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

/**
 * The list of feature clause in an Eiffel code
 *
 * @author Louis M
 */
public abstract class EiffelFeatureList extends ASTWrapperPsiElement
        implements EiffelPsiFeatures {

    /**
     * Creator of the object
     *
     * @param aNode The AST node of the element
     */
    public EiffelFeatureList(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * Every features in the feature list
     *
     * @return every feature in this.
     */
    public @NotNull List<EiffelFeature> getFeatures() {
        List<EiffelFeature> lResult = new LinkedList<>();
        for (EiffelPsiFeatureClause lClause : getFeatureClauseList()) {
            lResult.addAll(((EiffelFeatureClause) lClause).getFeatures());
        }
        return lResult;
    }

    /**
     * The features in the feature list that are public
     *
     * @return the public features.
     */
    public @NotNull List<EiffelFeature> getExportedFeatures() {
        List<EiffelFeature> lResult = new LinkedList<>();
        for (EiffelPsiFeatureClause lClause : getFeatureClauseList()) {
            if (lClause.getClients() != null) {
                if (((EiffelClients)lClause.getClients()).isConform(EiffelClassTypeName.ANY_TEXT)) {
                    lResult.addAll(((EiffelFeatureClause) lClause).getFeatures());
                }
            } else {
                lResult.addAll(((EiffelFeatureClause) lClause).getFeatures());
            }

        }
        return lResult;
    }

    /**
     * The features in the feature list that are exported to a type
     *
     * @param aTypeName The type to check if the feature clause are exported to
     * @return the exported features.
     */
    public @NotNull List<EiffelFeature> getExportedFeatures(String aTypeName) {
        List<EiffelFeature> lResult = new LinkedList<>();
        for (EiffelPsiFeatureClause lClause : getFeatureClauseList()) {
            if (lClause.getClients() != null) {
                if (((EiffelClients)lClause.getClients()).isConform(aTypeName)) {
                    lResult.addAll(((EiffelFeatureClause) lClause).getFeatures());
                }
            } else {
                lResult.addAll(((EiffelFeatureClause) lClause).getFeatures());
            }

        }
        return lResult;
    }
}
