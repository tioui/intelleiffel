package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.models.EiffelFeature;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.List;

/**
 * An object that can be used to obtain features
 */
public abstract class EiffelFeaturable extends ASTWrapperPsiElement {

    /**
     * The constructor of the object
     *
     * @param aNode The node of the element
     */
    public EiffelFeaturable(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * The list of featurable to used in this object
     *
     * @return the list of featurables
     */
    protected abstract List<EiffelFeaturable> getFeaturableList();

    /**
     * Every features in the feature list
     *
     * @return every feature in this.
     */
    public @NotNull List<EiffelFeature> getFeatures() {
        List<EiffelFeature> lResult = new LinkedList<>();
        for (EiffelFeaturable lParent : getFeaturableList()) {
            lResult.addAll(lParent.getFeatures());
        }
        EiffelUtil.getInstance().removeDuplicate(lResult);
        return lResult;
    }

    /**
     * The features in the feature list that are public
     *
     * @return the public features.
     */
    public @NotNull List<EiffelFeature> getExportedFeatures() {
        List<EiffelFeature> lResult = new LinkedList<>();
        for (EiffelFeaturable lParent : getFeaturableList()) {
            lResult.addAll(lParent.getExportedFeatures());
        }
        EiffelUtil.getInstance().removeDuplicate(lResult);
        return lResult;
    }

    /**
     * The features in the feature list that are exported to a type
     *
     * @param aTypeName The type to check if the feature clause are exported to
     * @return the exported features.
     */
    public @NotNull List<EiffelFeature> getExportedFeatures(String aTypeName) {
        List<EiffelFeature> lResult = new LinkedList<>();
        for (EiffelFeaturable lParent : getFeaturableList()) {
            lResult.addAll(lParent.getExportedFeatures(aTypeName));
        }
        EiffelUtil.getInstance().removeDuplicate(lResult);
        return lResult;
    }

}
