package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.psi.EiffelPsiClassNameIdentifier;
import net.libreti.intelleiffel.psi.EiffelTypes;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Mixin for the Grammar rules Class_Name_Identifier
 *
 * @author Louis M
 */
public abstract class EiffelClassNameIdentifier extends ASTWrapperPsiElement implements EiffelPsiClassNameIdentifier {

    /**
     * Initialize the mixin
     * @param aNode The syntaxic node
     */
    public EiffelClassNameIdentifier(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * Return the class name of the current Class name identifier
     * @return The class name
     */
    public @Nullable String getClassName() {
        String lResult = null;
        ASTNode lIdNode = getNode().findChildByType(EiffelTypes.EIF_IDENTIFIER);
        if (lIdNode != null) {
            lResult = lIdNode.getText();
        }
        return lResult;
    }

}
