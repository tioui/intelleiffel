package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.models.EiffelFeature;
import net.libreti.intelleiffel.models.EiffelNamable;
import net.libreti.intelleiffel.psi.EiffelPsiAlias;
import net.libreti.intelleiffel.psi.EiffelPsiFeatureName;
import net.libreti.intelleiffel.psi.EiffelPsiNewExportItem;
import net.libreti.intelleiffel.psi.EiffelPsiParent;
import net.libreti.intelleiffel.psi.EiffelPsiRenamePair;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A parent of a class
 *
 * @author Louis M
 */
public abstract class EiffelParent extends EiffelFeaturable implements EiffelPsiParent, EiffelNamable {

    /**
     * The constructor of the object
     *
     * @param aNode The node of the element
     */
    public EiffelParent(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * The name of the parent type
     *
     * @return the name of the parent
     */
    @Override
    public @NotNull String getName() {
        return ((EiffelClassTypeName)getClassType().getClassTypeName()).getName();
    }

    private void applyRename(List<EiffelFeature> aFeatures) {
        if (getFeatureAdaptation() != null) {
            if (getFeatureAdaptation().getRename() != null) {
                for (EiffelPsiRenamePair lPair :
                        getFeatureAdaptation().getRename().getRenameList().getRenamePairList()) {
                    String lOriginalName = lPair.getFeatureName().getIdentifier().getText();
                    if (lPair.getExtendedFeatureName() != null) {
                        String lRenameName = lPair.getExtendedFeatureName().getIdentifier().getText();
                        for (EiffelFeature lFeature : aFeatures) {
                            if (lFeature.getOriginalName().equals(lOriginalName)) {
                                lFeature.setRename(lRenameName);
                                List<EiffelAlias> lAliases =
                                        new ArrayList<>(lPair.getExtendedFeatureName().getAliasList().size());
                                for (EiffelPsiAlias lAlias : lPair.getExtendedFeatureName().getAliasList()) {
                                    lAliases.add((EiffelAlias) lAlias);
                                }
                                lFeature.setAliasesOverride(lAliases);
                            }
                        }
                    }

                }
            }
        }
    }

    /**
     * The list of featurable to used in this object
     *
     * @return the list of featurables
     */
    @Override
    protected List<EiffelFeaturable> getFeaturableList() {
        return new ArrayList<>();
    }

    /**
     * Every features in the feature list
     *
     * @return every feature in this.
     */
    @Override
    public @NotNull List<EiffelFeature> getFeatures() {
        List<EiffelFeature> lResult;
        EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(
                ((EiffelClassTypeName)getClassType().getClassTypeName()).getName(), getProject());
        if (lClass != null) {
            lResult = lClass.getAllFeatures();
            applyRename(lResult);
        } else {
            lResult = new ArrayList<>();
        }
        return lResult;
    }

    private @NotNull List<EiffelFeature> getConformFeatures(String aTypeName) {
        Set<EiffelFeature> lResult = new HashSet<>();
        List<EiffelFeature> lFeatures = getFeatures();
        if (getFeatureAdaptation() != null) {
            if (getFeatureAdaptation().getNewExports() != null) {
                if (getFeatureAdaptation().getNewExports().getNewExportList() != null) {
                    for (EiffelPsiNewExportItem lItem :
                            getFeatureAdaptation().getNewExports().getNewExportList().getNewExportItemList()) {
                        if (lItem.getClients() instanceof EiffelClients lClients) {
                            if (lClients.isConform(aTypeName)) {
                                if (lItem.getFeatureSet().getAllKeyword() != null) {
                                    lResult.addAll(lFeatures);
                                } else if (lItem.getFeatureSet().getFeatureList() != null) {
                                    for (EiffelPsiFeatureName lFeatureName :
                                            lItem.getFeatureSet().getFeatureList().getFeatureNameList()) {
                                        String lName = lFeatureName.getIdentifier().getText();
                                        for (EiffelFeature lFeature : lFeatures) {
                                            if (lFeature.getName().equals(lName)) {
                                                lResult.add(lFeature);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return new ArrayList<>(lResult);
    }

    /**
     * The features in the feature list that are public
     *
     * @return the public features.
     */
    @Override
    public @NotNull List<EiffelFeature> getExportedFeatures() {
        return getExportedFeatures(EiffelClassTypeName.ANY_TEXT);
    }

    /**
     * The features in the feature list that are exported to a type
     *
     * @param aTypeName The type to check if the feature clause are exported to
     * @return the exported features.
     */
    @Override
    public @NotNull List<EiffelFeature> getExportedFeatures(String aTypeName) {
        List<EiffelFeature> lResult;
        EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(
                ((EiffelClassTypeName)getClassType().getClassTypeName()).getName(), getProject());
        if (lClass != null) {
            lResult = lClass.getExportedFeatures(aTypeName);
            applyRename(lResult);
            lResult.addAll(getConformFeatures(aTypeName));
            EiffelUtil.getInstance().removeDuplicate(lResult);
        } else {
            lResult = new ArrayList<>();
        }
        return lResult;
    }
}
