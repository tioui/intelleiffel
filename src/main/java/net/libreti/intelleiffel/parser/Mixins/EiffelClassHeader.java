package net.libreti.intelleiffel.parser.Mixins;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import net.libreti.intelleiffel.psi.EiffelPsiClassHeader;
import net.libreti.intelleiffel.psi.EiffelPsiHeaderMark;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Mixin for the Grammar rules Class_header
 *
 * @author Louis M
 */
public abstract class EiffelClassHeader extends ASTWrapperPsiElement implements EiffelPsiClassHeader {

    /**
     * Initialize the mixin
     * @param aNode The syntaxic node
     */
    public EiffelClassHeader(@NotNull ASTNode aNode) {
        super(aNode);
    }

    /**
     * Return the class name of the current Class header element
     * @return The class name
     */
    public @Nullable String getClassName() {
        String lResult = null;
        if (getTupleDefinition() != null) {
            lResult = getTupleDefinition().getTupleKeyword().getText();
        } else if (getClassNameDefinition() != null) {
            lResult = getClassNameDefinition().getText();
        }
        return lResult;
    }

    /**
     * Check if the class is deferred.
     *
     * @return True if the class is deferred. False if not.
     */
    public boolean isDeferred() {
        boolean lResult = false;
        EiffelPsiHeaderMark lMark = getHeaderMark();
        if (lMark != null) {
            lResult = lMark.getDeferredKeyword() != null;
        }
        return lResult;
    }

    /**
     * Check if the class is frozen.
     *
     * @return True if the class is frozen. False if not.
     */
    public boolean isFrozen() {
        boolean lResult = false;
        EiffelPsiHeaderMark lMark = getHeaderMark();
        if (lMark != null) {
            lResult = lMark.getFrozenKeyword() != null;
        }
        return lResult;
    }

    /**
     * Check if the class is expanded.
     *
     * @return True if the class is expanded. False if not.
     */
    public boolean isExpanded() {
        boolean lResult = false;
        EiffelPsiHeaderMark lMark = getHeaderMark();
        if (lMark != null) {
            lResult = lMark.getExpandedKeyword() != null;
        }
        return lResult;
    }

}
