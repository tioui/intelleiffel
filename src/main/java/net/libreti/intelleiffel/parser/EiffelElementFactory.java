package net.libreti.intelleiffel.parser;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiFileFactory;
import net.libreti.intelleiffel.EiffelLanguage;
import net.libreti.intelleiffel.psi.EiffelFile;
import com.intellij.psi.tree.IElementType;
import net.libreti.intelleiffel.psi.EiffelTypes;
import org.jetbrains.annotations.Nullable;

/**
 * Factory used tu creates Eiffel Elements and files.
 *
 * @author Louis M
 */
public class EiffelElementFactory {

    /**
     * Singleton instance of the EiffelElementFactory class.
     */
    private static final EiffelElementFactory INSTANCE = new EiffelElementFactory();

    /**
     * Retreive the singleton instance of EiffelElementFactory.
     *
     * @return The EiffelElementFactory instance
     */
    public static EiffelElementFactory getInstance() {
        return INSTANCE;
    }

    /**
     * Used deep search to get the first element of the AST that fit an element type
     *
     * @param aElement The root of the AST to search
     * @param aType The type of the element to search.
     *
     * @return The first element that fit the type.
     */
    private @Nullable PsiElement getFirstElement(PsiElement aElement, IElementType aType) {
        PsiElement lResult = null;
        if (aElement.getNode().getElementType() == aType) {
            lResult = aElement;
        } else {
            PsiElement[] lElements = aElement.getChildren();
            int i = 0;
            while (lResult == null && i < lElements.length) {
                lResult = getFirstElement(lElements[i], aType);
                i = i + 1;
            }
        }
        return lResult;
    }

    /**
     * Create a new usable PsiElement of a certain type from aCode.
     *
     * @param aProject The Projet used to create the element.
     * @param aCode The code that represent the element.
     * @param aType The type of the element to return.
     *
     * @return The PsiElement or null if aCode does not generate a PsiElement of aType
     */
    public @Nullable PsiElement createEiffelElement(Project aProject, String aCode, IElementType aType) {
        EiffelFile lFile = createEiffelFile(aProject, aCode);
        PsiElement lResult = null;
        if (lFile != null) {
            PsiFile[] lRoots = lFile.getPsiRoots();
            int i = 0;
            while (lResult == null && i < lRoots.length) {
                lResult = getFirstElement(lRoots[i], aType);
                i = i + 1;
            }
        }
        return lResult;
    }

    /**
     * Create a new Eiffel file.
     *
     * @param aProject The project that will contain the new file
     * @param aCode The code to put in the new Eiffel file.
     * @return The new Eiffel File
     */
    public @Nullable EiffelFile createEiffelFile(Project aProject, String aCode) {
        EiffelFile lResult = null;
        PsiFileFactory lFactory = PsiFileFactory.getInstance(aProject);
        PsiFile lDummyFile = lFactory.createFileFromText(EiffelLanguage.getInstance(), aCode);
        if (lDummyFile instanceof EiffelFile) {
            lResult = (EiffelFile) lDummyFile;
        }
        return lResult;
    }

    /**
     * Return a new detachable keyword element
     *
     * @param aProject The Projet used to create the element.
     * @return the detachable keyword element
     */
    public @Nullable PsiElement createDetachableAttachmentKeyword(Project aProject) {
        return createEiffelElement(aProject, "class DUMMY feature name:detachable CLASSNAME end",
                EiffelTypes.EIF_ATTACHMENT_KEYWORD);
    }

    /**
     * Return a new attached keyword element
     *
     * @param aProject The Projet used to create the element.
     * @return the detachable keyword element
     */
    public @Nullable PsiElement createAttachedAttachmentKeyword(Project aProject) {
        return createEiffelElement(aProject, "class DUMMY feature name:attached CLASSNAME end",
                EiffelTypes.EIF_ATTACHMENT_KEYWORD);
    }

    /**
     * Return a new attached EiffelType element
     *
     * @param aProject The Projet used to create the element.
     * @return the detachable keyword element
     */
    public @Nullable PsiElement createType(Project aProject, String aCode) {
        return createEiffelElement(aProject, "class DUMMY feature name:" + aCode + " end",
                EiffelTypes.EIF_TYPE);
    }

}
