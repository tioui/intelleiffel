package net.libreti.intelleiffel.psi;

import com.intellij.psi.tree.IElementType;
import net.libreti.intelleiffel.EiffelLanguage;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

/**
 * The Eiffel language type of single token
 *
 * @author Louis M
 */
public class EiffelTokenType extends IElementType {

    /**
     * Class creator
     *
     * @param aDebugName A debugging text representing the token.
     */
    public EiffelTokenType(@NonNls @NotNull String aDebugName) {
        super(aDebugName, EiffelLanguage.getInstance());
    }

    /**
     * A text representation of the token type.
     *
     * @return text representation of the token type.
     */
    @Override
    public String toString() {
        String lResult;
        if (this == EiffelTypes.EIF_ACROSS_KEYWORD) {
            lResult = "'across'";
        } else if (this == EiffelTypes.EIF_AGENT_KEYWORD) {
            lResult = "'agent'";
        } else if (this == EiffelTypes.EIF_ALIAS_KEYWORD) {
            lResult = "'alias'";
        } else if (this == EiffelTypes.EIF_ALL_KEYWORD) {
            lResult = "'all'";
        } else if (this == EiffelTypes.EIF_AND_KEYWORD) {
            lResult = "'and'";
        } else if (this == EiffelTypes.EIF_ARROW_SEPARATOR) {
            lResult = "'->'";
        } else if (this == EiffelTypes.EIF_ASSIGN) {
            lResult = "':='";
        } else if (this == EiffelTypes.EIF_FOR_ALL) {
            lResult = "':='";
        } else if (this == EiffelTypes.EIF_EXISTS) {
            lResult = "':='";
        } else if (this == EiffelTypes.EIF_CLOCKWISE_CIRCLE) {
            lResult = "':='";
        } else if (this == EiffelTypes.EIF_COUNTER_CLOCKWISE_CIRCLE) {
            lResult = "':='";
        } else if (this == EiffelTypes.EIF_BROKEN_BAR) {
            lResult = "':='";
        } else if (this == EiffelTypes.EIF_ASSIGN_KEYWORD) {
            lResult = "'assign'";
        } else if (this == EiffelTypes.EIF_AS_KEYWORD) {
            lResult = "'as'";
        } else if (this == EiffelTypes.EIF_ATTRIBUTE_KEYWORD) {
            lResult = "'attribute'";
        } else if (this == EiffelTypes.EIF_CHARACTER) {
            lResult = "CHARACTER";
        } else if (this == EiffelTypes.EIF_CHECK_KEYWORD) {
            lResult = "'check'";
        } else if (this == EiffelTypes.EIF_CLASS_KEYWORD) {
            lResult = "'class'";
        } else if (this == EiffelTypes.EIF_COLON_SEPARATOR) {
            lResult = "':'";
        } else if (this == EiffelTypes.EIF_COMMA_SEPARATOR) {
            lResult = "','";
        } else if (this == EiffelTypes.EIF_CONVERT_KEYWORD) {
            lResult = "'convert'";
        } else if (this == EiffelTypes.EIF_CREATE_KEYWORD) {
            lResult = "'create'";
        } else if (this == EiffelTypes.EIF_CURRENT_KEYWORD) {
            lResult = "'Current'";
        } else if (this == EiffelTypes.EIF_DEBUG_KEYWORD) {
            lResult = "'debug'";
        } else if (this == EiffelTypes.EIF_DEFERRED_KEYWORD) {
            lResult = "'deferred'";
        } else if (this == EiffelTypes.EIF_DIVISION_OPERATOR) {
            lResult = "'//'";
        } else if (this == EiffelTypes.EIF_DOLLAR) {
            lResult = "'$'";
        } else if (this == EiffelTypes.EIF_DOT_DOT_SEPARATOR) {
            lResult = "'..'";
        } else if (this == EiffelTypes.EIF_DOT_SEPARATOR) {
            lResult = "'.'";
        } else if (this == EiffelTypes.EIF_DO_KEYWORD) {
            lResult = "'do'";
        } else if (this == EiffelTypes.EIF_ELSEIF_KEYWORD) {
            lResult = "'elseif'";
        } else if (this == EiffelTypes.EIF_ELSE_KEYWORD) {
            lResult = "'else'";
        } else if (this == EiffelTypes.EIF_EMPTY_BRACKETS) {
            lResult = "'[]]'";
        } else if (this == EiffelTypes.EIF_END_KEYWORD) {
            lResult = "'end'";
        } else if (this == EiffelTypes.EIF_ENSURE_KEYWORD) {
            lResult = "'ensure'";
        } else if (this == EiffelTypes.EIF_EQUAL_OPERATOR) {
            lResult = "'='";
        } else if (this == EiffelTypes.EIF_EQUIVALENT_OPERATOR) {
            lResult = "'~'";
        } else if (this == EiffelTypes.EIF_EXCLAMATION) {
            lResult = "'!'";
        } else if (this == EiffelTypes.EIF_EXPANDED_KEYWORD) {
            lResult = "'expanded'";
        } else if (this == EiffelTypes.EIF_EXPORT_KEYWORD) {
            lResult = "'export'";
        } else if (this == EiffelTypes.EIF_EXTERNAL_KEYWORD) {
            lResult = "'external'";
        } else if (this == EiffelTypes.EIF_FALSE_KEYWORD) {
            lResult = "'False'";
        } else if (this == EiffelTypes.EIF_FEATURE_KEYWORD) {
            lResult = "'feature'";
        } else if (this == EiffelTypes.EIF_FREE_OPERATOR) {
            lResult = "'Free operator'";
        } else if (this == EiffelTypes.EIF_FROM_KEYWORD) {
            lResult = "'from'";
        } else if (this == EiffelTypes.EIF_FROZEN_KEYWORD) {
            lResult = "'frozen'";
        } else if (this == EiffelTypes.EIF_GREATER_EQUAL_OPERATOR) {
            lResult = "'>='";
        } else if (this == EiffelTypes.EIF_GREATER_OPERATOR) {
            lResult = "'>'";
        } else if (this == EiffelTypes.EIF_HAT_OPERATOR) {
            lResult = "'^'";
        } else if (this == EiffelTypes.EIF_IDENTIFIER) {
            lResult = "'Identifier'";
        } else if (this == EiffelTypes.EIF_IF_KEYWORD) {
            lResult = "'if'";
        } else if (this == EiffelTypes.EIF_IMPLIES_KEYWORD) {
            lResult = "'implies'";
        } else if (this == EiffelTypes.EIF_INHERIT_KEYWORD) {
            lResult = "'inherit'";
        } else if (this == EiffelTypes.EIF_INSPECT_KEYWORD) {
            lResult = "'inspect'";
        } else if (this == EiffelTypes.EIF_INTERROGATION) {
            lResult = "'?'";
        } else if (this == EiffelTypes.EIF_INVARIANT_KEYWORD) {
            lResult = "'invariant'";
        } else if (this == EiffelTypes.EIF_LEFT_ARRAY) {
            lResult = "'<<'";
        } else if (this == EiffelTypes.EIF_LEFT_BRACE) {
            lResult = "'{'";
        } else if (this == EiffelTypes.EIF_LEFT_BRACKET) {
            lResult = "'['";
        } else if (this == EiffelTypes.EIF_LEFT_PARENTHESE) {
            lResult = "'('";
        } else if (this == EiffelTypes.EIF_LIKE_KEYWORD) {
            lResult = "'like'";
        } else if (this == EiffelTypes.EIF_LOCAL_KEYWORD) {
            lResult = "'local'";
        } else if (this == EiffelTypes.EIF_LOOP_KEYWORD) {
            lResult = "'loop'";
        } else if (this == EiffelTypes.EIF_LOWER_EQUAL_OPERATOR) {
            lResult = "'<=>'";
        } else if (this == EiffelTypes.EIF_LOWER_OPERATOR) {
            lResult = "'<'";
        } else if (this == EiffelTypes.EIF_LEFT_SHIFT_OPERATOR) {
            lResult = "'|<<'";
        } else if (this == EiffelTypes.EIF_RIGHT_SHIFT_OPERATOR) {
            lResult = "'|>>'";
        } else if (this == EiffelTypes.EIF_MINUS_OPERATOR) {
            lResult = "'-'";
        } else if (this == EiffelTypes.EIF_MODULO_OPERATOR) {
            lResult = "'\\\\'";
        } else if (this == EiffelTypes.EIF_NONE_KEYWORD) {
            lResult = "'NONE'";
        } else if (this == EiffelTypes.EIF_NOTE_KEYWORD) {
            lResult = "'note'";
        } else if (this == EiffelTypes.EIF_NOT_EQUAL_OPERATOR) {
            lResult = "'/='";
        } else if (this == EiffelTypes.EIF_NOT_EQUIVALENT_OPERATOR) {
            lResult = "'/~'";
        } else if (this == EiffelTypes.EIF_NOT_KEYWORD) {
            lResult = "'not'";
        } else if (this == EiffelTypes.EIF_NUMBER) {
            lResult = "NUMERIC";
        } else if (this == EiffelTypes.EIF_OBSOLETE_KEYWORD) {
            lResult = "'obsolete'";
        } else if (this == EiffelTypes.EIF_OLD_KEYWORD) {
            lResult = "'old'";
        } else if (this == EiffelTypes.EIF_ONCE_KEYWORD) {
            lResult = "'once'";
        } else if (this == EiffelTypes.EIF_ONLY_KEYWORD) {
            lResult = "'only'";
        } else if (this == EiffelTypes.EIF_OR_KEYWORD) {
            lResult = "'or'";
        } else if (this == EiffelTypes.EIF_PLUS_OPERATOR) {
            lResult = "'+'";
        } else if (this == EiffelTypes.EIF_PRECURSOR_KEYWORD) {
            lResult = "'Precursor'";
        } else if (this == EiffelTypes.EIF_QUOTE) {
            lResult = "'\"''";
        } else if (this == EiffelTypes.EIF_REAL) {
            lResult = "'REAL'";
        } else if (this == EiffelTypes.EIF_REDEFINE_KEYWORD) {
            lResult = "'redefine'";
        } else if (this == EiffelTypes.EIF_RENAME_KEYWORD) {
            lResult = "'rename'";
        } else if (this == EiffelTypes.EIF_REQUIRE_KEYWORD) {
            lResult = "'require'";
        } else if (this == EiffelTypes.EIF_RESCUE_KEYWORD) {
            lResult = "'rescue'";
        } else if (this == EiffelTypes.EIF_RESULT_KEYWORD) {
            lResult = "'Result'";
        } else if (this == EiffelTypes.EIF_RETRY_KEYWORD) {
            lResult = "'retry'";
        } else if (this == EiffelTypes.EIF_RIGHT_ARRAY) {
            lResult = "'>>'";
        } else if (this == EiffelTypes.EIF_RIGHT_BRACE) {
            lResult = "'}'";
        } else if (this == EiffelTypes.EIF_RIGHT_BRACKET) {
            lResult = "']'";
        } else if (this == EiffelTypes.EIF_RIGHT_PARENTHESE) {
            lResult = "')'";
        } else if (this == EiffelTypes.EIF_SELECT_KEYWORD) {
            lResult = "'select'";
        } else if (this == EiffelTypes.EIF_SEMI_COLON_SEPARATOR) {
            lResult = "';'";
        } else if (this == EiffelTypes.EIF_SINGLE_QUOTE) {
            lResult = "''";
        } else if (this == EiffelTypes.EIF_SLASH_OPERATOR) {
            lResult = "'/'";
        } else if (this == EiffelTypes.EIF_AND_OPERATOR) {
            lResult = "'&'";
        } else if (this == EiffelTypes.EIF_OR_OPERATOR) {
            lResult = "'|'";
        } else if (this == EiffelTypes.EIF_SOME_KEYWORD) {
            lResult = "'some'";
        } else if (this == EiffelTypes.EIF_STAR_OPERATOR) {
            lResult = "'*'";
        } else if (this == EiffelTypes.EIF_STRING) {
            lResult = "STRING";
        } else if (this == EiffelTypes.EIF_THEN_KEYWORD) {
            lResult = "'then'";
        } else if (this == EiffelTypes.EIF_TRUE_KEYWORD) {
            lResult = "'True'";
        } else if (this == EiffelTypes.EIF_TUPLE_KEYWORD) {
            lResult = "'TUPLE'";
        } else if (this == EiffelTypes.EIF_UNDEFINE_KEYWORD) {
            lResult = "'undefine'";
        } else if (this == EiffelTypes.EIF_UNTIL_KEYWORD) {
            lResult = "'until'";
        } else if (this == EiffelTypes.EIF_VARIANT_KEYWORD) {
            lResult = "'variant'";
        } else if (this == EiffelTypes.EIF_WHEN_KEYWORD) {
            lResult = "'when'";
        } else if (this == EiffelTypes.EIF_XOR_KEYWORD) {
            lResult = "'xor'";
        } else if (this == EiffelTypes.EIF_LINE_COMMENT) {
            lResult = "'<comment>'";
        } else {
            lResult = super.toString();
        }
        return lResult;
    }
}
