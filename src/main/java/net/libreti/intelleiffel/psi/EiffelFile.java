package net.libreti.intelleiffel.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.psi.FileViewProvider;
import com.intellij.openapi.fileTypes.FileType;
import org.jetbrains.annotations.NotNull;
import net.libreti.intelleiffel.EiffelLanguage;
import net.libreti.intelleiffel.EiffelFileType;

/**
 * Represent an Eiffel source code file (.e file)
 *
 * @author Louis M
 */

public class EiffelFile extends PsiFileBase {

    /**
     * Class creator.
     *
     * @param aViewProvider Manages Eiffel-specific access to PSI.
     */
    public EiffelFile(@NotNull FileViewProvider aViewProvider) {
        super(aViewProvider, EiffelLanguage.getInstance());
    }

    /**
     * The File type of Eiffel type.
     *
     * @return An LanguageFileType of an Eiffel file.
     */
    @NotNull
    @Override
    public FileType getFileType() {
        return EiffelFileType.getInstance();
    }

    /**
     * Text representation of the EiffelFile.
     *
     * @return A text representation of the file.
     */
    @Override
    public String toString() {
        return "Eiffel File";
    }
}
