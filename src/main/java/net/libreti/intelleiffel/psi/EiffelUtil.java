package net.libreti.intelleiffel.psi;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import net.libreti.intelleiffel.EiffelFileType;
import net.libreti.intelleiffel.models.EiffelAddressable;
import net.libreti.intelleiffel.models.EiffelCurrentAddressable;
import net.libreti.intelleiffel.models.EiffelEntityDeclaration;
import net.libreti.intelleiffel.models.EiffelFeature;
import net.libreti.intelleiffel.models.EiffelNamable;
import net.libreti.intelleiffel.models.EiffelResultAddressable;
import net.libreti.intelleiffel.parser.EiffelElementFactory;
import net.libreti.intelleiffel.parser.Mixins.EiffelClass;
import net.libreti.intelleiffel.parser.Mixins.EiffelDeclarationIdentifier;
import net.libreti.intelleiffel.parser.Mixins.EiffelType;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.util.LinkedList;
import java.util.List;

/**
 * Utility class for the Eiffel Psi tree
 *
 * @author Louis M
 */
public class EiffelUtil {

    /**
     * Singleton instance of the EiffelPsiHelper class.
     */
    private static final EiffelUtil INSTANCE = new EiffelUtil();

    /**
     * Retreive the singleton instance of EiffelPsiHelper.
     *
     * @return The EiffelPsiHelper instance
     */
    public static EiffelUtil getInstance() {
        return INSTANCE;
    }

    /**
     * Replace a class name alias by the true class name used internally in Eiffel
     * @param aClassName The class name (possibly alias)
     * @return The class name that is not an alias
     */
    private String replaceClassNameAlias(String aClassName) {
        String lResult = aClassName;
        if (aClassName.equalsIgnoreCase("STRING")) {
            lResult = "STRING_8";
        } else if (aClassName.equalsIgnoreCase("IMMUTABLE_STRING")) {
            lResult = "IMMUTABLE_STRING_8";
        } else if (aClassName.equalsIgnoreCase("CHARACTER")) {
            lResult = "CHARACTER_8";
        } else if (aClassName.equalsIgnoreCase("INTEGER")) {
            lResult = "INTEGER_32";
        } else if (aClassName.equalsIgnoreCase("NATURAL")) {
            lResult = "NATURAL_32";
        } else if (aClassName.equalsIgnoreCase("REAL")) {
            lResult = "REAL_32";
        } else if (aClassName.equalsIgnoreCase("DOUBLE")) {
            lResult = "REAL_64";
        }
        return lResult;
    }

    /**
     * Find the EiffelPsiEiffelFile in every file of the project.
     *
     * @param aClassName The name of the Eiffel class to search
     * @param aProject The IntelliJ project
     * @return The EiffelPsiEiffelFile that has been found in the project. Null if the class was not found.
     */
    public @Nullable EiffelClass findEiffelClass(String aClassName, Project aProject) {
        EiffelClass lResult = null;
        String lClassName = replaceClassNameAlias(aClassName);
        List<EiffelClass> lClasses = getEiffelClasses(aProject);
        for (EiffelClass lClass : lClasses) {
            if (lClass != null && lClass.getName() != null &&
                    replaceClassNameAlias(lClass.getName()).equalsIgnoreCase(lClassName)) {
                lResult = lClass;
            }
        }
        return lResult;
    }

    /**
     * Extract the EiffelPsiEiffelFile from an Eiffel file
     *
     * @param aFile The Eiffel file to extract the EiffelPsiEiffelFile from.
     * @return The EiffelPsiEiffelFile or null on error.
     */
    public @Nullable EiffelClass findClassInFile(EiffelFile aFile) {
        EiffelClass lResult = null;
        PsiFile[] lRoots = aFile.getPsiRoots();
        for (PsiFile lRoot : lRoots) {
            for (PsiElement laElement : lRoot.getChildren()) {
                if (laElement instanceof EiffelClass lClass) {
                    lResult = lClass;
                }
            }
        }
        return lResult;
    }

    /**
     * Extract every EiffelPsiEiffelFile in the project
     *
     * @param aProject The IntelliJ project
     * @return Every EiffelFiles
     */
    public @NotNull List<EiffelClass> getEiffelClasses(Project aProject) {
        List<EiffelClass> lResult = new LinkedList<EiffelClass>();
        for (VirtualFile lFile :
                FileTypeIndex.getFiles(EiffelFileType.getInstance(), GlobalSearchScope.allScope(aProject))) {
            if (PsiManager.getInstance(aProject).findFile(lFile) instanceof EiffelFile lEiffelFile) {
                lResult.add(findClassInFile(lEiffelFile));
            }
        }
        return lResult;
    }

    /**
     * Remove duplicated in a list of namable object
     * @param lList the list containing the namable
     */
    public void removeDuplicate(List<? extends EiffelNamable> lList) {
        for (int i = 0; i < lList.size() - 1; i = i + 1) {
            for (int j = i + 1; j < lList.size(); j = j + 1) {
                if (lList.get(i).getName().equals(lList.get(j).getName())) {
                    lList.remove(j);
                }
            }
        }
    }

    /**
     * Retrieve the inside of an Eiffel manifest STRING (without the marks)
     *
     * @param aString An Eiffel Manifest String
     * @return The message inside the string
     */
    public String getStringValue (String aString) {
        String lResult = aString.trim();
        if (lResult.charAt(0) == '"' && lResult.charAt(lResult.length() - 1) == '"') {
            boolean lMultiline = false;
            if (lResult.charAt(1) == '[' && lResult.charAt(lResult.length() - 2) == ']') {
                int lStart = 2;
                int lEnd = lResult.length() - 3;
                lMultiline = true;
                while (lMultiline && (lResult.charAt(lStart) != '\n')) {
                    if (lResult.charAt(lStart) != ' ' && lResult.charAt(lStart) == '\t') {
                        lMultiline = false;
                    }
                    lStart = lStart + 1;
                }
                while (lMultiline && (lResult.charAt(lEnd) != '\n')) {
                    if (lResult.charAt(lEnd) != ' ' && lResult.charAt(lEnd) == '\t') {
                        lMultiline = false;
                    }
                    lEnd = lEnd - 1;
                }
                if (lMultiline) {
                    lResult = lResult.substring(lStart, lEnd);
                }
            }
            if (!lMultiline) {
                lResult = lResult.substring(1, lResult.length() - 1);
            }
        }
        return lResult;
    }

    /**
     * Adding to an addressable list every identifier declared in an entity declaration list
     * @param aDeclarations The entity declaration list
     * @param aAddressables The list of addressable to add the declarations
     */
    private void addEntityDeclarationsToAddressables(EiffelPsiEntityDeclarationList aDeclarations,
                                                     List<EiffelAddressable> aAddressables) {
        for (EiffelPsiEntityDeclarationGroup lGroup : aDeclarations.getEntityDeclarationGroupList()) {
            EiffelType lType = (EiffelType)lGroup.getTypeMark().getType();
            for (EiffelPsiDeclarationIdentifier lIdentifier :
                    lGroup.getIdentifierList().getDeclarationIdentifierList()) {
                aAddressables.add(
                        new EiffelEntityDeclaration((EiffelDeclarationIdentifier) lIdentifier, lType));
            }
        }
    }

    /**
     * Retrieve every addressable elements accessible from a certain element in an EiffelCode
     *
     * @param aElement The element at the position that should get addressable elements
     * @return Every addressable elements
     */
    public List<EiffelAddressable> getAddressablesFromElement(PsiElement aElement) {
        List<EiffelAddressable> lAddressables = new LinkedList<>();
        PsiElement lCurrentElement = aElement;
        boolean lClassRoutine = false;
        while (lCurrentElement != null && !(lCurrentElement instanceof PsiFile)) {
            if (lCurrentElement instanceof EiffelPsiAttributeOrRoutine lAttributeOrRoutine) {
                if (lAttributeOrRoutine.getLocalDeclarations() != null &&
                        lAttributeOrRoutine.getLocalDeclarations().getEntityDeclarationList() != null) {
                    addEntityDeclarationsToAddressables(
                            lAttributeOrRoutine.getLocalDeclarations().getEntityDeclarationList(), lAddressables);
                }
                if (lAttributeOrRoutine.getPostcondition() != null &&
                        lAttributeOrRoutine.getPostcondition().getAssertion() != null) {
                    for (EiffelPsiAssertionClause lAssertion :
                            lAttributeOrRoutine.getPostcondition().getAssertion().getAssertionClauseList()) {
                        lClassRoutine = lAssertion.getUnlabeledAssertionClause() != null &&
                                lAssertion.getUnlabeledAssertionClause().getClassKeyword() != null;
                    }
                }
            } else if (lCurrentElement instanceof EiffelPsiDeclarationBodyValue lValue) {
                if (lValue.getFormalArguments() != null &&
                        lValue.getFormalArguments().getEntityDeclarationList() != null) {
                    addEntityDeclarationsToAddressables(
                            lValue.getFormalArguments().getEntityDeclarationList(), lAddressables);
                }
                if (lValue.getQueryMark() != null) {
                    lAddressables.add(new EiffelResultAddressable(
                            (EiffelType) lValue.getQueryMark().getTypeMark().getType()));
                }
            } else if (lCurrentElement instanceof EiffelClass lClass) {
                List<EiffelFeature> lFeatures = lClass.getAllFeatures();
                if (!lClassRoutine) {
                    lAddressables.addAll(lFeatures);
                    PsiElement lCurrentType =
                            EiffelElementFactory.getInstance().createType(aElement.getProject(), lClass.getName());
                    if (lCurrentType instanceof EiffelType lType) {
                        lAddressables.add(new EiffelCurrentAddressable(lType));
                    }
                } else {
                    for (EiffelFeature lFeature : lFeatures) {
                        if (lFeature.isConstant() || lFeature.isInstanceFreeRoutine()) {
                            lAddressables.add(lFeature);
                        }
                    }
                }
            } // Todo: Adding AS_KEYWORD when the Expression type has been programmed
            lCurrentElement = lCurrentElement.getParent();
        }

        return lAddressables;
    }

}
