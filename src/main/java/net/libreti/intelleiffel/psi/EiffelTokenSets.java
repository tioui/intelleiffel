package net.libreti.intelleiffel.psi;

import com.intellij.psi.tree.TokenSet;

/**
 * Contain every token set for the Eiffel language.
 *
 * @author Louis M
 */
public interface EiffelTokenSets {

    /**
     * Identifier and literal constants
     */
    TokenSet EMPTY_WORD = TokenSet.create(EiffelTypes.EIF_EMPTY_WORD);
    TokenSet CHARACTER = TokenSet.create(EiffelTypes.EIF_CHARACTER);
    TokenSet IDENTIFIER = TokenSet.create(EiffelTypes.EIF_IDENTIFIER);
    TokenSet STRING = TokenSet.create(EiffelTypes.EIF_STRING);
    TokenSet NUMBER = TokenSet.create(EiffelTypes.EIF_NUMBER);
    TokenSet REAL = TokenSet.create(EiffelTypes.EIF_REAL);
    TokenSet LINE_COMMENT = TokenSet.create(EiffelTypes.EIF_LINE_COMMENT);

    /**
     * Unary and Binary operators
     */
    TokenSet PLUS_OPERATOR = TokenSet.create(EiffelTypes.EIF_PLUS_OPERATOR);
    TokenSet MINUS_OPERATOR = TokenSet.create(EiffelTypes.EIF_MINUS_OPERATOR);
    TokenSet STAR_OPERATOR = TokenSet.create(EiffelTypes.EIF_STAR_OPERATOR);
    TokenSet SLASH_OPERATOR = TokenSet.create(EiffelTypes.EIF_SLASH_OPERATOR);
    TokenSet AND_OPERATOR = TokenSet.create(EiffelTypes.EIF_AND_OPERATOR);
    TokenSet OR_OPERATOR = TokenSet.create(EiffelTypes.EIF_OR_OPERATOR);
    TokenSet HAT_OPERATOR = TokenSet.create(EiffelTypes.EIF_HAT_OPERATOR);
    TokenSet LOWER_OPERATOR = TokenSet.create(EiffelTypes.EIF_LOWER_OPERATOR);
    TokenSet GREATER_OPERATOR = TokenSet.create(EiffelTypes.EIF_GREATER_OPERATOR);
    TokenSet LEFT_SHIFT_OPERATOR = TokenSet.create(EiffelTypes.EIF_LEFT_SHIFT_OPERATOR);
    TokenSet RIGHT_SHIFT_OPERATOR = TokenSet.create(EiffelTypes.EIF_RIGHT_SHIFT_OPERATOR);
    TokenSet DIVISION_OPERATOR = TokenSet.create(EiffelTypes.EIF_DIVISION_OPERATOR);
    TokenSet MODULO_OPERATOR = TokenSet.create(EiffelTypes.EIF_MODULO_OPERATOR);
    TokenSet EQUAL_OPERATOR = TokenSet.create(EiffelTypes.EIF_EQUAL_OPERATOR);
    TokenSet NOT_EQUAL_OPERATOR = TokenSet.create(EiffelTypes.EIF_NOT_EQUAL_OPERATOR);
    TokenSet GREATER_EQUAL_OPERATOR = TokenSet.create(EiffelTypes.EIF_GREATER_EQUAL_OPERATOR);
    TokenSet LOWER_EQUAL_OPERATOR = TokenSet.create(EiffelTypes.EIF_LOWER_EQUAL_OPERATOR);
    TokenSet EQUIVALENT_OPERATOR = TokenSet.create(EiffelTypes.EIF_EQUIVALENT_OPERATOR);
    TokenSet NOT_EQUIVALENT_OPERATOR = TokenSet.create(EiffelTypes.EIF_NOT_EQUIVALENT_OPERATOR);
    TokenSet FREE_OPERATOR = TokenSet.create(EiffelTypes.EIF_FREE_OPERATOR);
    TokenSet DIFFERENCE_OPERATOR = TokenSet.create(EiffelTypes.EIF_DIFFERENCE_OPERATOR);

    /**
     * Separators
     */
    TokenSet DOT_SEPARATOR = TokenSet.create(EiffelTypes.EIF_DOT_SEPARATOR);
    TokenSet SEMI_COLON_SEPARATOR = TokenSet.create(EiffelTypes.EIF_SEMI_COLON_SEPARATOR);
    TokenSet COMMA_SEPARATOR = TokenSet.create(EiffelTypes.EIF_COMMA_SEPARATOR);
    TokenSet COLON_SEPARATOR = TokenSet.create(EiffelTypes.EIF_COLON_SEPARATOR);
    TokenSet ARROW_SEPARATOR = TokenSet.create(EiffelTypes.EIF_ARROW_SEPARATOR);
    TokenSet DOT_DOT_SEPARATOR = TokenSet.create(EiffelTypes.EIF_DOT_DOT_SEPARATOR);

    /**
     * Prefixes
     */
    TokenSet EXCLAMATION = TokenSet.create(EiffelTypes.EIF_EXCLAMATION);
    TokenSet INTERROGATION = TokenSet.create(EiffelTypes.EIF_INTERROGATION);
    TokenSet DOLLAR = TokenSet.create(EiffelTypes.EIF_DOLLAR);
    TokenSet ASSIGN = TokenSet.create(EiffelTypes.EIF_ASSIGN);
    TokenSet FOR_ALL = TokenSet.create(EiffelTypes.EIF_FOR_ALL);
    TokenSet EXISTS = TokenSet.create(EiffelTypes.EIF_EXISTS);
    TokenSet CLOCKWISE_CIRCLE = TokenSet.create(EiffelTypes.EIF_CLOCKWISE_CIRCLE);
    TokenSet COUNTER_CLOCKWISE_CIRCLE = TokenSet.create(EiffelTypes.EIF_COUNTER_CLOCKWISE_CIRCLE);

    /**
     * Brackets
     */
    TokenSet BROKEN_BAR = TokenSet.create(EiffelTypes.EIF_BROKEN_BAR);
    TokenSet LEFT_ARRAY = TokenSet.create(EiffelTypes.EIF_LEFT_ARRAY);
    TokenSet RIGHT_ARRAY = TokenSet.create(EiffelTypes.EIF_RIGHT_ARRAY);
    TokenSet LEFT_PARENTHESE = TokenSet.create(EiffelTypes.EIF_LEFT_PARENTHESE);
    TokenSet RIGHT_PARENTHESE = TokenSet.create(EiffelTypes.EIF_RIGHT_PARENTHESE);
    TokenSet LEFT_BRACE = TokenSet.create(EiffelTypes.EIF_LEFT_BRACE);
    TokenSet RIGHT_BRACE = TokenSet.create(EiffelTypes.EIF_RIGHT_BRACE);
    TokenSet LEFT_BRACKET = TokenSet.create(EiffelTypes.EIF_LEFT_BRACKET);
    TokenSet RIGHT_BRACKET = TokenSet.create(EiffelTypes.EIF_RIGHT_BRACKET);
    TokenSet EMPTY_BRACKETS = TokenSet.create(EiffelTypes.EIF_EMPTY_BRACKETS);

    /**
     * Quotes
     */
    TokenSet SINGLE_QUOTE = TokenSet.create(EiffelTypes.EIF_SINGLE_QUOTE);
    TokenSet QUOTE = TokenSet.create(EiffelTypes.EIF_QUOTE);

    /**
     * Keywords
     */
    TokenSet ACROSS_KEYWORD = TokenSet.create(EiffelTypes.EIF_ACROSS_KEYWORD);
    TokenSet AGENT_KEYWORD = TokenSet.create(EiffelTypes.EIF_AGENT_KEYWORD);
    TokenSet ALIAS_KEYWORD = TokenSet.create(EiffelTypes.EIF_ALIAS_KEYWORD);
    TokenSet ALL_KEYWORD = TokenSet.create(EiffelTypes.EIF_ALL_KEYWORD);
    TokenSet AND_KEYWORD = TokenSet.create(EiffelTypes.EIF_AND_KEYWORD);
    TokenSet AS_KEYWORD = TokenSet.create(EiffelTypes.EIF_AS_KEYWORD);
    TokenSet ATTRIBUTE_KEYWORD = TokenSet.create(EiffelTypes.EIF_ATTRIBUTE_KEYWORD);
    TokenSet CHECK_KEYWORD = TokenSet.create(EiffelTypes.EIF_CHECK_KEYWORD);
    TokenSet CLASS_KEYWORD = TokenSet.create(EiffelTypes.EIF_CLASS_KEYWORD);
    TokenSet CONVERT_KEYWORD = TokenSet.create(EiffelTypes.EIF_CONVERT_KEYWORD);
    TokenSet CREATE_KEYWORD = TokenSet.create(EiffelTypes.EIF_CREATE_KEYWORD);
    TokenSet CURRENT_KEYWORD = TokenSet.create(EiffelTypes.EIF_CURRENT_KEYWORD);
    TokenSet DEBUG_KEYWORD = TokenSet.create(EiffelTypes.EIF_DEBUG_KEYWORD);
    TokenSet DEFERRED_KEYWORD = TokenSet.create(EiffelTypes.EIF_DEFERRED_KEYWORD);
    TokenSet DO_KEYWORD = TokenSet.create(EiffelTypes.EIF_DO_KEYWORD);
    TokenSet ELSE_KEYWORD = TokenSet.create(EiffelTypes.EIF_ELSE_KEYWORD);
    TokenSet ELSEIF_KEYWORD = TokenSet.create(EiffelTypes.EIF_ELSEIF_KEYWORD);
    TokenSet END_KEYWORD = TokenSet.create(EiffelTypes.EIF_END_KEYWORD);
    TokenSet ENSURE_KEYWORD = TokenSet.create(EiffelTypes.EIF_ENSURE_KEYWORD);
    TokenSet EXPANDED_KEYWORD = TokenSet.create(EiffelTypes.EIF_EXPANDED_KEYWORD);
    TokenSet EXPORT_KEYWORD = TokenSet.create(EiffelTypes.EIF_EXPORT_KEYWORD);
    TokenSet EXTERNAL_KEYWORD = TokenSet.create(EiffelTypes.EIF_EXTERNAL_KEYWORD);
    TokenSet FALSE_KEYWORD = TokenSet.create(EiffelTypes.EIF_FALSE_KEYWORD);
    TokenSet FEATURE_KEYWORD = TokenSet.create(EiffelTypes.EIF_FEATURE_KEYWORD);
    TokenSet FROM_KEYWORD = TokenSet.create(EiffelTypes.EIF_FROM_KEYWORD);
    TokenSet FROZEN_KEYWORD = TokenSet.create(EiffelTypes.EIF_FROZEN_KEYWORD);
    TokenSet IF_KEYWORD = TokenSet.create(EiffelTypes.EIF_IF_KEYWORD);
    TokenSet IMPLIES_KEYWORD = TokenSet.create(EiffelTypes.EIF_IMPLIES_KEYWORD);
    TokenSet INHERIT_KEYWORD = TokenSet.create(EiffelTypes.EIF_INHERIT_KEYWORD);
    TokenSet INSPECT_KEYWORD = TokenSet.create(EiffelTypes.EIF_INSPECT_KEYWORD);
    TokenSet INVARIANT_KEYWORD = TokenSet.create(EiffelTypes.EIF_INVARIANT_KEYWORD);
    TokenSet LIKE_KEYWORD = TokenSet.create(EiffelTypes.EIF_LIKE_KEYWORD);
    TokenSet LOCAL_KEYWORD = TokenSet.create(EiffelTypes.EIF_LOCAL_KEYWORD);
    TokenSet LOOP_KEYWORD = TokenSet.create(EiffelTypes.EIF_LOOP_KEYWORD);
    TokenSet NONE_KEYWORD = TokenSet.create(EiffelTypes.EIF_NONE_KEYWORD);
    TokenSet NOTE_KEYWORD = TokenSet.create(EiffelTypes.EIF_NOTE_KEYWORD);
    TokenSet NOT_KEYWORD = TokenSet.create(EiffelTypes.EIF_NOT_KEYWORD);
    TokenSet OBSOLETE_KEYWORD = TokenSet.create(EiffelTypes.EIF_OBSOLETE_KEYWORD);
    TokenSet OLD_KEYWORD = TokenSet.create(EiffelTypes.EIF_OLD_KEYWORD);
    TokenSet ONCE_KEYWORD = TokenSet.create(EiffelTypes.EIF_ONCE_KEYWORD);
    TokenSet ONLY_KEYWORD = TokenSet.create(EiffelTypes.EIF_ONLY_KEYWORD);
    TokenSet OR_KEYWORD = TokenSet.create(EiffelTypes.EIF_OR_KEYWORD);
    TokenSet PRECURSOR_KEYWORD = TokenSet.create(EiffelTypes.EIF_PRECURSOR_KEYWORD);
    TokenSet REDEFINE_KEYWORD = TokenSet.create(EiffelTypes.EIF_REDEFINE_KEYWORD);
    TokenSet RENAME_KEYWORD = TokenSet.create(EiffelTypes.EIF_RENAME_KEYWORD);
    TokenSet REQUIRE_KEYWORD = TokenSet.create(EiffelTypes.EIF_REQUIRE_KEYWORD);
    TokenSet RESCUE_KEYWORD = TokenSet.create(EiffelTypes.EIF_RESCUE_KEYWORD);
    TokenSet RESULT_KEYWORD = TokenSet.create(EiffelTypes.EIF_RESULT_KEYWORD);
    TokenSet RETRY_KEYWORD = TokenSet.create(EiffelTypes.EIF_RETRY_KEYWORD);
    TokenSet SELECT_KEYWORD = TokenSet.create(EiffelTypes.EIF_SELECT_KEYWORD);
    TokenSet SOME_KEYWORD = TokenSet.create(EiffelTypes.EIF_SOME_KEYWORD);
    TokenSet THEN_KEYWORD = TokenSet.create(EiffelTypes.EIF_THEN_KEYWORD);
    TokenSet TRUE_KEYWORD = TokenSet.create(EiffelTypes.EIF_TRUE_KEYWORD);
    TokenSet TUPLE_KEYWORD = TokenSet.create(EiffelTypes.EIF_TUPLE_KEYWORD);
    TokenSet UNDEFINE_KEYWORD = TokenSet.create(EiffelTypes.EIF_UNDEFINE_KEYWORD);
    TokenSet UNTIL_KEYWORD = TokenSet.create(EiffelTypes.EIF_UNTIL_KEYWORD);
    TokenSet VARIANT_KEYWORD = TokenSet.create(EiffelTypes.EIF_VARIANT_KEYWORD);
    TokenSet WHEN_KEYWORD = TokenSet.create(EiffelTypes.EIF_WHEN_KEYWORD);
    TokenSet XOR_KEYWORD = TokenSet.create(EiffelTypes.EIF_XOR_KEYWORD);
    TokenSet ASSIGN_KEYWORD = TokenSet.create(EiffelTypes.EIF_ASSIGN_KEYWORD);
    TokenSet ATTACHED_KEYWORD = TokenSet.create(EiffelTypes.EIF_ATTACHED_KEYWORD);
    TokenSet DETACHABLE_KEYWORD = TokenSet.create(EiffelTypes.EIF_DETACHABLE_KEYWORD);
    TokenSet SEPARATE_KEYWORD = TokenSet.create(EiffelTypes.EIF_SEPARATE_KEYWORD);

    /**
     * Usefull rule tokens
     */
    TokenSet ACTUALS = TokenSet.create(EiffelTypes.EIF_ACTUALS);
    TokenSet ACTUAL_GENERICS = TokenSet.create(EiffelTypes.EIF_ACTUAL_GENERICS);
    TokenSet ACTUAL_LIST = TokenSet.create(EiffelTypes.EIF_ACTUAL_LIST);
    TokenSet ADDRESS = TokenSet.create(EiffelTypes.EIF_ADDRESS);
    TokenSet AGENT = TokenSet.create(EiffelTypes.EIF_AGENT);
    TokenSet AGENT_ACTUAL = TokenSet.create(EiffelTypes.EIF_AGENT_ACTUAL);
    TokenSet AGENT_ACTUALS = TokenSet.create(EiffelTypes.EIF_AGENT_ACTUALS);
    TokenSet AGENT_ACTUAL_LIST = TokenSet.create(EiffelTypes.EIF_AGENT_ACTUAL_LIST);
    TokenSet AGENT_QUALIFIED = TokenSet.create(EiffelTypes.EIF_AGENT_QUALIFIED);
    TokenSet AGENT_QUALIFIED_CALL = TokenSet.create(EiffelTypes.EIF_AGENT_QUALIFIED_CALL);
    TokenSet AGENT_TARGET = TokenSet.create(EiffelTypes.EIF_AGENT_TARGET);
    TokenSet AGENT_UNQUALIFIED = TokenSet.create(EiffelTypes.EIF_AGENT_UNQUALIFIED);
    TokenSet ALIAS = TokenSet.create(EiffelTypes.EIF_ALIAS);
    TokenSet ANCHOR = TokenSet.create(EiffelTypes.EIF_ANCHOR);
    TokenSet ANCHORED = TokenSet.create(EiffelTypes.EIF_ANCHORED);
    TokenSet ASSERTION = TokenSet.create(EiffelTypes.EIF_ASSERTION);
    TokenSet ASSERTION_CLAUSE = TokenSet.create(EiffelTypes.EIF_ASSERTION_CLAUSE);
    TokenSet ASSIGNER_CALL = TokenSet.create(EiffelTypes.EIF_ASSIGNER_CALL);
    TokenSet ASSIGNER_MARK = TokenSet.create(EiffelTypes.EIF_ASSIGNER_MARK);
    TokenSet ASSIGNMENT = TokenSet.create(EiffelTypes.EIF_ASSIGNMENT);
    TokenSet ATTACHED_TEST = TokenSet.create(EiffelTypes.EIF_ATTACHED_TEST);
    TokenSet ATTACHMENT_KEYWORD = TokenSet.create(EiffelTypes.EIF_ATTACHMENT_KEYWORD);
    TokenSet ATTACHMENT_MARK = TokenSet.create(EiffelTypes.EIF_ATTACHMENT_MARK);
    TokenSet ATTRIBUTE = TokenSet.create(EiffelTypes.EIF_ATTRIBUTE);
    TokenSet ATTRIBUTE_OR_ROUTINE = TokenSet.create(EiffelTypes.EIF_ATTRIBUTE_OR_ROUTINE);
    TokenSet BASIC_EXPRESSION = TokenSet.create(EiffelTypes.EIF_BASIC_EXPRESSION);
    TokenSet BINARY = TokenSet.create(EiffelTypes.EIF_BINARY);
    TokenSet BINARY_AND_THEN = TokenSet.create(EiffelTypes.EIF_BINARY_AND_THEN);
    TokenSet BINARY_EXPRESSION = TokenSet.create(EiffelTypes.EIF_BINARY_EXPRESSION);
    TokenSet BINARY_EXPRESSION_NO_TYPE = TokenSet.create(EiffelTypes.EIF_BINARY_EXPRESSION_NO_TYPE);
    TokenSet BINARY_OR_ELSE = TokenSet.create(EiffelTypes.EIF_BINARY_OR_ELSE);
    TokenSet BOOLEAN_CONSTANT = TokenSet.create(EiffelTypes.EIF_BOOLEAN_CONSTANT);
    TokenSet BOOLEAN_EXPRESSION = TokenSet.create(EiffelTypes.EIF_BOOLEAN_EXPRESSION);
    TokenSet BRACED_TYPE = TokenSet.create(EiffelTypes.EIF_BRACED_TYPE);
    TokenSet BRACKET_EXPRESSION = TokenSet.create(EiffelTypes.EIF_BRACKET_EXPRESSION);
    TokenSet BRACKET_TARGET = TokenSet.create(EiffelTypes.EIF_BRACKET_TARGET);
    TokenSet CALL = TokenSet.create(EiffelTypes.EIF_CALL);
    TokenSet CALL_AGENT = TokenSet.create(EiffelTypes.EIF_CALL_AGENT);
    TokenSet CALL_AGENT_BODY = TokenSet.create(EiffelTypes.EIF_CALL_AGENT_BODY);
    TokenSet CHARACTER_CONSTANT = TokenSet.create(EiffelTypes.EIF_CHARACTER_CONSTANT);
    TokenSet CHECK = TokenSet.create(EiffelTypes.EIF_CHECK);
    TokenSet CHOICE = TokenSet.create(EiffelTypes.EIF_CHOICE);
    TokenSet CHOICES = TokenSet.create(EiffelTypes.EIF_CHOICES);
    TokenSet CLASS_HEADER = TokenSet.create(EiffelTypes.EIF_CLASS_HEADER);
    TokenSet CLASS_LIST = TokenSet.create(EiffelTypes.EIF_CLASS_LIST);
    TokenSet CLASS_NAME_DEFINITION = TokenSet.create(EiffelTypes.EIF_CLASS_NAME_DEFINITION);
    TokenSet CLASS_NAME_IDENTIFIER = TokenSet.create(EiffelTypes.EIF_CLASS_NAME_IDENTIFIER);
    TokenSet CLASS_OR_TUPLE_TYPE = TokenSet.create(EiffelTypes.EIF_CLASS_OR_TUPLE_TYPE);
    TokenSet CLASS_TYPE = TokenSet.create(EiffelTypes.EIF_CLASS_TYPE);
    TokenSet CLASS_TYPE_NAME = TokenSet.create(EiffelTypes.EIF_CLASS_TYPE_NAME);
    TokenSet CLIENTS = TokenSet.create(EiffelTypes.EIF_CLIENTS);
    TokenSet COMPOUND = TokenSet.create(EiffelTypes.EIF_COMPOUND);
    TokenSet CONDITIONAL = TokenSet.create(EiffelTypes.EIF_CONDITIONAL);
    TokenSet CONDITIONAL_EXPRESSION = TokenSet.create(EiffelTypes.EIF_CONDITIONAL_EXPRESSION);
    TokenSet CONSTANT = TokenSet.create(EiffelTypes.EIF_CONSTANT);
    TokenSet CONSTANT_INTERVAL = TokenSet.create(EiffelTypes.EIF_CONSTANT_INTERVAL);
    TokenSet CONSTRAINING_TYPES = TokenSet.create(EiffelTypes.EIF_CONSTRAINING_TYPES);
    TokenSet CONSTRAINT = TokenSet.create(EiffelTypes.EIF_CONSTRAINT);
    TokenSet CONSTRAINT_CREATORS = TokenSet.create(EiffelTypes.EIF_CONSTRAINT_CREATORS);
    TokenSet CONSTRAINT_LIST = TokenSet.create(EiffelTypes.EIF_CONSTRAINT_LIST);
    TokenSet CONVERSION_PROCEDURE = TokenSet.create(EiffelTypes.EIF_CONVERSION_PROCEDURE);
    TokenSet CONVERSION_QUERY = TokenSet.create(EiffelTypes.EIF_CONVERSION_QUERY);
    TokenSet CONVERTER = TokenSet.create(EiffelTypes.EIF_CONVERTER);
    TokenSet CONVERTERS = TokenSet.create(EiffelTypes.EIF_CONVERTERS);
    TokenSet CONVERTER_LIST = TokenSet.create(EiffelTypes.EIF_CONVERTER_LIST);
    TokenSet CREATION_CALL = TokenSet.create(EiffelTypes.EIF_CREATION_CALL);
    TokenSet CREATION_CLAUSE = TokenSet.create(EiffelTypes.EIF_CREATION_CLAUSE);
    TokenSet CREATION_EXPRESSION = TokenSet.create(EiffelTypes.EIF_CREATION_EXPRESSION);
    TokenSet CREATION_INSTRUCTION = TokenSet.create(EiffelTypes.EIF_CREATION_INSTRUCTION);
    TokenSet CREATORS = TokenSet.create(EiffelTypes.EIF_CREATORS);
    TokenSet DEBUG = TokenSet.create(EiffelTypes.EIF_DEBUG);
    TokenSet DECLARATION_BODY = TokenSet.create(EiffelTypes.EIF_DECLARATION_BODY);
    TokenSet DECLARATION_BODY_VALUE = TokenSet.create(EiffelTypes.EIF_DECLARATION_BODY_VALUE);
    TokenSet DECLARATION_IDENTIFIER = TokenSet.create(EiffelTypes.EIF_DECLARATION_IDENTIFIER);
    TokenSet EFFECTIVE_ROUTINE = TokenSet.create(EiffelTypes.EIF_EFFECTIVE_ROUTINE);
    TokenSet EIFFEL_FILE = TokenSet.create(EiffelTypes.EIF_EIFFEL_FILE);
    TokenSet ELSE_PART = TokenSet.create(EiffelTypes.EIF_ELSE_PART);
    TokenSet ENTITY = TokenSet.create(EiffelTypes.EIF_ENTITY);
    TokenSet ENTITY_DECLARATION_GROUP = TokenSet.create(EiffelTypes.EIF_ENTITY_DECLARATION_GROUP);
    TokenSet ENTITY_DECLARATION_LIST = TokenSet.create(EiffelTypes.EIF_ENTITY_DECLARATION_LIST);
    TokenSet ENTITY_IDENTIFIER = TokenSet.create(EiffelTypes.EIF_ENTITY_IDENTIFIER);
    TokenSet EXCLAMATION_MARK = TokenSet.create(EiffelTypes.EIF_EXCLAMATION_MARK);
    TokenSet EXIT_CONDITION = TokenSet.create(EiffelTypes.EIF_EXIT_CONDITION);
    TokenSet EXPLICIT_CREATION_CALL = TokenSet.create(EiffelTypes.EIF_EXPLICIT_CREATION_CALL);
    TokenSet EXPLICIT_CREATION_TYPE = TokenSet.create(EiffelTypes.EIF_EXPLICIT_CREATION_TYPE);
    TokenSet EXPLICIT_VALUE = TokenSet.create(EiffelTypes.EIF_EXPLICIT_VALUE);
    TokenSet EXPRESSION = TokenSet.create(EiffelTypes.EIF_EXPRESSION);
    TokenSet EXPRESSION_LIST = TokenSet.create(EiffelTypes.EIF_EXPRESSION_LIST);
    TokenSet EXPRESSION_NO_BINARY = TokenSet.create(EiffelTypes.EIF_EXPRESSION_NO_BINARY);
    TokenSet EXPRESSION_NO_BINARY_NO_TYPE = TokenSet.create(EiffelTypes.EIF_EXPRESSION_NO_BINARY_NO_TYPE);
    TokenSet EXPRESSION_NO_TYPE = TokenSet.create(EiffelTypes.EIF_EXPRESSION_NO_TYPE);
    TokenSet EXTENDED_FEATURE_NAME = TokenSet.create(EiffelTypes.EIF_EXTENDED_FEATURE_NAME);
    TokenSet EXTERNAL_LANGUAGE = TokenSet.create(EiffelTypes.EIF_EXTERNAL_LANGUAGE);
    TokenSet EXTERNAL_NAME = TokenSet.create(EiffelTypes.EIF_EXTERNAL_NAME);
    TokenSet EXTERNAL_ROUTINE = TokenSet.create(EiffelTypes.EIF_EXTERNAL_ROUTINE);
    TokenSet FEATURES = TokenSet.create(EiffelTypes.EIF_FEATURES);
    TokenSet FEATURE_ADAPTATION = TokenSet.create(EiffelTypes.EIF_FEATURE_ADAPTATION);
    TokenSet FEATURE_BODY = TokenSet.create(EiffelTypes.EIF_FEATURE_BODY);
    TokenSet FEATURE_CLAUSE = TokenSet.create(EiffelTypes.EIF_FEATURE_CLAUSE);
    TokenSet FEATURE_DECLARATION = TokenSet.create(EiffelTypes.EIF_FEATURE_DECLARATION);
    TokenSet FEATURE_DECLARATION_LIST = TokenSet.create(EiffelTypes.EIF_FEATURE_DECLARATION_LIST);
    TokenSet FEATURE_LIST = TokenSet.create(EiffelTypes.EIF_FEATURE_LIST);
    TokenSet FEATURE_NAME = TokenSet.create(EiffelTypes.EIF_FEATURE_NAME);
    TokenSet FEATURE_SET = TokenSet.create(EiffelTypes.EIF_FEATURE_SET);
    TokenSet FEATURE_VALUE = TokenSet.create(EiffelTypes.EIF_FEATURE_VALUE);
    TokenSet FORMAL_ARGUMENTS = TokenSet.create(EiffelTypes.EIF_FORMAL_ARGUMENTS);
    TokenSet FORMAL_GENERIC = TokenSet.create(EiffelTypes.EIF_FORMAL_GENERIC);
    TokenSet FORMAL_GENERICS = TokenSet.create(EiffelTypes.EIF_FORMAL_GENERICS);
    TokenSet FORMAL_GENERIC_LIST = TokenSet.create(EiffelTypes.EIF_FORMAL_GENERIC_LIST);
    TokenSet FORMAL_GENERIC_NAME = TokenSet.create(EiffelTypes.EIF_FORMAL_GENERIC_NAME);
    TokenSet HEADER_MARK = TokenSet.create(EiffelTypes.EIF_HEADER_MARK);
    TokenSet IDENTIFIER_LIST = TokenSet.create(EiffelTypes.EIF_IDENTIFIER_LIST);
    TokenSet INHERITANCE = TokenSet.create(EiffelTypes.EIF_INHERITANCE);
    TokenSet INHERIT_CLAUSE = TokenSet.create(EiffelTypes.EIF_INHERIT_CLAUSE);
    TokenSet INITIALIZATION = TokenSet.create(EiffelTypes.EIF_INITIALIZATION);
    TokenSet INLINE_AGENT = TokenSet.create(EiffelTypes.EIF_INLINE_AGENT);
    TokenSet INSTRUCTION = TokenSet.create(EiffelTypes.EIF_INSTRUCTION);
    TokenSet INTEGER_CONSTANT = TokenSet.create(EiffelTypes.EIF_INTEGER_CONSTANT);
    TokenSet INTERNAL = TokenSet.create(EiffelTypes.EIF_INTERNAL);
    TokenSet INTERROGATION_MARK = TokenSet.create(EiffelTypes.EIF_INTERROGATION_MARK);
    TokenSet INVARIANT = TokenSet.create(EiffelTypes.EIF_INVARIANT);
    TokenSet ITERATION = TokenSet.create(EiffelTypes.EIF_ITERATION);
    TokenSet KEY_LIST = TokenSet.create(EiffelTypes.EIF_KEY_LIST);
    TokenSet LOCAL_DECLARATIONS = TokenSet.create(EiffelTypes.EIF_LOCAL_DECLARATIONS);
    TokenSet LOOP = TokenSet.create(EiffelTypes.EIF_LOOP);
    TokenSet LOOP_BODY = TokenSet.create(EiffelTypes.EIF_LOOP_BODY);
    TokenSet LOOP_BODY_EXPRESSION = TokenSet.create(EiffelTypes.EIF_LOOP_BODY_EXPRESSION);
    TokenSet LOOP_EXPRESSION = TokenSet.create(EiffelTypes.EIF_LOOP_EXPRESSION);
    TokenSet LOOP_SYMBOLIC = TokenSet.create(EiffelTypes.EIF_LOOP_SYMBOLIC);
    TokenSet LOOP_SYMBOLIC_DECLARATION = TokenSet.create(EiffelTypes.EIF_LOOP_SYMBOLIC_DECLARATION);
    TokenSet LOOP_SYMBOLIC_EXPRESSION = TokenSet.create(EiffelTypes.EIF_LOOP_SYMBOLIC_EXPRESSION);
    TokenSet MANIFEST_ARRAY = TokenSet.create(EiffelTypes.EIF_MANIFEST_ARRAY);
    TokenSet MANIFEST_ARRAY_TYPE = TokenSet.create(EiffelTypes.EIF_MANIFEST_ARRAY_TYPE);
    TokenSet MANIFEST_CONSTANT = TokenSet.create(EiffelTypes.EIF_MANIFEST_CONSTANT);
    TokenSet MANIFEST_TUPLE = TokenSet.create(EiffelTypes.EIF_MANIFEST_TUPLE);
    TokenSet MANIFEST_TYPE = TokenSet.create(EiffelTypes.EIF_MANIFEST_TYPE);
    TokenSet MANIFEST_VALUE = TokenSet.create(EiffelTypes.EIF_MANIFEST_VALUE);
    TokenSet MESSAGE = TokenSet.create(EiffelTypes.EIF_MESSAGE);
    TokenSet MULTIPLE_CONSTRAINT = TokenSet.create(EiffelTypes.EIF_MULTIPLE_CONSTRAINT);
    TokenSet MULTI_BRANCH = TokenSet.create(EiffelTypes.EIF_MULTI_BRANCH);
    TokenSet NEW_EXPORTS = TokenSet.create(EiffelTypes.EIF_NEW_EXPORTS);
    TokenSet NEW_EXPORT_ITEM = TokenSet.create(EiffelTypes.EIF_NEW_EXPORT_ITEM);
    TokenSet NEW_EXPORT_LIST = TokenSet.create(EiffelTypes.EIF_NEW_EXPORT_LIST);
    TokenSet NEW_FEATURE = TokenSet.create(EiffelTypes.EIF_NEW_FEATURE);
    TokenSet NEW_FEATURE_LIST = TokenSet.create(EiffelTypes.EIF_NEW_FEATURE_LIST);
    TokenSet NON_CONFORMANCE = TokenSet.create(EiffelTypes.EIF_NON_CONFORMANCE);
    TokenSet NON_OBJECT_CALL = TokenSet.create(EiffelTypes.EIF_NON_OBJECT_CALL);
    TokenSet NON_OBJECT_CALL_PARENTHESIZED = TokenSet.create(EiffelTypes.EIF_NON_OBJECT_CALL_PARENTHESIZED);
    TokenSet NOTES = TokenSet.create(EiffelTypes.EIF_NOTES);
    TokenSet NOTE_ENTRY = TokenSet.create(EiffelTypes.EIF_NOTE_ENTRY);
    TokenSet NOTE_ITEM = TokenSet.create(EiffelTypes.EIF_NOTE_ITEM);
    TokenSet NOTE_LIST = TokenSet.create(EiffelTypes.EIF_NOTE_LIST);
    TokenSet NOTE_VALUES = TokenSet.create(EiffelTypes.EIF_NOTE_VALUES);
    TokenSet OBJECT_CALL = TokenSet.create(EiffelTypes.EIF_OBJECT_CALL);
    TokenSet OBJECT_CALL_START = TokenSet.create(EiffelTypes.EIF_OBJECT_CALL_START);
    TokenSet OBJECT_TEST = TokenSet.create(EiffelTypes.EIF_OBJECT_TEST);
    TokenSet OBSOLETE = TokenSet.create(EiffelTypes.EIF_OBSOLETE);
    TokenSet OLD = TokenSet.create(EiffelTypes.EIF_OLD);
    TokenSet ONCE = TokenSet.create(EiffelTypes.EIF_ONCE);
    TokenSet ONCE_STRING = TokenSet.create(EiffelTypes.EIF_ONCE_STRING);
    TokenSet ONLY = TokenSet.create(EiffelTypes.EIF_ONLY);
    TokenSet OPERATOR_EXPRESSION = TokenSet.create(EiffelTypes.EIF_OPERATOR_EXPRESSION);
    TokenSet PARENT = TokenSet.create(EiffelTypes.EIF_PARENT);
    TokenSet PARENTHESIZED = TokenSet.create(EiffelTypes.EIF_PARENTHESIZED);
    TokenSet PARENT_LIST = TokenSet.create(EiffelTypes.EIF_PARENT_LIST);
    TokenSet PARENT_QUALIFICATION = TokenSet.create(EiffelTypes.EIF_PARENT_QUALIFICATION);
    TokenSet PLACEHOLDER = TokenSet.create(EiffelTypes.EIF_PLACEHOLDER);
    TokenSet POSTCONDITION = TokenSet.create(EiffelTypes.EIF_POSTCONDITION);
    TokenSet PRECONDITION = TokenSet.create(EiffelTypes.EIF_PRECONDITION);
    TokenSet PRECURSOR = TokenSet.create(EiffelTypes.EIF_PRECURSOR);
    TokenSet QUERY_MARK = TokenSet.create(EiffelTypes.EIF_QUERY_MARK);
    TokenSet REAL_CONSTANT = TokenSet.create(EiffelTypes.EIF_REAL_CONSTANT);
    TokenSet REDEFINE = TokenSet.create(EiffelTypes.EIF_REDEFINE);
    TokenSet RENAME = TokenSet.create(EiffelTypes.EIF_RENAME);
    TokenSet RENAME_LIST = TokenSet.create(EiffelTypes.EIF_RENAME_LIST);
    TokenSet RENAME_PAIR = TokenSet.create(EiffelTypes.EIF_RENAME_PAIR);
    TokenSet RENAMING = TokenSet.create(EiffelTypes.EIF_RENAMING);
    TokenSet RESCUE = TokenSet.create(EiffelTypes.EIF_RESCUE);
    TokenSet RETRY = TokenSet.create(EiffelTypes.EIF_RETRY);
    TokenSet ROUTINE_MARK = TokenSet.create(EiffelTypes.EIF_ROUTINE_MARK);
    TokenSet SELECT = TokenSet.create(EiffelTypes.EIF_SELECT);
    TokenSet SIGN = TokenSet.create(EiffelTypes.EIF_SIGN);
    TokenSet SIMPLE_CALL = TokenSet.create(EiffelTypes.EIF_SIMPLE_CALL);
    TokenSet SINGLE_CONSTRAINT = TokenSet.create(EiffelTypes.EIF_SINGLE_CONSTRAINT);
    TokenSet TAG_MARK = TokenSet.create(EiffelTypes.EIF_TAG_MARK);
    TokenSet TARGET = TokenSet.create(EiffelTypes.EIF_TARGET);
    TokenSet THEN_PART = TokenSet.create(EiffelTypes.EIF_THEN_PART);
    TokenSet THEN_PART_EXPRESSION = TokenSet.create(EiffelTypes.EIF_THEN_PART_EXPRESSION);
    TokenSet THEN_PART_EXPRESSION_LIST = TokenSet.create(EiffelTypes.EIF_THEN_PART_EXPRESSION_LIST);
    TokenSet THEN_PART_LIST = TokenSet.create(EiffelTypes.EIF_THEN_PART_LIST);
    TokenSet TUPLE_DECLARATION_GROUP = TokenSet.create(EiffelTypes.EIF_TUPLE_DECLARATION_GROUP);
    TokenSet TUPLE_DECLARATION_LIST = TokenSet.create(EiffelTypes.EIF_TUPLE_DECLARATION_LIST);
    TokenSet TUPLE_DEFINITION = TokenSet.create(EiffelTypes.EIF_TUPLE_DEFINITION);
    TokenSet TUPLE_IDENTIFIER = TokenSet.create(EiffelTypes.EIF_TUPLE_IDENTIFIER);
    TokenSet TUPLE_IDENTIFIER_LIST = TokenSet.create(EiffelTypes.EIF_TUPLE_IDENTIFIER_LIST);
    TokenSet TUPLE_PARAMETERS = TokenSet.create(EiffelTypes.EIF_TUPLE_PARAMETERS);
    TokenSet TUPLE_PARAMETER_LIST = TokenSet.create(EiffelTypes.EIF_TUPLE_PARAMETER_LIST);
    TokenSet TUPLE_TYPE = TokenSet.create(EiffelTypes.EIF_TUPLE_TYPE);
    TokenSet TYPE = TokenSet.create(EiffelTypes.EIF_TYPE);
    TokenSet TYPE_INTERVAL = TokenSet.create(EiffelTypes.EIF_TYPE_INTERVAL);
    TokenSet TYPE_LIST = TokenSet.create(EiffelTypes.EIF_TYPE_LIST);
    TokenSet TYPE_MARK = TokenSet.create(EiffelTypes.EIF_TYPE_MARK);
    TokenSet UNARY = TokenSet.create(EiffelTypes.EIF_UNARY);
    TokenSet UNARY_EXPRESSION = TokenSet.create(EiffelTypes.EIF_UNARY_EXPRESSION);
    TokenSet UNDEFINE = TokenSet.create(EiffelTypes.EIF_UNDEFINE);
    TokenSet UNLABELED_ASSERTION_CLAUSE = TokenSet.create(EiffelTypes.EIF_UNLABELED_ASSERTION_CLAUSE);
    TokenSet UNQUALIFIED_CALL = TokenSet.create(EiffelTypes.EIF_UNQUALIFIED_CALL);
    TokenSet VARIANT = TokenSet.create(EiffelTypes.EIF_VARIANT);
    TokenSet WHEN_PART = TokenSet.create(EiffelTypes.EIF_WHEN_PART);
    TokenSet WHEN_PART_LIST = TokenSet.create(EiffelTypes.EIF_WHEN_PART_LIST);

}
