package net.libreti.intelleiffel.psi;

import com.intellij.psi.tree.IElementType;
import net.libreti.intelleiffel.EiffelLanguage;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

/**
 * Eiffel grammar single element type.
 *
 * @author Louis M
 */
public class EiffelElementType extends IElementType {

    /**
     * Class creator.
     *
     * @param aDebugName A debugging texte representing the element.
     */
    public EiffelElementType(@NonNls @NotNull String aDebugName) {
        super(aDebugName, EiffelLanguage.getInstance());
    }
}
