package net.libreti.intelleiffel.lexer;

import com.intellij.lexer.FlexAdapter;

/**
 * This class define an IntelliJ plugin adapter for the EiffelLexer (Flex).
 *
 * @author Louis M
 */
public class EiffelLexerAdapter extends FlexAdapter {

    /**
     * Class creator.
     */
    public EiffelLexerAdapter() {
        super(new EiffelLexer(null));
    }

}