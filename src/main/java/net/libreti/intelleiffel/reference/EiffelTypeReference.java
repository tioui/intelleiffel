package net.libreti.intelleiffel.reference;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.util.IncorrectOperationException;
import net.libreti.intelleiffel.parser.Mixins.EiffelClassTypeName;
import net.libreti.intelleiffel.parser.Mixins.EiffelClass;
import net.libreti.intelleiffel.psi.EiffelPsiClassHeader;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Give references for Eiffel types.
 *
 * @author Louis M
 */
class EiffelTypeReference extends PsiReferenceBase<EiffelClassTypeName> {

    /**
     * Creator of the reference utility
     *
     * @param aElement The element to reference
     */
    public EiffelTypeReference(@NotNull EiffelClassTypeName aElement) {
        super(aElement, TextRange.allOf(aElement.getText()));
    }

    /**
     * Resolve the reference of the type `myElement`
     *
     * @return The Eiffel Psi Element of the definition of the Eiffel type
     */
    @Override
    public @Nullable PsiElement resolve() {
        PsiElement lResult = null;
        EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(myElement.getName(),
                myElement.getProject());
        if (lClass != null) {
            EiffelPsiClassHeader lHeader = lClass.getClassHeader();
            if (lHeader.getTupleDefinition() != null) {
                lResult = lHeader.getTupleDefinition().getTupleKeyword();
            } else if (lHeader.getClassNameDefinition() != null) {
                lResult = lHeader.getClassNameDefinition();
            }
        }
        return lResult;
    }

    /**
     * When a rename refactor is used, launch the setName of the element.
     *
     * @param aNewElementName the new name of the target element.
     * @return The PsiElement that has been renamed.
     *
     * @throws IncorrectOperationException When an error occured in the renaming.
     */
    @Override
    public PsiElement handleElementRename(String aNewElementName) throws IncorrectOperationException {
        myElement.setName(aNewElementName);
        return myElement;
    }

}
