package net.libreti.intelleiffel.reference;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiReference;
import com.intellij.psi.PsiReferenceProvider;
import com.intellij.util.ProcessingContext;
import net.libreti.intelleiffel.parser.Mixins.EiffelClassTypeName;
import org.jetbrains.annotations.NotNull;

/**
 * Provade class type reference
 *
 * @author Louis M
 */
public class EiffelTypeReferenceProvider extends PsiReferenceProvider {

    /**
     * Return the reference (The class definition) of an EiffelClassTypeName element.
     * @param aElement The EiffelClassTypeName element
     * @param aContext The context in which the reference do happen (not used)
     * @return An array (empty or containing one element) that contain the reference of the Class Type
     */
    @Override
    public PsiReference @NotNull [] getReferencesByElement(@NotNull PsiElement aElement,
                                                           @NotNull ProcessingContext aContext) {
        PsiReference[] lResult;
        if (aElement instanceof EiffelClassTypeName lClassTypeName) {
            lResult = new PsiReference[]{new EiffelTypeReference(lClassTypeName)};
        } else {
            lResult = PsiReference.EMPTY_ARRAY;
        }
        return lResult;
    }

}
