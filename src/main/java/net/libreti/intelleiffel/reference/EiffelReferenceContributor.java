package net.libreti.intelleiffel.reference;

import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.PsiReferenceContributor;
import com.intellij.psi.PsiReferenceRegistrar;
import net.libreti.intelleiffel.parser.Mixins.EiffelClassTypeName;
import org.jetbrains.annotations.NotNull;

/**
 * Used to create Reference provider
 *
 * @author Louis M
 */
public class EiffelReferenceContributor extends PsiReferenceContributor {

    /**
     * Creator of the contributor
     *
     * @param registrar Used to register reference providers
     */
    @Override
    public void registerReferenceProviders(@NotNull PsiReferenceRegistrar registrar) {
        registrar.registerReferenceProvider(PlatformPatterns.psiElement(EiffelClassTypeName.class),
                                            new EiffelTypeReferenceProvider());
    }
}
