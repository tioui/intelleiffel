package net.libreti.intelleiffel;

import com.intellij.lang.Language;

/**
 * Class that represent the Eiffel language.
 *
 * @author Louis M
 */
public class EiffelLanguage extends Language {

    /**
     * Singleton instance of the EiffelLanguage class.
     */
    private static final EiffelLanguage INSTANCE = new EiffelLanguage();

    /**
     * Retreive the singleton instance of EiffelLanguage.
     *
     * @return The EiffelLanguage instance
     */
    public static EiffelLanguage getInstance() {
        return INSTANCE;
    }



    /**
     * Creator of the class.
     */
    private EiffelLanguage() {
        super("Eiffel");
    }
}
