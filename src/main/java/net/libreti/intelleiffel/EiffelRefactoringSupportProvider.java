package net.libreti.intelleiffel;

import com.intellij.lang.refactoring.RefactoringSupportProvider;
import com.intellij.psi.PsiElement;
import net.libreti.intelleiffel.parser.Mixins.EiffelClassNameRenamable;
import net.libreti.intelleiffel.parser.Mixins.EiffelClass;
import net.libreti.intelleiffel.psi.EiffelUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Indicate which refactoring can be done in place
 *
 * @author Louis M
 */
final public class EiffelRefactoringSupportProvider extends RefactoringSupportProvider {

    /**
     * {@inheritDoc}
     *
     * @param aContext The context in which the reference do happen (not used)
     * @return True if the refactoring is available. Else if not
     */
    @Override
    public boolean isAvailable(@NotNull PsiElement aContext) {
        boolean lResult = super.isAvailable(aContext);
        if (aContext instanceof EiffelClassNameRenamable) {
            EiffelClass lClass = EiffelUtil.getInstance().findEiffelClass(
                    ((EiffelClassNameRenamable) aContext).getName(), aContext.getProject());
            lResult = lClass != null && lClass.isWritable();
        }
        return lResult;
    }

    /**
     * Indicate which refactoring can be done in place
     *
     * @param aElementToRename The element that have to be refactored
     * @param aContext The context in which the reference do happen (not used)
     * @return True if the element can be refactored in place. False if not.
     */
    @Override
    public boolean isMemberInplaceRenameAvailable(@NotNull PsiElement aElementToRename, @Nullable PsiElement aContext) {
        return (aElementToRename instanceof EiffelClassNameRenamable);
    }

}
