package net.libreti.intelleiffel;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import net.libreti.intelleiffel.lexer.EiffelLexerAdapter;
import net.libreti.intelleiffel.psi.EiffelTypes;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

/**
 * Class used to determine syntax highlighting.
 *
 * @author Louis M
 */
public class EiffelSyntaxHighlighter extends SyntaxHighlighterBase {

    /**
     * Every Attribute keys to put in the Attribute keys arrays.
     */
    private static final TextAttributesKey IDENTIFIER =
            createTextAttributesKey("EIFFEL_IDENTIFIER", DefaultLanguageHighlighterColors.IDENTIFIER);
    private static final TextAttributesKey KEYWORD =
            createTextAttributesKey("EIFFEL_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);
    private static final TextAttributesKey NUMBER =
            createTextAttributesKey("EIFFEL_NUMBER", DefaultLanguageHighlighterColors.NUMBER);
    private static final TextAttributesKey STRING =
            createTextAttributesKey("EIFFEL_STRING", DefaultLanguageHighlighterColors.STRING);
    private static final TextAttributesKey OPERATION =
            createTextAttributesKey("EIFFEL_OPERATION", DefaultLanguageHighlighterColors.OPERATION_SIGN);
    private static final TextAttributesKey BRACES =
            createTextAttributesKey("EIFFEL_BRACES", DefaultLanguageHighlighterColors.BRACES);
    private static final TextAttributesKey PARENTHESES =
            createTextAttributesKey("EIFFEL_PARENTHESES", DefaultLanguageHighlighterColors.PARENTHESES);
    private static final TextAttributesKey BRACKETS =
            createTextAttributesKey("EIFFEL_BRACKETS", DefaultLanguageHighlighterColors.BRACKETS);
    private static final TextAttributesKey DOT =
            createTextAttributesKey("EIFFEL_DOT", DefaultLanguageHighlighterColors.DOT);
    private static final TextAttributesKey COMMA =
            createTextAttributesKey("EIFFEL_COMMA", DefaultLanguageHighlighterColors.COMMA);
    private static final TextAttributesKey COMMENT =
            createTextAttributesKey("EIFFEL_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
    private static final TextAttributesKey BAD_CHARACTER =
            createTextAttributesKey("EIFFEL_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);


    /**
     * Every attribute keys arrays that will be used for different syntax color.
     */
    private static final TextAttributesKey[] IDENTIFIER_KEYS = new TextAttributesKey[]{IDENTIFIER};
    private static final TextAttributesKey[] KEYWORD_KEYS = new TextAttributesKey[]{KEYWORD};
    private static final TextAttributesKey[] NUMBER_KEYS = new TextAttributesKey[]{NUMBER};
    private static final TextAttributesKey[] STRING_KEYS = new TextAttributesKey[]{STRING};
    private static final TextAttributesKey[] OPERATION_KEYS = new TextAttributesKey[]{OPERATION};
    private static final TextAttributesKey[] BRACES_KEYS = new TextAttributesKey[]{BRACES};
    private static final TextAttributesKey[] PARENTHESES_KEYS = new TextAttributesKey[]{PARENTHESES};
    private static final TextAttributesKey[] BRACKETS_KEYS = new TextAttributesKey[]{BRACKETS};
    private static final TextAttributesKey[] DOT_KEYS = new TextAttributesKey[]{DOT};
    private static final TextAttributesKey[] COMMA_KEYS = new TextAttributesKey[]{COMMA};
    private static final TextAttributesKey[] COMMENT_KEYS = new TextAttributesKey[]{COMMENT};
    private static final TextAttributesKey[] BAD_CHARACTER_KEYS = new TextAttributesKey[]{BAD_CHARACTER};
    private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

    /**
     * Adapter to the Lexer used for syntax highlithing.
     *
     * @return the lexer adapter.
     */
    @Override
    public @NotNull Lexer getHighlightingLexer() {
        return new EiffelLexerAdapter();
    }

    /**
     * Specify what attributes keys to used for operator token
     *
     * @param aTokenType The token to color
     * @return The attribute keys array that represent the color to use.
     */
    public TextAttributesKey @NotNull [] getTokenHighlightsOperator(IElementType aTokenType) {
        TextAttributesKey[] lResult = EMPTY_KEYS;
        if (
            aTokenType.equals(EiffelTypes.EIF_PLUS_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_MINUS_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_STAR_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_SLASH_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_AND_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_OR_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_DIVISION_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_MODULO_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_HAT_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_DOT_DOT_SEPARATOR) ||
            aTokenType.equals(EiffelTypes.EIF_LOWER_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_GREATER_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_LEFT_SHIFT_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_RIGHT_SHIFT_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_LOWER_EQUAL_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_GREATER_EQUAL_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_ASSIGN) ||
            aTokenType.equals(EiffelTypes.EIF_FOR_ALL) ||
            aTokenType.equals(EiffelTypes.EIF_EXISTS) ||
            aTokenType.equals(EiffelTypes.EIF_CLOCKWISE_CIRCLE) ||
            aTokenType.equals(EiffelTypes.EIF_COUNTER_CLOCKWISE_CIRCLE) ||
            aTokenType.equals(EiffelTypes.EIF_BROKEN_BAR) ||
            aTokenType.equals(EiffelTypes.EIF_FREE_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_EQUAL_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_NOT_EQUAL_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_EQUIVALENT_OPERATOR) ||
            aTokenType.equals(EiffelTypes.EIF_NOT_EQUIVALENT_OPERATOR)
        ) {
            lResult = OPERATION_KEYS;
        }
        return lResult;
    }

    /**
     * Specify what attributes keys to used for keyword token
     *
     * @param aTokenType The token to color
     * @return The attribute keys array that represent the color to use.
     */
    public TextAttributesKey @NotNull [] getTokenHighlightsKeywords(IElementType aTokenType) {
        TextAttributesKey[] lResult = EMPTY_KEYS;
        if (
            aTokenType.equals(EiffelTypes.EIF_ACROSS_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_ASSIGN_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_AGENT_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_ALIAS_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_ALL_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_AND_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_AS_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_ATTACHED_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_ATTRIBUTE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_CHECK_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_CLASS_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_CONVERT_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_CREATE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_CURRENT_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_DEBUG_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_DEFERRED_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_DETACHABLE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_DO_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_ELSE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_ELSEIF_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_END_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_ENSURE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_EXPANDED_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_EXPORT_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_EXTERNAL_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_FALSE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_FEATURE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_FROM_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_FROZEN_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_IF_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_IMPLIES_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_INHERIT_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_INSPECT_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_INVARIANT_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_LIKE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_LOCAL_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_LOOP_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_NONE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_NOTE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_NOT_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_OBSOLETE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_OLD_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_ONCE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_ONLY_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_OR_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_PRECURSOR_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_REDEFINE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_RENAME_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_REQUIRE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_RESCUE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_RESULT_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_RETRY_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_SELECT_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_SEPARATE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_SOME_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_THEN_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_TRUE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_TUPLE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_UNDEFINE_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_UNTIL_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_VARIANT_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_WHEN_KEYWORD) ||
            aTokenType.equals(EiffelTypes.EIF_XOR_KEYWORD)
        ) {
            lResult = KEYWORD_KEYS;
        }
        return lResult;
    }

    /**
     * Specify what attributes keys to used for every syntax token
     *
     * @param aTokenType The token to color
     * @return The attribute keys array that represent the color to use.
     */
    @Override
    public TextAttributesKey @NotNull [] getTokenHighlights(IElementType aTokenType) {
        TextAttributesKey[] lResult = getTokenHighlightsOperator(aTokenType);
        if (lResult == EMPTY_KEYS) {
            lResult = getTokenHighlightsKeywords(aTokenType);
        }
        if (lResult == EMPTY_KEYS) {
            if (aTokenType.equals(EiffelTypes.EIF_IDENTIFIER)) {
                lResult = IDENTIFIER_KEYS;
            } else if (aTokenType.equals(EiffelTypes.EIF_NUMBER) || aTokenType.equals(EiffelTypes.EIF_REAL)) {
                lResult = NUMBER_KEYS;
            } else if (aTokenType.equals(EiffelTypes.EIF_STRING)) {
                lResult = STRING_KEYS;
            } else if (aTokenType.equals(EiffelTypes.EIF_LINE_COMMENT)) {
                lResult = COMMENT_KEYS;
            } else if (aTokenType.equals(EiffelTypes.EIF_LEFT_PARENTHESE) ||
                    aTokenType.equals(EiffelTypes.EIF_RIGHT_PARENTHESE)){
                lResult = PARENTHESES_KEYS;
            } else if (aTokenType.equals(EiffelTypes.EIF_LEFT_BRACKET) ||
                    aTokenType.equals(EiffelTypes.EIF_RIGHT_BRACKET)){
                lResult = BRACKETS_KEYS;
            } else if (aTokenType.equals(EiffelTypes.EIF_LEFT_BRACE) || aTokenType.equals(EiffelTypes.EIF_RIGHT_BRACE)){
                lResult = BRACES_KEYS;
            } else if (aTokenType.equals(EiffelTypes.EIF_DOT_SEPARATOR)) {
                lResult = DOT_KEYS;
            } else if (aTokenType.equals(EiffelTypes.EIF_COMMA_SEPARATOR)) {
                lResult = COMMA_KEYS;
            } else if (aTokenType.equals(TokenType.BAD_CHARACTER)) {
                lResult = BAD_CHARACTER_KEYS;
            }
        }
        return lResult;
    }
}
