Intelleiffel
============

An Eiffel Plugin for IntelliJ.

The plugin is currently incomplete. Here is what is working and what is not:

- [] Code Highlighting
    - [x] Code completion for classes
    - [] Code completion for feature
    - [] Other code completion
- [] Code refactoring
    - [x] Class refactoring
    - [] Other refactoring
- [] References
    - [x] Reference for classes
    - [] Other references
- [] Usages
    - [x] Usage for classes
    - [] Usage for other
- [] Annotator
    - [x] Annotator for unknown classes
- [x] Formatting and auto-formatting
- [x] Commenter
- [] Documentation system
    - [x] Class documentation system
    - [] Other documentation system
- [x] Structure view, Navigation bar, Symbol and class navigation
- [] Project management (adding libraries, run, debug, etc.)

Execution
=========

To execute the project, you need IntelliJ installed in the system and accessible.

To execute the project, using a shwll console, go to the root directory of the
project and use the folowing command:

```bash
./gradlew runIde
```

Community discussions
---------------------

Join our discord server at : https://discord.gg/KXqsQHyDpq .

License
-------

MIT License

Copyright © 2023 Louis M

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
