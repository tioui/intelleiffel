import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.grammarkit.tasks.GenerateParserTask
import org.jetbrains.grammarkit.tasks.GenerateLexerTask


plugins {
    id("java")
    id("org.jetbrains.kotlin.jvm") version "2.0.0"
    id("org.jetbrains.intellij") version "1.17.4"
    id("org.jetbrains.grammarkit") version "2022.3.2.2"
}

group = "net.libreti"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
    version.set("2024.1.4")
    type.set("IC") // Target IDE Platform
    plugins.set(listOf("com.intellij.java"))
}

sourceSets {
    main {
        kotlin {
            srcDir("src/main/kotlin")
        }
        java {
            srcDirs("src/main/java", "src/main/gen")
        }
    }
}

apply {
    plugin("org.jetbrains.grammarkit")
}
tasks {



    generateParser {
        sourceFile.set(File("src/main/grammar/Eiffel.bnf"))
        targetRootOutputDir = project.layout.projectDirectory.dir("src/main/gen")
        pathToParser = "net/libreti/intelleiffel/parser/EiffelParser.java"
        pathToPsiRoot = "net/libreti/intelleiffel/psi"
        purgeOldFiles = false
    }

    generateLexer {
        dependsOn("generateParser")
        sourceFile.set(File("src/main/grammar/Eiffel.flex"))
        targetOutputDir = project.layout.projectDirectory.dir("src/main/gen/net/libreti/intelleiffel/lexer")
        purgeOldFiles = true
    }

    // Set the JVM compatibility versions
    withType<JavaCompile> {
        sourceCompatibility = "17"
        targetCompatibility = "17"
    }
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        dependsOn("generateParser")
        dependsOn("generateLexer")
        compilerOptions {
            jvmTarget.set(JvmTarget.JVM_17)
        }
    }

//    patchPluginXml {
//        sinceBuild.set("241")
//        untilBuild.set("241.*")
//    }

    signPlugin {
        certificateChain.set(System.getenv("CERTIFICATE_CHAIN"))
        privateKey.set(System.getenv("PRIVATE_KEY"))
        password.set(System.getenv("PRIVATE_KEY_PASSWORD"))
    }

    publishPlugin {
        token.set(System.getenv("PUBLISH_TOKEN"))
    }

}

